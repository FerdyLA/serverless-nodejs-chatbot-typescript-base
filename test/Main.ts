// import { main as rpcMain } from "RpcTest";
// import { main as queryMain } from "QueryTest";
// import { main as hidMain } from "HidTest";
import { main as apiMain } from "ApiTest";

export async function main(...args: string[])
{
    let [cmd, ...rest] = args;
    
    switch (cmd)
    {
        case "api":
        {
            console.log("running cmd api");
            apiMain(...rest);
            break;
        }
        
        default:
        {
            console.log(`invalid cmd: ${cmd}`);
        }
    }
}
// npm run cmd ______
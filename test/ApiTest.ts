import { Api } from "api/Api";
import { Permission } from "models/app/Permission";

export async function testRpc()
{
    console.log("Login & Domain Change Testing via RPC calls");
    try
    {
        //let api = new Api("18r", "system/toUNNCyvOUFKiG3gyXL7P4ZM");
        //let api = new Api("18r", "web/srqO01q6ZZi79T9sNlmVwUAn");
        //let api = new Api("18r", "mobile/sMZVm9kvhkNRMMslhiTjyd4d");
        //let api = new Api("18r", "eps/Ch7YEfQKh22OTypwC1hmScbT");
        
        console.log("HEADERS:");
        let api = new Api("caviot", "", "sys/9SqweUQ50G2V29WRAjjCfBTU");
        // let api = new Api("caviot", "", "test/0fSt0yPzAWNvIU0qTOgkv6Jr");
        // let api = new Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.log(api.headers);
        console.log("=================================");
        
        // console.log("SERVER TIME:");
        // console.log(await api.call("test.timestamp"));
        // // console.log(await api.call("test.elrhckj"));
        // console.log("=================================");
        
        // console.log("Perm check: ",Permission.match("caviot/role/guest","caviot/role/%%guest%%"));
        
        // console.log("LIST CONTEXT PERMS:");
        // console.log(await api.auth.checkContextPermissions());
        // console.log("=================================");
        
        // console.log("RUBBISH PERM TEST:");
        // console.log(await api.call("test.rubbishPerm"));
        // console.log("=================================");
        
        // let env = await api.call("test.getEnv");
        // console.log("SERVER ENV:", env);
        
        // let z = await api.call("auth.setPassword", "app_dev@embrioent.com", "Omn1tr1b3$");
        // console.log("SET PWD (Master):", z);
        
        let t = await api.call("auth.setPassword", "technician@embrioent.com", "00000");
        console.log("SET PWD (Ferdy)", t);
        
        let m = await api.call("auth.setPassword", "manager@embrioent.com", "00000");
        console.log("SET PWD (Ferdy)", m);
        
        // let z = await api.call("auth.setPassword", "davidnguyen968@gmail.com", "C@vi0tD3v");
        // console.log("SET PWD (Master):", z);
        
        // let f = await api.call("auth.setPassword", "dnthien@cmc.com.vn", "C@vi0tD3v");
        // console.log("SET PWD (Ferdy)", f);
        
        // // console.log("Test Change Domain:", await api.call("test.changeDomain"));
        
        // console.log("LOGIN:");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("ferdinand.lim@embrioent.com", "bb40thcoy");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("davidnguyen968@gmail.com", "C@vi0tD3v");
        // let { user } = await api.auth.login("dnthien@cmc.com.vn", "123456");
        // if (!user) throw "Invalid login";
        // console.log("- - - - - - - - - - - - - - - - -");
        
        // console.log("LOGIN OK:");
        // console.log(user);
        // console.log("=================================");
        // console.log(api.headers["x-omni-accesstoken"]);
        // console.log("=================================");
        
        // // console.log("LIST CONTEXT PERMS:");
        // // console.log(await api.auth.checkContextPermissions());
        // // console.log("=================================");
        
        // // console.log("CHECK Permissions:");
        // // console.log(await api.call("permission.listFunctionPermissions"));
        // // console.log("=================================");
        
        // // console.log("CHECK Methods:");
        // // console.log(await api.call("permission.listAllowedMethods"));
        // // console.log("=================================");
        
        // // // console.log("CHECK 3:");
        // // // console.log(await api.call("test.check3"));
        // // // console.log("=================================");
        
        // // // console.log("LIST CONTEXT PERMS:");
        // // // console.log(await api.auth.checkContextPermissions());
        // // // console.log("=================================");
        
        // // // console.log("RUBBISH PERM TEST:");
        // // // console.log(await api.call("test.rubbishPerm"));
        // // // console.log("=================================");
        
        // // // console.log("CHECK TOKEN:");
        // // // console.log(await api.call("auth.checkToken"));
        // // // console.log("=================================");
        
        // // console.log("DOMAINS:");
        // // let domains = await api.member.listDomains();
        // // console.log(domains);
        // // console.log("=================================");
        
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        
        // console.log("LOGIN:");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("ferdinand.lim@embrioent.com", "bb40thcoy");
        // console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"))
        // // let { user } = await api.auth.login("davidnguyen968@gmail.com", "C@vi0tD3v");
        // // console.log(await api.auth.login("dnthien@cmc.com.vn", "123456"));
        // console.log("=================================");
        // console.log("LOGOUT:");
        // console.log(await api.auth.logout());
        // console.log("=================================");
        
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        
        // console.log("LOGIN:");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("ferdinand.lim@embrioent.com", "bb40thcoy");
        // console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"))
        // // let { user } = await api.auth.login("davidnguyen968@gmail.com", "C@vi0tD3v");
        // // console.log(await api.auth.login("dnthien@cmc.com.vn", "123456"));
        // console.log("=================================");
        // console.log("LOGOUT:");
        // console.log(await api.auth.logout());
        // console.log("=================================");
        
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        
        // console.log("LOGIN:");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("ferdinand.lim@embrioent.com", "bb40thcoy");
        // console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"))
        // // let { user } = await api.auth.login("davidnguyen968@gmail.com", "C@vi0tD3v");
        // // console.log(await api.auth.login("dnthien@cmc.com.vn", "123456"));
        // console.log("=================================");
        // console.log("LOGOUT:");
        // console.log(await api.auth.logout());
        // console.log("=================================");
        
        // console.log("REFRESH:");
        // // console.log(await api.auth.changeDomain(domains[Math.floor(Math.random() * domains.length)].key));
        // // console.log(await api.auth.changeDomain("test"));
        // // console.log(await api.auth.changeDomain("caviot-master"));
        // console.log(await api.auth.refresh());
        // console.log("=================================");
        
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        
        
        
        // console.log("LOGOUT:");
        // console.log(await api.auth.logout());
        // console.log("=================================");
        
        // console.log("HEADERS:");
        // console.log(api.headers);
        // console.log("=================================");
        
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        
        // console.log("LOGIN:");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("ferdinand.lim@embrioent.com", "bb40thcoy");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("davidnguyen968@gmail.com", "C@vi0tD3v");
        // console.log(await api.auth.login("dnthien@cmc.com.vn", "123456"));
        // console.log("=================================");
        
        // console.log("HEADERS:");
        // console.log(api.headers);
        // console.log("=================================");
        
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        
        // console.log("Switch To Master Domain 1:");
        // // console.log(await api.auth.changeDomain(domains[Math.floor(Math.random() * domains.length)].key));
        // // console.log(await api.auth.changeDomain("test"));
        // console.log(await api.auth.changeDomain("caviot-master"));
        // console.log("=================================");
        
        // console.log("REFRESH:");
        // // console.log(await api.auth.changeDomain(domains[Math.floor(Math.random() * domains.length)].key));
        // // console.log(await api.auth.changeDomain("test"));
        // // console.log(await api.auth.changeDomain("caviot-master"));
        // console.log(await api.auth.refresh());
        // console.log("=================================");
        
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        
        
        
        // console.log("LOGOUT:");
        // console.log(await api.auth.logout());
        // console.log("=================================");
        
        // console.log("HEADERS:");
        // console.log(api.headers);
        // console.log("=================================");
        
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        
        // console.log("LOGIN:");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("ferdinand.lim@embrioent.com", "bb40thcoy");
        // console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"))
        // // let { user } = await api.auth.login("davidnguyen968@gmail.com", "C@vi0tD3v");
        // // console.log(await api.auth.login("dnthien@cmc.com.vn", "123456"));
        // console.log("=================================");
        
        // console.log("HEADERS:");
        // console.log(api.headers);
        // console.log("=================================");
        
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        
        // console.log("Switch To Master Domain 2:");
        // // console.log(await api.auth.changeDomain(domains[Math.floor(Math.random() * domains.length)].key));
        // // console.log(await api.auth.changeDomain("test"));
        // console.log(await api.auth.changeDomain("caviot-master"));
        // console.log("=================================");
        
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        
        
        
        // console.log("HEADERS:");
        // console.log(api.headers);
        // console.log("=================================");
        
        // console.log("LIST CONTEXT PERMS:");
        // console.log(await api.auth.checkContextPermissions());
        // console.log("=================================");
        
        // console.log("CHECK Permissions:");
        // console.log(await api.call("permission.listFunctionPermissions"));
        // console.log("=================================");
        
        // console.log("CHECK Methods:");
        // console.log(await api.call("permission.listAllowedMethods"));
        // console.log("=================================");
        
        // console.log("RUBBISH PERM TEST:");
        // console.log(await api.call("test.rubbishPerm"));
        // console.log("=================================");
        // console.log(api.headers["x-omni-accesstoken"]);
        // console.log("=================================");
        
        // console.log("CLIENT API:");
        // console.log(api.domain);
        // console.log("=================================");
        // console.log(api.headers["x-omni-accesstoken"]);
        // console.log("=================================");
        
        // console.log("CONTEXT USER:");
        // console.log(await api.call("test.getUser"));
        // console.log(await api.auth.checkContextUser());
        // console.log("=================================");
        // console.log(api.headers["x-omni-accesstoken"]);
        // console.log("=================================");
        
        // console.log("CHECK TOKEN:");
        // console.log(await api.call("auth.checkToken"));
        // console.log("=================================");
        
        // await api.registry.save("18r/config/roles.json",
        // {
        //     "caviot/guest":                   { en: "Guest", zh: "Guest" },
        //     "caviot/member":                  { en: "Member", zh: "Member" },
            
        //     "caviot/member/building/admin":   { en: "Building Admin", zh: "Building Admin" },
        //     "caviot/member/building/staff":   { en: "Building Staff", zh: "Building Staff" },
        //     "caviot/member/building/visitor": { en: "Building Visitor", zh: "Building Visitor" },
            
        //     "caviot/member/office/admin":     { en: "Office Admin", zh: "Office Admin" },
        //     "caviot/member/office/staff":     { en: "Office Staff", zh: "Office AdStaffin" },
        //     "caviot/member/office/visitor":   { en: "Office Visitor", zh: "Office Visitor" },
            
        //     "caviot/member/retail/admin":     { en: "Retail Admin", zh: "Retail Admin" },
        //     "caviot/member/retail/staff":     { en: "Retail Staff", zh: "Retail Staff" },
        //     "caviot/member/retail/visitor":   { en: "Retail Visitor", zh: "Retail Visitor" },
        // });
        
        // let key = await api.auth.createApiKey("blah",
        // {
        //     tokens:
        //     {
        //         accessDuration: null,           // null means never expire
        //         refreshDuration: null,          // other examples: { minutes: 15 }, { days: 3 }
        //     },
        //     permissions:
        //     {
        //         baseApiRoles: ["caviot/dev/develop", "caviot/guest/view"],  // default roles when using the api key
        //         baseMemberRoles: ["caviot/user/view"],                  // default roles when authenticated (in addition to roles above)
        //     }
        // });
        
        // "permissions": { "baseApiRoles": ["caviot/system", "caviot/guest"], "baseMemberRoles": ["caviot/member"] }
        console.log(".............");
        console.log("............");
        console.log("...........");
        console.log("..........");
        console.log(".........");
        console.log("........");
        console.log(".......");
        console.log("......");
        console.log(".....");
        console.log("....");
        console.log("...");
        console.log("..");
        console.log(".");
    }
    catch (ex)
    {
        console.log(ex);
    }
}



export async function main(...args: string[])
{
    console.log("Running tests:", args);
    
    try
    {
        testRpc();
        
    //     console.log("HEADERS:");
    //     let api = new Api("caviot", "", "sys/9SqweUQ50G2V29WRAjjCfBTU");
    //     console.log(api.headers);
    //     console.log("=================================");
        
    //     console.log("LOGIN:");
    //     let { user } = await api.auth.login("dnthien@cmc.com.vn", "123456");
    //     if (!user) throw "Invalid login";
    //     console.log("- - - - - - - - - - - - - - - - -");
        
    //     console.log("LOGIN OK:");
    //     console.log(user);
    //     console.log("=================================");
    //     console.log(api.headers["x-omni-accesstoken"]);
    //     console.log("=================================");
        
    //     console.log("GET USER:");
    //     console.log(await api.call("test.getUser"));
    //     console.log("=================================");
        
    //     console.log("LOGOUT:");
    //     console.log(await api.auth.logout());
    //     console.log("=================================");
        
    //     console.log("LOGIN:");
    //     console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"));
    //     console.log("=================================");
        
    //     console.log("GET USER:");
    //     console.log(await api.call("test.getUser"));
    //     console.log("=================================");
        
    //     console.log("REFRESH:");
    //     console.log(await api.auth.refresh());
    //     console.log("=================================");
        
    //     console.log("GET USER:");
    //     console.log(await api.call("test.getUser"));
    //     console.log("=================================");
        
    //     console.log("LOGOUT:");
    //     console.log(await api.auth.logout());
    //     console.log("=================================");
        
    //     console.log("GET USER:");
    //     console.log(await api.call("test.getUser"));
    //     console.log("=================================");
        
    //     console.log("LOGIN:");
    //     console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"));
    //     console.log("=================================");
        
    //     console.log("GET USER:");
    //     console.log(await api.call("test.getUser"));
    //     console.log("=================================");
        
    //     console.log("Switch To Master Domain:");
    //     console.log(await api.auth.changeDomain("caviot-master"));
    //     console.log("=================================");
        
    //     console.log("GET USER:");
    //     console.log(await api.call("test.getUser"));
    //     console.log("=================================");
        
    //     console.log("REFRESH:");
    //     console.log(await api.auth.refresh());
    //     console.log("=================================");
        
    //     console.log("GET USER:");
    //     console.log(await api.call("test.getUser"));
    //     console.log("=================================");
        
    //     console.log("LOGOUT:");
    //     console.log(await api.auth.logout());
    //     console.log("=================================");
        
    //     console.log("GET USER:");
    //     console.log(await api.call("test.getUser"));
    //     console.log("=================================");
        
    //     console.log("LOGIN:");
    //     console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"));
    //     console.log("=================================");
        
    //     console.log("GET USER:");
    //     console.log(await api.call("test.getUser"));
    //     console.log("=================================");
        
    //     console.log("LOGOUT:");
    //     console.log(await api.auth.logout());
    //     console.log("=================================");
        
    //     console.log("GET USER:");
    //     console.log(await api.call("test.getUser"));
    //     console.log("=================================");
        
    //     console.log("LOGIN:");
    //     console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"));
    //     console.log("=================================");
        
    //     console.log("GET USER:");
    //     console.log(await api.call("test.getUser"));
    //     console.log("=================================");
    }
    catch (ex)
    {
        console.log("ERROR", ex);
    }
}

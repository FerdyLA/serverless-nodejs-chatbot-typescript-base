# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 167.71.220.115 (MySQL 5.7.30-0ubuntu0.18.04.1)
# Database: yois
# Generation Time: 2020-05-07 13:38:35 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Api
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Api`;

CREATE TABLE `Api` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(254) NOT NULL DEFAULT '',
  `name` varchar(254) NOT NULL DEFAULT '',
  `options` longtext NOT NULL,
  `callback_url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Api` WRITE;
/*!40000 ALTER TABLE `Api` DISABLE KEYS */;

INSERT INTO `Api` (`id`, `key`, `name`, `options`, `callback_url`)
VALUES
	(1,'Y325XUMNO58P0J5V053IAMR85YN6Q7KPH6HY2PMHT6FU3JQKUUHNXU9BFO45W0DJ','yois-main-web-sandbox','{\"tokens\":{\"accessDuration\":{\"minutes\":30},\"refreshDuration\":{\"minutes\":10080}},\"permissions\":{\"baseApiPerms\":[\"yois/access/guest\",\"function/listJob\",\"function/addJob\",\"function/editJob\",\"function/deleteJob\"],\"baseMemberPerms\":[\"yois/access/user\",\"function/createapikey\"]}}','http://ferdinand.sg/yois/callback.php');

/*!40000 ALTER TABLE `Api` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Domain
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Domain`;

CREATE TABLE `Domain` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(254) NOT NULL DEFAULT '',
  `key` varchar(254) NOT NULL DEFAULT '' COMMENT 'used for custom url',
  `desc` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


# Dump of table Member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Member`;

CREATE TABLE `Member` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `domainId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  `permission` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Registry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Registry`;

CREATE TABLE `Registry` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(254) NOT NULL DEFAULT '',
  `data` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Role`;

CREATE TABLE `Role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(254) NOT NULL DEFAULT '',
  `name` varchar(254) NOT NULL DEFAULT '',
  `data` longtext,
  `permission` longtext,
  `domainId` int(11) NOT NULL DEFAULT '0' COMMENT 'domainId or 0 for global',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Role` WRITE;
/*!40000 ALTER TABLE `Role` DISABLE KEYS */;

INSERT INTO `Role` (`id`, `key`, `name`, `data`, `permission`, `domainId`)
VALUES
	(1,'super-admin','Super Administrator',NULL,'[\"yois/access/admin\"]',0);

/*!40000 ALTER TABLE `Role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table telegram_bot
# ------------------------------------------------------------

DROP TABLE IF EXISTS `telegram_bot`;

CREATE TABLE `telegram_bot` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bot_name` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bot_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bot_payment_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bot_outlets` longtext COLLATE utf8mb4_unicode_ci,
  `stripe_key` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


# Dump of table telegram_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `telegram_user`;

CREATE TABLE `telegram_user` (
  `id` int(11) unsigned NOT NULL,
  `first_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `added` int(11) NOT NULL,
  `bot_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


# Dump of table telegram_user_action
# ------------------------------------------------------------

DROP TABLE IF EXISTS `telegram_user_action`;

CREATE TABLE `telegram_user_action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `action_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `action_cache` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed` int(11) NOT NULL DEFAULT '0',
  `bot_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


# Dump of table telegram_invoice
# ------------------------------------------------------------

DROP TABLE IF EXISTS `telegram_invoice`;

CREATE TABLE `telegram_invoice` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `telegram_user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(11,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `provider` varchar(254) NOT NULL DEFAULT 'stripe',
  `shipping_option_id` int(11) DEFAULT NULL,
  `order_info` longtext,
  `telegram_payment_id` varchar(254) DEFAULT NULL,
  `provider_payment_id` varchar(254) DEFAULT NULL,
  `paid` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


# Dump of table telegram_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `telegram_product`;

CREATE TABLE `telegram_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(254) DEFAULT NULL,
  `name` varchar(254) NOT NULL DEFAULT '',
  `description` text,
  `cost` decimal(11,2) NOT NULL,
  `reward_type` varchar(254) NOT NULL DEFAULT 'credit',
  `reward_amount` int(11) DEFAULT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `telegram_product` WRITE;
/*!40000 ALTER TABLE `telegram_product` DISABLE KEYS */;

INSERT INTO `telegram_product` (`id`, `key`, `name`, `description`, `cost`, `reward_type`, `reward_amount`, `is_active`)
VALUES
	(1,'packa','Pack A','SGD 10 for 125 credits',10.00,'credit',125,1),
	(2,'packb','Pack B','SGD 20 for 300 (250+50) credits',20.00,'credit',300,1),
	(3,'packc','Pack C','SGD 50 for 1000 (625+375) credits',50.00,'credit',1000,1),
	(4,'packd','Pack D','SGD 100 for 2500 (1250+1250) credits',100.00,'credit',2500,1);

/*!40000 ALTER TABLE `telegram_product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table telegram_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `telegram_settings`;

CREATE TABLE `telegram_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `last_update_id` int(11) DEFAULT NULL,
  `last_updated_time` datetime DEFAULT NULL,
  `onemap_token` text COLLATE utf8mb4_unicode_ci,
  `onemap_token_created` datetime DEFAULT NULL,
  `yoiso_last_id` int(11) DEFAULT NULL,
  `yoiso_last_time` datetime DEFAULT NULL,
  `referral_reward` decimal(11,2) NOT NULL DEFAULT '20.00',
  `signup_initial_credits` decimal(11,2) NOT NULL DEFAULT '100.00',
  `driver_commision` decimal(11,2) NOT NULL DEFAULT '0.05',
  `merchant_commission` decimal(11,2) NOT NULL DEFAULT '0.20',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `telegram_settings` WRITE;
/*!40000 ALTER TABLE `telegram_settings` DISABLE KEYS */;

INSERT INTO `telegram_settings` (`id`, `last_update_id`, `last_updated_time`, `onemap_token`, `onemap_token_created`, `yoiso_last_id`, `yoiso_last_time`, `referral_reward`, `signup_initial_credits`, `driver_commision`, `merchant_commission`)
VALUES
	(1,143934646,'2020-04-26 16:28:27','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQyMTgsInVzZXJfaWQiOjQyMTgsImVtYWlsIjoiZmVyZHlAdGVjaHRlZW0ubmV0d29yayIsImZvcmV2ZXIiOmZhbHNlLCJpc3MiOiJodHRwOlwvXC9vbTIuZGZlLm9uZW1hcC5zZ1wvYXBpXC92MlwvdXNlclwvc2Vzc2lvbiIsImlhdCI6MTU4NzYzNDQwMSwiZXhwIjoxNTg4MDY2NDAxLCJuYmYiOjE1ODc2MzQ0MDEsImp0aSI6IjZiY2NiNDAzMDMzZTNlZGNhZGI2YWFlODg3ODAwMDI5In0.mXchEhIMdrlxdprXPAxGqdxazdig3P98hOLOC-7E2wI','2020-04-25 17:42:00',264043816,'2020-04-25 16:22:32',20.00,150.00,0.05,0.20);

/*!40000 ALTER TABLE `telegram_settings` ENABLE KEYS */;
UNLOCK TABLES;


-- # Dump of table telegram_user
-- # ------------------------------------------------------------

-- DROP TABLE IF EXISTS `telegram_user`;

-- CREATE TABLE `telegram_user` (
--   `id` int(24) NOT NULL,
--   `user_id` int(24) DEFAULT '0',
--   `first_name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
--   `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
--   `added` int(24) NOT NULL,
--   `private_chat_id` int(24) NOT NULL,
--   `user_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
--   `user_credits` decimal(11,2) NOT NULL DEFAULT '100.00',
--   `bot_username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
--   `store_name` mediumtext COLLATE utf8mb4_unicode_ci,
--   `postal` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
--   `unit` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
--   `lat` decimal(20,10) DEFAULT NULL,
--   `lon` decimal(20,10) DEFAULT NULL,
--   `last_updated` datetime DEFAULT NULL,
--   `transport_mode` int(11) DEFAULT NULL,
--   `phone_number` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
--   `joined` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
--   `last_alert` datetime DEFAULT NULL,
--   `referred_by` int(11) DEFAULT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- # Dump of table telegram_user_action
-- # ------------------------------------------------------------

-- DROP TABLE IF EXISTS `telegram_user_action`;

-- CREATE TABLE `telegram_user_action` (
--   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
--   `user_id` int(24) NOT NULL,
--   `timestamp` int(24) NOT NULL,
--   `state` longtext CHARACTER SET utf8mb4 NOT NULL,
--   `cache` longtext CHARACTER SET utf8mb4 NOT NULL,
--   `executed` int(24) DEFAULT '0',
--   `completed` int(24) DEFAULT '0',
--   `bot_user` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;


# Dump of table User
# ------------------------------------------------------------

DROP TABLE IF EXISTS `User`;

CREATE TABLE `User` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(254) NOT NULL DEFAULT '',
  `email` varchar(254) NOT NULL DEFAULT '',
  `mobile` varchar(254) DEFAULT '',
  `pwdHash` text NOT NULL,
  `publicData` longtext NOT NULL,
  `privateData` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;

INSERT INTO `User` (`id`, `key`, `email`, `mobile`, `pwdHash`, `publicData`, `privateData`)
VALUES
	(1,'limhjf@icloud.com','','','Bg8/AAAnEHNoYTUxMiNFmhJ0LSQaEWDJgkMN5TjK5wmrWb32Rw2rEiNkmg4Wh3PWhJoG+OaSFAH6yRGHiQ3LbXPqaQpEtM//cKvoRFTXSDAhWgsq+iSSJbgQKhuB','{\"name\":\"Ferdinand Lim\"}','{}'),
	(2,'contact@ferdinand.sg','','','Bg8/AAAnEHNoYTUxMuXj35C8is8Yj2PDygA5LPGvsZfmyIMTCLrhyjsH0beU2tjzPsMUeImDxfS/2H1ofawwndGQgyHaWUb7SeKkuM8VQfuT3IuRLU6JokhL6lmH','{\"name\":\"Ferdy 2\"}','{}');

/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table UserRole
# ------------------------------------------------------------

DROP TABLE IF EXISTS `UserRole`;

CREATE TABLE `UserRole` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `roleId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `UserRole` WRITE;
/*!40000 ALTER TABLE `UserRole` DISABLE KEYS */;

INSERT INTO `UserRole` (`id`, `roleId`, `userId`)
VALUES
	(1,1,1);

/*!40000 ALTER TABLE `UserRole` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table UserSession
# ------------------------------------------------------------

DROP TABLE IF EXISTS `UserSession`;

CREATE TABLE `UserSession` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `refreshToken` text NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `log`;

CREATE TABLE `log` (
  `time` int(24) NOT NULL,
  `details` longtext NOT NULL,
  `bot_username` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table telegram_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `telegram_group`;

CREATE TABLE `telegram_group` (
  `id` int(24) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `joined` int(24) NOT NULL,
  `users` longtext NOT NULL,
  `settings` longtext NOT NULL,
  `cache` longtext NOT NULL,
  `bot_username` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

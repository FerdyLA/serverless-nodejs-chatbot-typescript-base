# Chariot Documentation

## 1. Basic Information

### 1.1 What is Chariot
Chariot is a Facilities Management System. It is used to manage assets and properties.

Chariot is used to manage all the Assets and ensure everything is accounted for and at a working condition.

IOT will be added in the future, with the same structure as Component, but with different tables to ensure better compatibility with the particular IOT.

In the current system, it is used to manage the maintenance of the user's assets, alerting users of maintenance windows and allowing other users to report on components that are not working or having issues.

In the future, Facilities Management and User Ratings can be added.

### 1.2 Language and Infrastrusture
- Language: Typescript
- Backend: Node.JS
- Server: AWS Lambda
- Database: AWS RDS (Aurora MySQL)

### 1.3 Connection Protocol
- Remote Procedure Call (RPC)
- RESTful: Yes

### 1.4 Terminologies used in this document
- Domain (Chariot): Domain is basically a group of assets, rights and users. Rules of a domain will only apply to that particular domain and do not cross with other.
- Service: Service is the class that the RPC/RESTful API will be able to call
- Method: Method belongs to a service and consist of the actual logic of any known task
- Permission (Chariot): Permission are User / User Roles / API specific permissions used to allow/deny access to Service Methods
- Roles (18r): Roles are the same as permissions in Chariot
- Roles (Chariot): Roles are assignable to users that hold a list of permissions shared by everyone in that User Role. User Roles are limited to ONLY a single domain


## 2. Logic Difference from 18 Robinson
** Refer to 18 Robinson Documentation for actual comparison

### 2.1 Domains

#### 2.1.1 What is a Domain used for?
Domains are used to keep track of groups of assets/users/permissions. A user can own multiple domains, each containing a particular property for example.

#### 2.1.2 Login (Handling Domains)
- When a user login initially, the user is authenticated without a domain. A list of domain is to be presented to the user. User has the choice to either choose a domain to proceed, or to create a new domain. Upon choosing a domain, user is to be re-authenticated with a new accessToken, with the selected domain in the records.
- DIFFERENCE: 
    1. 18 Robinson do not use domain and it is by default an empty string (""). Chariot uses domains.

### 2.2 Allow/Deny tags for each Service Method

#### 2.2.1 What are Allow/Deny tags?
Allow/Deny tags are used in Services to authorise users with the appropriate permissions to access that API (Service Method)

#### 2.2.2 How does the tags work?
- These tags are place above Service Methods to mark the allowing and denying any user with/without specified permissions
- Tags are specific strings that are used to match with permissions.
- Tags in the same allow() function serves as "AND"/"&&", whereas multiple lines of allow() tags on the same method serves as "OR"/"||".
- DIFFERENCE: 
   1. 18 Robinson tags are with Regex, whereas Chariot tags are strings. 
   1. 18 Robinson roles are strings, whereas Chariot permissions are Regex.
   1. 18 Robinson tags uses Regex, so multiple regex is required to get a perticular role, whereas Chariot permissions list all the possible roles, starting from the highest possible position, to the lowest (chariot/role/dev/sysadm/owner/admin/user/guest). By doing this, it allows all user to have access to this in 1 line.
   1. Chariot adds ("chariot/function/{servce}/{method}") tags to allow additional function access to specific User / User Role with the permission column in the respective DB table
- Example:
```ts
export class ExampleService extends Service
{
    // In this case, if user is not admin, but has been added the function permission "chariot/function/example/test" will still be able to access
    @allow("chariot/role/dev/sysadm/owner/admin") // role permission
    @allow("chariot/function/example/test") // alternate function permission
    public async test(print: string): Promise<string>
    {
        ...
    }

    // In this case, user needs either of these both.
    @allow("chariot/role/dev/sysadm/owner/admin","chariot/function/example/strictTest") // requires both role permission && function permission
    // OR
    @allow("chariot/api/carpark","chariot/function/example/strictTest") // requires both api permission && function permission
    public async strictTest(print: string): Promise<string>
    {
        ...
    }
}
```

### 2.3 Storage (S3)
- Storage in Chariot is uploaded/list/managed via client
- All upload/list/delete codes can be found on client side, but lacking the authentication to our S3.
- S3 details is retrieved from server, then filled in to the code and a direct Upload/List/Delete is called from the client side
- ***Future*** 18 Robinson logic is prefered. Please update code to use the 18 Robinson's logic

## 3. Service and Method Logics

### 3.1 Service

#### 3.1.1 What happens when the endpoint is called?
1. LAMBDA instance is spawned (if not already exist)
1. Service is initiated
1. If accessToken is passed in on header
    - accessToken is decoded to resume previous session
    - User details and permissions is loaded into memory
1. Service and Method is called
    - Checks if method's required permission in present in the session
    - If not present, error "Access Denied" will be thrown
    - if present, variables will be passed into method

#### 3.1.2 API Key and its importance
- A valid API key is **REQUIRED** to interface with our API.
- API key is used to identify which client the user is using.
- API key is also used for external APIs to connect to our service

### 3.2 Authentication ([AuthService](src/services/auth/AuthService.ts) & [Auth](src/services/auth/Auth.ts))

#### 3.2.1 Authentication
- accessToken is mainly used to authenticate the user.
- Upon login successful, client will receive an accessToken and a refreshToken
- Client should store the tokens in its local storage.
- If "Access Denied" error is received, client should load refreshToken and call auth.refresh().
- auth.verify() is used to check if accessToken is still valid.

#### 3.2.2 Encryption

##### 3.2.2.1 ACCESSTOKEN and REFRESHTOKEN
- Encryption is done with jwt.sign() and jwt.verify()
- accessToken and refreshToken are encryted with a secret key.

##### 3.2.2.2 PASSWORD
- Password is encrypted with Password class on Password.ts

### 3.3 Permissions ([PermissionService](src/services/permission/PermissionService.ts) & [Service](src/server/Service.ts))
- Permission can be granted and revoked on PermissionService.
- permission.list() will list all possible allocatable functions Permissions (using permission.listFunctionPermissions())
- permission.listByUser() should be moved to [AccountService.ts](src/services/user/AccountService.ts)
- permission.listFunctionPermissions() scans through all programmed function permissions (those with "chariot/function/...") and list them
- permission.listAllowedMethods() list all methods that the current user is allowed to access. You can use this to streamline the process of blocking users from even calling the service/method in the first place.
- All permission-based logic are found in ServiceContext under [Service.ts](src/server/Service.ts)

### 3.4 Account ([AccountService](src/services/user/AccountService.ts))
- AccountService is used to do all transaction pertaining to the current logged in user. i.e Change password, check permission, update profile etc

### 3.5 Invite ([InviteService](src/services/user/InviteService.ts))
- Invites is used to invite a user into the system.
- Invite uses a separate table from the users, containing the respective presets in the *data* column
- It is supposed to contain preset domain, roles, and rights to the system. It can also be used for general invites, with no presets
- Invite presets can include:
    - domainId (used to auto join a domain)
    - domain details (new domain)
    - domain assets (new domain)
    - domain roles (new domain or existing domain)
    - user specific permissions
    - ... more

### 3.6 Sign Up ([SignUpService](src/services/user/SignUpService.ts))
- Sign up is used to allow user to signup for a new account. The new account will then be able to create a domain or be invited to a domain. There is no plans on making domains public and joinable as this is supposed to be a closed domain.
- User not belonging to a domain will be regarded as a public user (guest) in that domain itself. They can use services that are opened to the public like feedback forms etc.

### 3.6 User ([UserService](src/services/user/UserService.ts))
- User is currently only used to load User Profiles

### 3.7 Device ([DeviceService](src/services/user/DeviceService.ts))
- Device service is used to register user device.
- Plan is to merge that into [AccountService](src/services/user/AccountService.ts)

### 3.8 Registry ([RegistryService](src/services/registry/RegistryService.ts))
- Registry service is used to modify system-wide configuration. This should be where fixed text can be stored/modified/added/removed. This service **should** be restricted to only dev/systemadm roles.

### 3.9 Storage ([StorageService](src/services/storage/StorageService.ts))
- storage.getParams allow client to get the S3 Params from server to perform the Upload/List/Delete operations.
- Other methods not working

### 3.10 Member vs Group ([MemberService](src/services/member/MemberService.ts) & [GroupService](src/services/member/GroupService.ts))

#### 3.10.1 Member
- Member is used to identify perticipants of a domain. If you are a participant of a domain, you are considered a member of the domain
- Member title holds the roles of a user and its respective role permissions
- A user can hold multiple memberships to a domain, giving it multiple roles with different permissions in a domain

#### 3.10.2 Group
- Group ***does not contain roles or permissions***. Group is used for group actions, like sending a notification to a group, getting a list of users in a group (i.e. listing all mechanics)
- A user can belong to multiple groups

### 3.11 Locations ([LocationService](src/services/location/LocationService.ts))
- Locations are used to set various location
- Locations are nestable. A location can have many sub-Locations and a sub-Location can have many sub-sub-Locations and so on. A location can **ONLY** have **ONE** parent Location
- Locations will be used by Components, and even facility bookings in the future updates

### 3.12 Components ([ComponentService](src/services/location/ComponentService.ts))
- A Component is any asset or items found in a location
- Components can only belong to a Location. Component cannot be without a Location. But a Location can be empty without any Component.
- A component can be anything, from an Air-Conditioner to a Lamp, from a TV to a Table.
- A component consist of a list of parts. This parts list is used during maintenance and also repairs.
- ***Future: Users should be able to see a list of past repairs and maintenance to the specific component, followed by the next maintenance window***
- ***Future: IOT is not a Component and should get its own service and category***

### 3.12 Component Categories ([ComponentCategoryService](src/services/location/ComponentCategoryService.ts))
- There are many types of Components, like TV, Bed, Lamp, Buggy, Table, A/C etc
- Each category includes a set of action and a list of possible issues. The list of issues is prompted to users when they are making a report on the Component. The action can be set to "notify all technicians (using group) of this issue".
- Categories are created such that a default action can be done when a user report an issue with the component. i.e. if the TV is broken, no matter it is LG TV, Toshiba TV or Samsung TV, they have the same possible problems like cracked screens, cannot turn on etc, and should alert a similar pool of technicians to fix them.
- In the case which a special sub-category is needed with a special team of technicians, please create another Component Category. i.e. Smart TV vs TV. Smart TV might have certain features that is different from TV and might need special team of technicians who knows how to fix the issue.

### 3.13 Tasks ([TaskService](src/services/task/TaskService.ts))
- When an issue is reported on the Component, a Task is created.
- Task can be auto assigned to a pre-defined user, a random user, or just set to notify all users in a group. When a task is assigned to a user, only the user and the manager can see this Task.
- Task that is unassigned can be seen by all users
- Users can tap into a task and start adding a progress to the Task, by adding photos, comments, or even select the parts changed.
- When a Task is completed, user can mark the task as completed
- ***Future: Various triggers can be customised in the flow of the tasks, like "When a progress is added", "When a Task is marked as completed" etc**

### 3.14 Dashboard ([DashService](src/services/task/DashService.ts))
- This is used to show the various dashboard items
- **NOTE**: ***This is incomplete***

## 4. User Flows (Sample use-cases)
*[refers to backend processes]

### 4.1 Invite
1. Navigate to Invite page
1. Enters email address of invitee
1. Chooses default role(s) and permission(s)
1. Click "Invite"
1. [Server receives info from backend]
1. [Adds record to invite table]
1. [Sends an email to specified email]
1. Invitee clicks on email link
1. Invitee login or signup (see Signup flow)
1. [Server loads user and invite and automatically populates table with roles, permissions, domains etc to the respective table]

### 4.1 Signup
1. Navigate to Signup on client
1. Enter all the required details
1. Click "Signup"
1. [Server saves user into respective tables]
1. [Server sends verification email to user]
1. User click on email link
1. [Email is marked as verified]

### 4.1 Login
1. Enter login credentials
1. [Server to look for invites with this email address]
1. [Server to populate tables with roles, permissions, domains etc from invite to the respective tables]
1. [accessToken & refreshToken are generated and sent to client]
1. [login successful]
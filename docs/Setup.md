
VPC needed to allow lambda to access RDS.
Currently RDS is set to open to any IP.


https://ap-southeast-1.console.aws.amazon.com/lambda/home?region=ap-southeast-1#/functions/omnitribe-18r-dev-rpc?tab=graph

When you enable VPC, your Lambda function will lose default internet access.
If you require external internet access for your function, ensure that your
security group allows outbound connections and that your VPC has a NAT gateway.


enabling VPC without NAT will make HID portal access to not work.

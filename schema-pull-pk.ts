import * as mysql from "promise-mysql";
import * as fs from "fs";

const schemaFile = "src/models/db/Schema.ts";

const connConfig: mysql.ConnectionConfig =
{
    host     : '{DATABASE_URL}',
    user     : '{DATABASE_USER}',
    password : '{DATABASE_PASSWORD}',
    database : '{DATABASE}',
    port     : 3306,
    
    supportBigNumbers: true,
    bigNumberStrings: true,
};

async function getConnection()
{
    return await mysql.createConnection(connConfig);
}


class DbInfo
{
    public map: Map<string, SchemaInfo>;
    
    public constructor()
    {
        this.map = new Map();
    }
    
    public schema(name: string): SchemaInfo
    {
        let x = this.map.get(name);
        if (!x) this.map.set(name, x = new SchemaInfo(name));
        return x;
    }
    
    public toString(): string
    {
        return `/**
 * This file is generated by running: npm run schema:pull
 * That script should be run whenever changes are made to the database.
 * Do not directly modify this file.
 */
import { Pk, Id } from "db/Types";

export interface Schema
{
${[...this.map.values()].map(x => x.toSchemaString()).join("\n")}
}

export const PK =
{
${[...this.map.values()].map(x => x.toKeysString()).join("\n")}
}

${[...this.map.values()].map(x => x.toNamespaceString()).join("\n\n")}
`;
    }
}

class SchemaInfo
{
    public name: string;
    public map: Map<string, TableInfo>;
    
    public constructor(name: string)
    {
        this.name = name;
        this.map = new Map();
    }
    
    public table(name: string): TableInfo
    {
        let x = this.map.get(name);
        if (!x) this.map.set(name, x = new TableInfo(this, name));
        return x;
    }
    
    public toNamespaceString()
    {
        return `export namespace ${this.name}
{
${[...this.map.values()].map(x => x.toString()).join("\n\n")}
}`;
    }
    
    public toSchemaString()
    {
        return `${[...this.map.values()].map(x => `    "${this.name}.${x.name}": ${this.name}.${x.name};`).join("\n")}`;
    }
    
    public toKeysString()
    {
        return `${[...this.map.values()].map(x => `    "${this.name}.${x.name}": { ${x.toKeysString()} },`).join("\n")}`;
    }
}

class TableInfo
{
    public schema: SchemaInfo;
    public name: string;
    public map: Map<string, ColumnInfo>;
    
    public constructor(schema: SchemaInfo, name: string)
    {
        this.schema = schema;
        this.name = name;
        this.map = new Map();
    }
    
    public column(name: string): ColumnInfo
    {
        let x = this.map.get(name);
        if (!x) this.map.set(name, x = new ColumnInfo(name));
        return x;
    }
    
    public toString(): string
    {
        return `    export interface ${this.name}
    {
${[...this.map.values()].map(x => x.toString()).join("\n")}
    }
    
    export const ${this.name} = "${this.schema.name}.${this.name}";`;
    }
    
    public toKeysString(): string
    {
        let keys = [...this.map.values()].filter(x => x.isPrimaryKey);
        return `${keys.map(k => `${k.name}: ""`).join(", ")}`;
    }
}

class ColumnInfo
{
    public name: string;
    public type: string;
    public nullable: boolean;
    
    public constructor(name: string)
    {
        this.name = name;
        this.type = "!";
        this.nullable = true;
    }
    
    public get isPrimaryKey()
    {
        return this.type === "Pk";
    }
    
    public getDefaultValue()
    {
    }
    
    public getType(key: string, ctype: string, dtype: string): string
    {
        if (key === "PRI")
        {
            return "Pk";
        }
        else if (key === "MUL")
        {
            return "Id";
        }
        else
        {
            switch (ctype)
            {
                case "tinyint(1)": return "boolean";
            }
            
            switch (dtype)
            {
                case "bigint": return "string";
                case "mediumint": return "int";
                case "smallint": return "int";
                case "int": return "int";
                case "tinyint": return "int";
                
                case "decimal": return "float";
                case "float": return "float";
                case "double": return "float";
                
                case "longtext": return "string";
                case "mediumtext": return "string";
                case "text": return "string";
                case "tinytext": return "string";
                
                case "varchar": return "string";
                case "json": return "any";
                
                case "date": return "Date";
                case "time": return "Date";
                case "datetime": return "Date";
                case "timestamp": return "Date";
                
                case "enum":
                    let t = ctype.replace(/'/g, '"');
                    let e = JSON.parse("[" + t.substring(5, t.length - 1) + "]") as string[];
                    return e.map(v => JSON.stringify(v)).join(" | ");
                
                default:
                    throw new Error(`Unhandled type: ${dtype}`);
            }
        }
    }
    
    public set(key: string, ctype: string, dtype: string, nullable: string)
    {
        this.type = this.getType(key, ctype, dtype);
        this.nullable = nullable !== 'NO';
    }
    
    public toString(): string
    {
        if (this.nullable)
            return `        ${this.name}: ${this.type} | null;`;
        else
            return `        ${this.name}: ${this.type};`;
    }
}

async function fetchDbInfo()
{
    let db = new DbInfo();
    let conn = await getConnection();
    let results = await conn.query(`
        SELECT
            TABLE_SCHEMA \`schema\`, TABLE_NAME \`table\`,
            COLUMN_NAME \`column\`, COLUMN_KEY \`key\`, COLUMN_TYPE ctype, DATA_TYPE dtype, IS_NULLABLE nullable
        FROM information_schema.columns
        WHERE TABLE_SCHEMA NOT IN ('mysql', 'performance_schema', 'information_schema', 'sys');
    `) as { schema: string, table: string, column:string, key: string, ctype: string, dtype: string, nullable: string }[];
    
    for (let r of results)
    {
        db.schema(r.schema).table(r.table).column(r.column).set(r.key, r.ctype, r.dtype, r.nullable);
    }
    
    conn.end();
    return db;
}

async function main()
{
    let dbInfo = await fetchDbInfo();
    
    fs.writeFileSync(schemaFile, dbInfo.toString(), "utf8");
    console.log(`Saved to ${schemaFile}`);
}

main();

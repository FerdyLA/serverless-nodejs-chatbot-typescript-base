import { VerifyResult, AccessTokenData } from "models/api/Auth";
import { getAccessSecret } from "services/auth/Config";
import * as jwt from "jsonwebtoken";
import { ApiKeyOptions } from "models/api/Api";
import { Table, Data, schema } from "db/Database";

export async function verify(projectKey: string, domainKey: string, accessToken: string): Promise<VerifyResult>
{
    let decoded: AccessTokenData = jwt.verify(accessToken, getAccessSecret(projectKey, domainKey)) as any;
    return { user: decoded.user };
}

export async function fetchApiKeyOptions(apiKey: string): Promise<ApiKeyOptions>
{
    // let Api = Table.from(schema.cah.Api);
    // let api = await Api.pick({ key: apiKey });
    // if (!api) throw "Invalid API Key";
    // return JSON.parse(api.options);
        throw "NOT IMPLEMENTED";
}

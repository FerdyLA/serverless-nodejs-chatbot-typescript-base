
export const tokenConfig =
{
    accessSecret: "asdasda",
    accessDuration: { minutes: 15 },
    
    refreshSecret: "asdasda",
    refreshDuration: { minutes: 15 },
};

export const passwordConfig =
{
    saltLength: 128 / 8,    // 64 bits minimum
    hashLength: 512 / 8,    // 32 bits minimum (64 bytes for sha512, 32 bytes for sha256)
    iterations: 10000,      // as high as possible without impacting performance
    digest: "sha512",
};

export function getAccessSecret(project: string, domain: string)
{
    return `${project}/${domain}/access/asdfasdfasdf`;
}

export function getRefreshSecret(project: string, domain: string)
{
    return `${project}/${domain}/refresh/asdfasdfasdf`;
}

import { Service } from "server/Service";
import { Table, Data, schema } from "db/Database";
import { SignUp } from "models/app/SignUp";
import { passwordConfig } from "services/auth/Config";
import { Password } from "services/auth/Password";
import { Email } from "services/email/Email"
import { getSignupTemplete } from "services/email/EmailTemplate";
import { allow, deny } from "server/Decorators";
import { sign } from "jsonwebtoken";

// ProjectID = 2, DomainID = 3

/** Only creates account. Doesnt modify */
export class SignupService extends Service
{
    @allow("all")
    public async createAccount(info: SignUp): Promise<boolean>
    {
        let User = Table.from(schema.yois.User);
        
        let pwdHash = await Password.hash(info.password, passwordConfig);
        
        let privateData = {} as any;
        let publicData = {} as any;
        if (info.data) 
        {
            for (const dataKey of Object.keys(info.data))
            {
                if (dataKey=="name") publicData[dataKey] = info.data[dataKey];
                else if (dataKey=="userId") publicData["invitedByUserId"] = info.data[dataKey];
                else privateData[dataKey] = info.data[dataKey];
            }
        }
        
        await User.save({
            key: info.email,
            pwdHash: pwdHash,
            publicData: JSON.stringify(publicData),
            privateData: JSON.stringify(privateData),
        });
        
        let user = await User.pick({ key: info.email });

        if (user)
        {
            let name = (info.data.name != undefined && info.data.name.trim() != "") ? info.data.name : "Sir/Madam";
            
            try
            {
                let template = getSignupTemplete(name);
                let reply = await Email.send(info.email, template);
                // return reply.MessageId != undefined;
                // deleteInviteIfInvited(info.email)
                return true;
            }
            catch (ex)
            {
                throw ex;
            }
        }
        
        throw new Error("Account cannot be created.");
    }
    
    @allow("all")
    public async emailVerification(email: string): Promise<boolean>
    {
        let regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        let result = regexp.test(email);
        if (!result) throw new Error("Email format incorrect");
        
        let User = Table.from(schema.yois.User);
        let user = await User.pick({ key: email });
        if (user) throw new Error("Email exists. Please enter another email or login with this email.");
        
        return true;
    }
}

export const rpc = Service.rpc(SignupService);

// async function deleteInviteIfInvited(email: string): Promise<boolean>
// {
//     let Invite = Table.from(schema.yois.Invite);
//     let result = await Invite.remove({ key: email })>0;
//     return true;
// }
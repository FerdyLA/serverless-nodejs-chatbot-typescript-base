/** use getUpdates to get Update from Telegram 
 * offset int?
 * limit int?
 * timeout int?
 * allowed_updates string[]?
 * 
 * use setWebhook to specify URL to receive incoming updates containing Update (JSON serialised)
 * url string!
 * certificate InputFile?
 * max_connections int?
 * allowed_updates string[]?
 * 
 * use deleteWebhook to remove webhook integration
 * 
 * use getWebhookInfo to get current webhook status
 * 
 * */ 
import axios, { AxiosInstance } from "axios";
import { TelegramReply } from "telegram/TelegramObjects";
import { Telegram } from "telegram/Telegram";
import { schema } from "db/Query";
import { Table } from "db/Table";

export class OneMap
{
    Token: string = "";

    public async init(): Promise<boolean>
    {
        let SettingsTable = Table.from(schema.yois.telegram_settings);
        let settings = (await SettingsTable.filter().list()).pop();
        if (!settings) return false;
        if (!settings.onemap_token || (settings.onemap_token_created && (Date.now()-settings.onemap_token_created.getTime()>(60*60*24*2)*1000)))
        {
            var options = {
                method: 'POST',
                uri: "https://developers.onemap.sg/privateapi/auth/post/getToken",
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
                },
                formData: {
                    email: process.env.ONEMAP_USER,
                    password: process.env.ONEMAP_PASS
                },
            };
            
            const rp = require('request-promise');
            let response = await rp(options);

            // let reply: TelegramReply = {
            //     text: "OneMap response: "+JSON.stringify(response),
            //     telegram_chat_id: 1156778274
            // }
            // let msgObj = await Telegram.replyToMessageObj(reply);
            // await Telegram.sendMessage(msgObj);

            if (response) 
            {
                let responseObj: OneMapTokenResponse = JSON.parse(response);
                settings.onemap_token = responseObj.access_token;
                settings.onemap_token_created = new Date();
                await SettingsTable.save(settings);
                this.Token = responseObj.access_token;

                return true;
            }

            // var request = require("request");

            // var options = { 
            //     method: 'POST',
            //     url: 'https://developers.onemap.sg/privateapi/auth/post/getToken',
            //     headers: 
            //     { 
            //         'cache-control': 'no-cache',
            //         'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' 
            //     },
            //     formData: { 
            //         email: process.env.ONEMAP_USER, 
            //         password: process.env.ONEMAP_PASS 
            //     } 
            // };

            // let response: OneMapTokenResponse = request(options, function (error: any, response: any, body: any) {
            //     return body;
            // });
            // if (response) 
            // {
            //     settings.onemap_token = response.access_token;
            //     settings.onemap_token_created = new Date();
            //     this.Token = response.access_token;
            // }

            return false;
        }
        else    
            this.Token = settings.onemap_token;

        return true;
    }

    public async getDistance(lat1: number, lon1: number, lat2: number, lon2: number, mode: "walk"|"drive"|"cycle" = "drive"): Promise<number>
    {
        if (!this.Token) await this.init();
        if (!this.Token) throw "No Token";

        var options = {
            method: 'GET',
            uri: "https://developers.onemap.sg/privateapi/routingsvc/route?start="+lat1+","+lon1+"&end="+lat2+","+lon2+"&routeType="+mode+"&token="+this.Token,
        };
        
        const rp = require('request-promise');
        let response = await rp(options);
        // let reply: TelegramReply = {
        //     text: response,
        //     telegram_chat_id: 1156778274
        // }
        // let msgObj = await Telegram.replyToMessageObj(reply);
        // await Telegram.sendMessage(msgObj);
        if (response) 
        {
            let responseObj: OneMapDistanceResponse = JSON.parse(response);
            if (responseObj && responseObj.route_summary && responseObj.route_summary.total_distance) return responseObj.route_summary.total_distance/1000.0;
        }

        throw "Cannot find distance";
    }

    public async getPostalCodeLocation(postal: string): Promise<string[]>
    {
        let ajax: AxiosInstance = axios.create(
            {
                baseURL: "https://developers.onemap.sg"
            });
        let result = await ajax.request(
            {
                url: `/commonapi/search?searchVal=${postal}&returnGeom=Y&getAddrDetails=Y&pageNum=1`,
                method: "get"
            });
        let reply: OneMapReply = result.data;

        if (reply.results.length>0) 
        {
            let result = reply.results[0];

            return [result.LATITUDE,result.LONGITUDE,result.SEARCHVAL,result.ADDRESS];
        }

        return [];
    }
}

export interface OneMapReply
{
    found: number;
    totalNumPages: number;
    pageNum: number;
    results: OneMapResult[]
}

export interface OneMapTokenResponse
{
    access_token: string;
    expiry_timestamp: string;
}

export interface OneMapDistanceResponse
{
    status_message: string;
    alternative_names: string[];
    route_name: string[];
    route_geometry: string; // don't use
    route_instructions: string[][];
    alternative_summaries: OneMapSummaryObject[];
    via_points: number[][];
    route_summary: OneMapSummaryObject;
    found_alternative: boolean;
    status: string;
    via_indices: number[];
    hint_data: OneMapHintObject;
    alternative_geometries: string; // don't use
    alternative_instructions: string[][][];
    alternative_indices: number[];
}

export interface OneMapHintObject
{
    locations: string[];
    checksum: number[];
}

export interface OneMapSummaryObject
{
    end_point: string;
    start_point: string;
    total_time: number; // in seconds
    total_distance: number; // in meters
}

export interface OneMapResult
{
    SEARCHVAL: string;
    BLK_NO: string;
    ROAD_NAME: string;
    BUILDING: string;
    ADDRESS: string;
    POSTAL: string;
    X: string;
    Y: string;
    LATITUDE: string;
    LONGITUDE: string;
    LONGTITUDE: string;
}
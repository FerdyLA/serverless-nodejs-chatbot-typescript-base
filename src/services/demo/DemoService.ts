/**
 * This project is create by Ferdy http://ferdy.sg
 * The base code for the RPC model for serverless is done by Munir
 * 
 * This service is used to receive only message-containing Telegram Updates, process it, and send a reply
 * All group replies are immediate, but private chat replies are queued for better Q&A
 * Private Chat Queues are best for asking questions and waiting for answers. 
 * For messages that do not require any answers, just send using sendMessage function.
 */

import { Service } from "server/Service";
import { Table, Data, schema } from "db/Database";
import { allow, deny } from "server/Decorators";
import { Telegram } from "telegram/Telegram";
import { TelegramReply } from "telegram/TelegramObjects";
import { TelegramMessage } from "telegram/TelegramAPIObjects";
import { TelegramBotModel } from "telegram/TelegramBotModel";

export let TELEGRAMURL: string;
let BOTTOKEN: string;
export let BOTUSER: string;

export const MAXKEYBOARDOPTIONS: float = 20.0;

export class DemoService extends Service
{
    @allow("all")
    public async telegram(input: any): Promise<void>
    {
        let Game = new DemoTelegramBot(BOTUSER);
        
        try 
        {
            // init
            if (!process.env.TELEGRAM_TOKEN_CAH) 
            {
                await Game.log("No Bot Token");
                throw new Error("No Bot Token");
            }
            if (!process.env.TELEGRAMBOT_USERNAME_CAH) 
            {
                await Game.log("No Bot Username");
                throw new Error("No Bot Username");
            }
            BOTTOKEN = process.env.TELEGRAM_TOKEN_CAH;
            BOTUSER = process.env.TELEGRAMBOT_USERNAME_CAH;
            TELEGRAMURL = "https://api.telegram.org/bot"+BOTTOKEN+"/";
            // end init

            // check if input is an update from telegram
            if(Telegram.isInputUpdate(input))
            {
                // await Game.log("Is Input Update");
                let message: TelegramMessage | undefined = input.message;
                // check if message exists
                if (message)
                {
                    if (message.text && message.text.charAt(0)==="/")
                    {
                        await Game.commandLogic(message);
                    }
                    else if (message.new_chat_members)
                    {
                        await Game.memberJoin(message);
                    }
                    else if (message.left_chat_member)
                    {
                        await Game.memberLeft(message);
                    }
                    else if (message.group_chat_created)
                    {
                        await Game.groupJoin(message);
                    }
                    else 
                    {
                        await Game.checkReply(message);
                    }
    
                    // check and execute user's next pending action
                    if (message.from) await Telegram.checkExecutePendingUserAction(message.from.id);
                }
            }
        }
        catch (ex)
        {
            await Game.log("There is an error: "+JSON.stringify(ex));
        }
    }
}
export const rpc = Service.rpc(DemoService);

export class DemoTelegramBot extends TelegramBotModel
{
    public async commandLogic(message: TelegramMessage): Promise<void>
    {
        if (message.text)
        {
            let reply: string = "";
            let msgs = message.text.indexOf("@") ? message.text.split("@") : [message.text];
            let msg = msgs[0];
            switch (msg) {
                // the usual /help command
                case "/help":
                    let possiblecommands: string[] = [];
                    if (message.chat.type==="private")
                    {
                        possiblecommands = ["- /help: Hmmm. What special powers do I have?",
                        "- /start: Just to begin talking to me.",
                        "- /status: Am I still alive? How many chats I have?",
                        ];
                    } 
                    else if (message.chat.type==="group")
                    {
                        possiblecommands = ["- /help: Soooo... What special powers do I have?",
                        "- /status: Am I still alive? How many chats I have?",
                        ];
                    }
                    reply += "Hmmm. Let's see. I can understand:\n"+possiblecommands.join("\n")+"\n\nTry it!";
                    break;
                // /start command is called when user initiate a private chat. SAVE THE USER DATA HERE
                case "/start":
                    if (message.chat.type==="group")
                    {
                        reply += "Sadly, this is a group chat. Type /start to me @"+this.botUser+" in the private chat to initiate the love.";
                    } 
                    else if (message.chat.type==="private" && message.from)
                    {
                        let UserTable = Table.from(schema.chatbots.telegram_user);
                        let you = await UserTable.pick({ private_chat_id: message.chat.id });
                        if (you) 
                        {
                            you.private_chat_id = message.chat.id;
                            await UserTable.save(you);
                            reply += "Hey "+(message.from ? message.from.username ? "@"+message.from.username : message.from.first_name : "weirdo")+"! have you forgotten that we have started the conversation?";
                        }
                        else 
                        {
                            await UserTable.save({ id: message.from.id, first_name: message.from.first_name, username: message.from.username, added: Date.now() / 1000, private_chat_id: message.chat.id, bot_username: this.botUser });
                            reply += "Ok "+(message.from ? message.from.username ? "@"+message.from.username : message.from.first_name : "weirdo")+". Let's begin our hours-long chat!";
                        }
                    }
                    else
                    {
                        reply += "Not sure where you want to begin this chat with, but try typing /start to me @"+this.botUser+" in the private chat to initiate the love.";
                    }
                    break;
                // I usually use this for chatbot status. Kind of like, are you alive, or how many group chats you have?
                case "/status":
                    let ChatTable = Table.from(schema.chatbots.telegram_group);
                    let chat_counts = await ChatTable.count();
                    reply += "*lub-dub* *lub-dub* *lub-dub*\n\nHey "+(message.from ? message.from.username ? "@"+message.from.username : message.from.first_name : "weirdo")+", I'm still kick'in.\n\nI am current ";
                    reply += chat_counts>25 ? "busy with "+chat_counts+" number of group chats. So SHHUSH! I don't have time for you." : chat_counts>0 ? "extremely free as I only have "+chat_counts+" group chat(s)." : "bored as there isn't any group chats that I am in.\n\n*draw circles in a corner*";
                    reply += "\n\nI was last patched by @FerdyLA at "+process.env.LAST_DEPLOYED;
                    break;
                default:
                    reply += "Sorry, I'm still learning. I have not idea what command "+msg+" was.\n\nTo find out what I can understand, try typing /help";
                    break;
            }
    
            let telegramReply: TelegramReply = {
                text: reply,
                telegram_message: message
            }
            let msgObj = await Telegram.replyToMessageObj(telegramReply);
            await Telegram.sendMessage(msgObj);
        }
    }

    public async memberJoin(message: TelegramMessage): Promise<void>
    {
        if (message.new_chat_members)
        {
            let reply: TelegramReply = {
                text: "",
                telegram_message: message
            }
            if (reply.telegram_message)
            {
                for (const user of message.new_chat_members) 
                {
                    if (user.username && user.username===this.botUser)
                    {
                        reply["text"] = "I am "+user.first_name+".\n\nThanks for inviting me to ";
                        reply['text'] += reply.telegram_message.chat.title || "your Group Chat";
                        reply['text'] += ".\n\nTo begin chatting with me, type /start\n\n";
                    }
                }
                if (reply['text']==="")
                {
                    let users = [];
                    for (const user of message.new_chat_members) 
                    {
                        if (!user.username || user.username!==this.botUser)
                        {
                            users.push(user.username ? "@"+user.username : user.first_name);
                        }
                    }
                    reply['text'] = "Hi ";
                    reply['text'] += users.join("  ");
                    reply['text'] += ",\n\nWelcome to "+(reply.telegram_message.chat.title || "our Group Chat")+".\n\nType /start to me @"+this.botUser+" in the private chat to initiate the love.";
                }
                let msgObj = await Telegram.replyToMessageObj(reply);
                await Telegram.sendMessage(msgObj);
            }
        }
    }

    public async memberLeft(message: TelegramMessage): Promise<void>
    {
        if (message.left_chat_member)
        {
            if (!message.left_chat_member.username || message.left_chat_member.username!==this.botUser)
            {
                let reply: TelegramReply = {
                    text: "",
                    telegram_message: message
                }
                await Telegram.removeUserFromGroup(message.left_chat_member, message.chat);
                reply['text'] += "Awww. "+(message.left_chat_member.username ? "@"+message.left_chat_member.username : message.left_chat_member.first_name)+" have left the building. Invite him/her back!!! I miss "+(message.left_chat_member.username ? "@"+message.left_chat_member.username : message.left_chat_member.first_name)+" already! T.T";
                let msgObj = await Telegram.replyToMessageObj(reply);
                await Telegram.sendMessage(msgObj);
            }
        }
    }

    public async groupJoin(message: TelegramMessage): Promise<void>
    {
        if (message.text)
        {
            let reply: TelegramReply = {
                text: "",
                telegram_message: message
            }
            reply['text'] += "Hurray! A new Group Chat has been created!\n\nMy spidey sense tells me we are going to have loads of fun!\n\nTo begin chatting with me, type /start @"+this.botUser+" in the private chat to initiate the love.\n\n";
            let msgObj = await Telegram.replyToMessageObj(reply);
            await Telegram.sendMessage(msgObj);
        }
    }

    public async checkReply(message: TelegramMessage): Promise<void> 
    { 
        if (message.chat.type=="private")
        {
            let reply: TelegramReply = {
                text: message.text ? "I don't understand \""+message.text+"\"\n\n" : "",
                telegram_message: message,
                quote: true
            }

            // TODO: Process the message here and create your own replies accordingly

            let msgObj = await Telegram.replyToMessageObj(reply);
            await Telegram.sendMessage(msgObj);
        }
    }
}
import * as AWS from "aws-sdk";

const SES_SOURCE = "ferdy@techteem.sg";
const SES_REGION = "ap-southeast-2";
const SES_VERSION = "latest";

export interface EmailTemplate
{
    subject: string;
    html: string;
    text: string;
}

export namespace Email
{
    /** send emails using SES<br><br>
    toEmail: Email of Recepient<br>
    subject: Subject of Email<br>
    bodyHTML: HTML within &lt;body&gt; tag only. Use &lt;p&gt; &lt;br&gt; &lt;b&gt; etc<br>
    bodySimple: use \\n for next line etc */
    export async function send(recepientEmail: string, template: EmailTemplate): Promise<any>
    {
        try
        {
            AWS.config.update({ region: SES_REGION });
            
            // Create sendEmail params
            let params =
            {
                Destination: // required
                {
                    CcAddresses: [],
                    ToAddresses: [recepientEmail],
                },
                Message: // required
                {
                    Body: // required
                    {
                        Html:
                        {
                            Charset: "UTF-8",
                            Data: `<html><body>${template.html}</body></html>`
                        },
                        Text:
                        {
                            Charset: "UTF-8",
                            Data: template.text
                        }
                    },
                    Subject:
                    {
                        Charset: "UTF-8",
                        Data: template.subject,
                    }
                },
                Source: SES_SOURCE, // required
                ReplyToAddresses: [],
            };
            
            // Create the promise and SES service object
            let data = await new AWS.SES({ apiVersion: SES_VERSION }).sendEmail(params).promise();
            return data;
        }
        catch (ex)
        {
            throw ex;
        }
    }
}
import { EmailTemplate } from "services/email/Email";
import { User,Member,Role,Domain } from "models/app/User"

export function getInvitationTemplate(recepientName: string, inviter: User, role: Role|undefined): EmailTemplate
{
    let roleText = role ?  " as a "+role.name : ""
    if (!inviter.members) throw new Error("No Domain present");
    let subject = `Invitation to use YOIS`;

    let domainName = "YOIS";
    let domain = inviter.members[0].domain;
    if (domain) domainName = domain.name;
    
    let html =
`Dear ${recepientName},<br>
<br>
You have been invited by ${inviter.name} to use YOIS app/web to access ${domainName}${roleText}. Please download the app in the links below:<br>
<br>
<a href="http://yois.techteem.io">Website</a><br>
<br>
Please sign up/login with this email address to ensure the appropriate rights is given.<br>
<br>
Regards,<br>
System Administrator
`;

    let text =
`Dear ${recepientName},

You have been invited by ${inviter.name} to use YOIS app/web to access ${domainName}${roleText}. Please download the app in the links below:

<a href="http://yois.techteem.io">Website</a>

Please sign up/login with this email address to ensure the appropriate rights is given.

Regards,
System Administrator
`;
    return { subject, html, text };
}


export function getSignupTemplete(recepientName: string): EmailTemplate
{
    let subject = "YOIS: Account Created";
    
    let html =
`Dear ${recepientName},<br>
<br>
Your account for YOIS app/web has been successfully created. Please download the app in the links below:<br>
<br>
<a href="http://yois.techteem.io">Website</a><br>
<br>
Please sign in/login with this email address using the password you entered during the signup process.<br>
<br>
Regards,<br>
System Administrator
`;
    
    let text =
`Dear ${recepientName},

Your account for YOIS app/web has been successfully created. Please download the app in the links below:

<a href="http://yois.techteem.io">Website</a>

Please sign in/login with this email address using the password you entered during the signup process.

Regards,
System Administrator
`;
    
    return { subject, html, text };
}

export function getResetPasswordTemplate(recepientName: string, token: string, expiry: string): EmailTemplate
{
    let subject = "YOIS: Reset Password";
    
    let html =
`Dear ${recepientName},<br>
<br>
You are receiving this emasil because you requested a password reset for your YOIS account. If you did not request this change, you can safely ignore this email.<br>
<br>
To choose a new password, please follow the link below (link expires: ${expiry}):<br>
[url]/${token}<br>
<br>
Regards,<br>
System Administrator
`;
    
    let text =
`Dear ${recepientName},

You are receiving this email because you requested a password reset for your YOIS account. If you did not request this change, you can safely ignore this email.

To choose a new password, please follow the link below (link expires: ${expiry}):
[url]/${token}

Regards,
System Administrator
`;
// TODO: Replace [url]
    
    return { subject, html, text };
}

export function getPasswordChangedTemplate(recepientName: string): EmailTemplate
{
    let subject = "YOIS: Password Changed";
    
    let html =
`Dear ${recepientName},<br>
<br>
Password has been change for your YOIS account. If this action is not done by you, please reset your account password at the link below:<br>
[url]<br>
<br>
Regards,<br>
System Administrator
`;
    
    let text =
`Dear ${recepientName},

Password has been change for your YOIS account. If this action is not done by you, please reset your account password at the link below:
[url]

Regards,
System Administrator
`;
// TODO: Replace [url]
    
    return { subject, html, text };
}
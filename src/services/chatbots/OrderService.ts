import axios, { AxiosInstance } from "axios";
import { Service } from "server/Service";
import { allow } from "server/Decorators";
import { TelegramReply, TelegramReplyKeyboard, CustomerActionCache, CustomerOrder, Category, ItemAddOn, PayNowConfig } from "telegram/TelegramObjects";
import { TelegramUpdate, TelegramAnswerPreCheckOutQueryObj, TelegramSendInvoiceObj, TelegramSendMessageObj, TelegramReplyKeyboardRemove, TelegramInlineKeyboardMarkup, TelegramReplyKeyboardMarkup, TelegramKeyboardButton, TelegramUser, TelegramMessage, TelegramChat, TelegramDeleteMessageObj, TelegramEditMessageObj, TelegramSendLocationObj, TelegramSendPhotoObj, LabeledPrice } from "telegram/TelegramAPIObjects";
import { Table, Data } from "db/Table";
import { schema } from "db/Query";
import { OneMap } from "services/onemap/OneMap";
import { Telegram } from "telegram/Telegram";
import { ApiResult } from "models/api/Api";

const rp = require('request-promise');

type BotTokenType = {
    [key: string]: string
}
interface RESTAURANTCHOICE
{
    distance: number;
    text: string;
}

let TELEGRAMURL: string;
let BOTUSER: string;
let BOTSTRIPEKEY: string;
let BOTTOKENS: BotTokenType = {}
let BOTPAYMENTKEY: string;

let CUSTOMER: Data<schema.yois.telegram_customer>;
let RESTAURANT: Data<schema.yois.Restaurant> | null;
let OWNER: Data<schema.yois.telegram_user> | null;
let PENDINGACTION: Data<schema.yois.telegram_customer_action> | null;

let CHAT: TelegramChat;

let RESTAURANTTABLE = Table.from(schema.yois.Restaurant);
let RESTAURANTITEMTABLE = Table.from(schema.yois.RestaurantItem);
let RESTAURANTCATEGORYTABLE = Table.from(schema.yois.RestaurantCategory);
let CUSTOMERTABLE = Table.from(schema.yois.telegram_customer);
let PENDINGACTIONTABLE = Table.from(schema.yois.telegram_customer_action);
let USERTABLE = Table.from(schema.yois.telegram_user);
let USERACTIONTABLE = Table.from(schema.yois.telegram_user_action);
let ORDERTABLE = Table.from(schema.yois.Order);
let BOTTABLE = Table.from(schema.yois.telegram_bot);

export class OrderService extends Service
{

    @allow("function/alertCustomer")
    public async alertCustomer(order_id: string)
    {
        try 
        {
            let order = await ORDERTABLE.pick({ id: order_id })
            if (order)
            {
                BOTUSER = order.bot_username;
                let bot = await BOTTABLE.pick({ bot_name: order.bot_username });
                if (!bot) throw "Unable to find the bot in our system";
                TELEGRAMURL = "https://api.telegram.org/bot"+bot.bot_token+"/";

                let message = "Order #"+order_id+" status has been changed to: <b>";
                if (order.status==-1) message += "Cancelled by you\n";
                else if (order.status==-2) message += "Cancelled by Merchant\n";
                else if (order.status==-3) message += "Cancelled as unable to find Driver\n";
                else if (order.status==-4) message += "Delivery Failed\n";
                else if (order.status==1) message += "Waiting for Merchant to accept\n";
                else if (order.status==2 && order.ready==0) message += "Preparing/Packing/Waiting for Collection\n";
                else if (order.status==2) message += "Prepared and Packed, Ready for Collection\n";
                else if (order.status==3) message += "Collected by Driver\n";
                else if (order.status==4) message += "Delivered Successfully\n";
                else if (order.status==5 && order.ready==0) message += "Preparing/Packing\n";
                else if (order.status==5) message += "Prepared and Packed, Ready for Collection\n";

                if (order.status==5) message += "</b>\n\n<i>Click on the location below to find your way to the Merchant with your preferred app.</i>";
                else message += "</b>";
                let reply2: TelegramReply = {
                    text: message,
                    telegram_chat_id: order.telegram_customer
                }
                let msgObj2 = await OrderTelegram.replyToMessageObj(reply2);
                let result = await OrderTelegram.sendMessage(msgObj2);
    
                if (result && order.status==5)
                {
                    let resultMessage = result.result;

                    let restaurant = await RESTAURANTTABLE.pick({ id: String(order.restaurant_id) });
                    if (restaurant)
                    {
                        let owner = await USERTABLE.pick({ id: String(restaurant.creator_telegram) });
                        if (owner && owner.postal)
                        {
                            let oneMap = new OneMap();
                            let loc = await oneMap.getPostalCodeLocation(owner.postal);
                    
                            await OrderTelegram.sendLocation({
                                chat_id: order.telegram_customer,
                                reply_to_message_id: resultMessage.message_id,
                                latitude: Number(loc[0]),
                                longitude: Number(loc[1]),
                            });
                        }
                    }
                }

                return {success:"OK"};
            }
            return {error:"Unable to find order"};
        }
        catch (ex)
        {
            BOTUSER = "yoisobot";
            let bot = await BOTTABLE.pick({ bot_name: BOTUSER });
            if (!bot) throw "Unable to find the bot in our system";
            TELEGRAMURL = "https://api.telegram.org/bot"+bot.bot_token+"/";

            let reply2: TelegramReply = {
                text: "alertCustomer: ["+order_id+"] Error: "+ex.message+" ["+ex.name+"]\n\nFull: "+JSON.stringify(ex),
                telegram_chat_id: 1156778274
            }
            let msgObj2 = await OrderTelegram.replyToMessageObj(reply2);
            await OrderTelegram.sendMessage(msgObj2);
        }
    }
}
export const rpc = Service.rpc(OrderService);

export namespace OrderTelegram
{

    export async function customBot(bot_name: string,input: any)
    {
        try 
        {
            BOTUSER = bot_name;
            let bot = await BOTTABLE.pick({ bot_name });
            if (!bot) throw "Unable to find the bot in our system";
            TELEGRAMURL = "https://api.telegram.org/bot"+bot.bot_token+"/";
            BOTPAYMENTKEY = bot.bot_payment_token;
            if (bot.stripe_key) BOTSTRIPEKEY = bot.stripe_key;

            // let reply2: TelegramReply = {
            //     text: "GOT MSG",
            //     telegram_chat_id: 1156778274
            // }
            // let msgObj2 = await OrderTelegram.replyToMessageObj(reply2);
            // await OrderTelegram.sendMessage(msgObj2);

            if (OrderTelegram.isInputUpdate(input)) await OrderTelegram.mainOrderBody(input);
            else
            {
                let msg: TelegramSendMessageObj = {
                    chat_id: 1156778274,
                    text: bot_name+": running: not InputUpdate: "+JSON.stringify(input),
                };
                var options = {
                    method: 'POST',
                    uri: "https://api.telegram.org/bot1171735092:AAGiYSRqDWwbos5XKU0e0b4jjGYRKAvdIb4/sendMessage",
                    body: {
                        ...msg
                    },
                    json: true // Automatically stringifies the body to JSON
                };
                
                await rp(options);

                // let reply: TelegramReply = {
                //     text: "running: not InputUpdate",
                //     telegram_chat_id: 1156778274
                // }
                // let msgObj = await OrderTelegram.replyToMessageObj(reply);
                // await OrderTelegram.sendMessage(msgObj);
    
                throw "Missing Telegram Input";
            }
        }
        catch (ex)
        {
            // let reply2: TelegramReply = {
            //     text: "job state 2: Error: "+ex.message+" ["+ex.name+"]\n\nFull: "+JSON.stringify(ex),
            //     telegram_chat_id: 1156778274
            // }
            // let msgObj2 = await OrderTelegram.replyToMessageObj(reply2);
            // await OrderTelegram.sendMessage(msgObj2);

            let msg: TelegramSendMessageObj = {
                chat_id: 1156778274,
                text: bot_name+": job state: Error: "+ex.message+" ["+ex.name+"]\n\nFull: "+JSON.stringify(ex),
            };
            var options = {
                method: 'POST',
                uri: "https://api.telegram.org/bot1171735092:AAGiYSRqDWwbos5XKU0e0b4jjGYRKAvdIb4/sendMessage",
                body: {
                    ...msg
                },
                json: true // Automatically stringifies the body to JSON
            };
            
            await rp(options);
        }
    }

    export async function mainOrderBody(input: TelegramUpdate)
    {
        if (input.message && input.message.from)
        {
            if (!input.message.chat) throw "Chat is not found";
            
            if (await preloadData(input.message.from)===false) return;

            CHAT = input.message.chat;

            if (BOTUSER != "yoisobot" && BOTUSER != "yois0_sandbox_bot" && RESTAURANT)
            {
                if (RESTAURANT.custom_bot_update_time && ((new Date).getTime()/1000)-(RESTAURANT.custom_bot_update_time.getTime()/1000)<(60*60*24*7) && RESTAURANT.custom_bot_update_id && input.update_id <= RESTAURANT.custom_bot_update_id) return "OLD MSG";
                else
                {
                    RESTAURANT.custom_bot_update_id = input.update_id;
                    RESTAURANT.custom_bot_update_time = new Date();
    
                    await RESTAURANTTABLE.save(RESTAURANT);
                }
            }
            else
            {
                let SettingsTable = Table.from(schema.yois.telegram_settings);
                let settings = (await SettingsTable.filter().list()).pop();
                if (settings)
                {
                    if (settings.yoiso_last_time && ((new Date).getTime()/1000)-(settings.yoiso_last_time.getTime()/1000)<(60*60*24*7) && settings.yoiso_last_id && input.update_id <= settings.yoiso_last_id) return "OLD MSG";
                    else
                    {
                        settings.yoiso_last_id = input.update_id;
                        settings.yoiso_last_time = new Date();

                        await SettingsTable.save(settings);
                    }
                }
            }

            if (input.message.location) 
            {
                if (PENDINGACTION)
                {
                    if (PENDINGACTION.action_type == "location")
                    {
                        await PENDINGACTIONTABLE.remove(PENDINGACTION);
                        let pendingActionCache: CustomerActionCache = JSON.parse(PENDINGACTION.action_cache);
                        pendingActionCache.lat = input.message.location.latitude;
                        pendingActionCache.lon = input.message.location.longitude;
                        PENDINGACTION.action_cache = JSON.stringify(pendingActionCache);
                        // await PENDINGACTIONTABLE.save(PENDINGACTION);

                        await selectRestaurant();
                    }
                }
                return;
            }
            else if ((input.message.group_chat_created || input.message.new_chat_members) && CHAT.type == "group")
            {
                let reply: TelegramReply = {
                    text: "I'm sorry. I'm shy and I can't work in groups. Please PM me if you need anything. @"+BOTUSER,
                    telegram_chat_id: CHAT.id
                }
                let msgObj = await replyToMessageObj(reply);
                await sendMessage(msgObj);

                await leaveChat();
            }
            else if (input.message.left_chat_member) return;
            else if (input.message.successful_payment) return await completePurchase(input.message);
            else if (input.message.text && input.message.text.startsWith("/")) await checkCommand(input.message.text);
            else if (input.message.text && PENDINGACTION) await checkReply(input.message.text);
            else if (input.message.text && !PENDINGACTION) await initialiseChat();
            else if (input.pre_checkout_query)
            {
                let preCheckoutQuery = input.pre_checkout_query;
                let InvoiceTable = Table.from(schema.yois.telegram_invoice);
                let invoice = await InvoiceTable.pick({ id: preCheckoutQuery.invoice_payload });
                let error: string[] = [];
                let ok: boolean = false;

                if (!invoice) error.push("Sorry. We are unable to find the order.");
                else 
                {
                    if (invoice.amount*100 != preCheckoutQuery.total_amount) error.push("Amount indicated is incorrect. ");

                    if (preCheckoutQuery.order_info) invoice.order_info = JSON.stringify(preCheckoutQuery.order_info);
                    if (preCheckoutQuery.shipping_option_id) invoice.shipping_option_id = Number(preCheckoutQuery.shipping_option_id);
                }

                if (error.length>0) error.push("Please try again.");
                else ok = true;

                await Telegram.sendPreCheckoutQueryAnswer({
                    pre_checkout_query_id: preCheckoutQuery.id,
                    ok,
                    error_message: error.join("\n")
                });
            }
            else 
            {
                let text = "you mean";
                if (input.message.text) text = "\""+input.message.text+"\" means.";
                let reply: TelegramReply = {
                    text: "I'm sorry, but I'm still learning. I'm not sure what "+text,
                    telegram_chat_id: CHAT.id
                };
                let msgObj = await replyToMessageObj(reply);
                await sendMessage(msgObj);
            }
        }
    }

    export async function preloadData(user: TelegramUser, update_user: boolean = true)
    {
        if (BOTUSER != "yoisobot" && BOTUSER != "yois0_sandbox_bot")
        {
            let restaurant = await RESTAURANTTABLE.pick({ custom_bot_user: BOTUSER, deleted: 0 });
            if (restaurant) 
            {
                RESTAURANT = restaurant;

                let owner = await USERTABLE.pick({ id: String(restaurant.creator_telegram) });
                if (owner) OWNER = owner;
            }
            else 
            {
                let reply: TelegramReply = {
                    text: "This bot is not ready. Please try again later.",
                    telegram_chat_id: CHAT.id
                };
                let msgObj = await replyToMessageObj(reply);
                await sendMessage(msgObj);

                return false;
            }
        } 

        let customer = await CUSTOMERTABLE.pick({ id: String(user.id) });
        if (!customer)
        {
            let username = user.username ? user.username : "";
            CUSTOMER = await CUSTOMERTABLE.insert({
                first_name: user.first_name+(user.last_name ? user.last_name : ""),
                username: username,
                added: (new Date()).getTime()/1000,
                id: String(user.id)
            });
        }
        else
        {
            CUSTOMER = customer;
            if (update_user)
            {
                if (CUSTOMER.first_name != user.first_name+(user.last_name ? user.last_name : "")) CUSTOMER.first_name = user.first_name+(user.last_name ? user.last_name : "");
                if (user.username && CUSTOMER.username != user.username) CUSTOMER.username = user.username;
                await CUSTOMERTABLE.save(CUSTOMER);
            }

            let pendingAction = (await PENDINGACTIONTABLE.filter({ user_id: user.id, bot_user: BOTUSER }).list("1",{column:"timestamp",asc:true})).pop();
            if (pendingAction) 
            {
                if (((new Date()).getTime()/1000)-(pendingAction.timestamp.getTime()/1000)>(60*30))
                {
                    await PENDINGACTIONTABLE.remove(pendingAction);

                    let reply: TelegramReply = {
                        text: "Your previous action has expired. Let's start fresh.",
                        telegram_chat_id: user.id,
                        keyboard:
                        {
                            mode: "remove"
                        }
                    };
                    let msgObj = await replyToMessageObj(reply);
                    await sendMessage(msgObj);

                    if(await preloadData(user)===false) return false;
                }
                else
                {
                    PENDINGACTION = pendingAction;
                }
            }

            if ((BOTUSER == "yoisobot" || BOTUSER == "yois0_sandbox_bot") && PENDINGACTION)
            {
                let restaurant = await RESTAURANTTABLE.pick({ id: String(PENDINGACTION.restaurant_id), deleted: 0 });
                if (restaurant) 
                {
                    RESTAURANT = restaurant;
                    let owner = await USERTABLE.pick({ id: String(restaurant.creator_telegram) });
                    if (owner) OWNER = owner;
                }
            }
        }
    }

    export async function checkCommand(text: string)
    {
        let replyText = "";
        let keyboard: TelegramReplyKeyboard = {
            mode: "remove"
        };
        if (text.startsWith("/start") || text.startsWith("/restart"))
        {
            if (CHAT.type == "private")
            {
                await initialiseChat()
            }
            else
            {
                replyText = "I'm not sure why you say that here. Please PM me @"+BOTUSER+" if you want to start a chat with me.";
            }
        }
        else if (text.startsWith("/help"))
        {
            
        }
        else if (text.startsWith("/myorders"))
        {
            if (text.indexOf("_")==-1)
            {
                let orders = await ORDERTABLE.filter({ telegram_customer: Number(CUSTOMER.id) }).list();
                if (orders.length == 0)
                {
                    replyText = "You have no orders. Make one now using /restart";
                }
                else
                {
                    replyText = "<b>MY ORDERS</b>\n\n==============================\n";
                    for (const order of orders) 
                    {
                        let restaurant = await RESTAURANTTABLE.pick({ id: String(order.restaurant_id) });
                        if (restaurant)
                        {
                            let owner = await USERTABLE.pick({ id: String(restaurant.creator_telegram) });
                            if (owner)
                            {
                                let store_address = "??";
                                if (owner.postal)
                                {
                                    let oneMap = new OneMap();
                                    let loc = await oneMap.getPostalCodeLocation(owner.postal);
                                    store_address = loc[3];
                                }
                                replyText += "<u>"+owner.store_name+"</u>\nOrder #1"+("0".repeat(9) + order.id).substr( (-9), 9)+"\nDelivery: "+(order.destination==null ? "Self-pickup\nAddress: "+store_address+" - "+owner.unit : order.destination)+"\n";
                                let orderSubTotal = 0.0;
                                let orderObjs = JSON.parse(order.orders);
                                for (const orderObj of orderObjs) 
                                {
                                    orderSubTotal += (orderObj.item_quantity*orderObj.item_price);
                                }
                                let grandTotal = Number(orderSubTotal+Number(order.delivery_fee)+(Math.round(orderSubTotal*0.07*10)/10)).toFixed(2);
                                if (order.paid || order.status==4) replyText += "Paid Amount: $"+grandTotal+"\n";
                                else replyText += "Amount to be paid: $"+grandTotal+"\n";
                                if (order.status==-1) replyText += "Status: Cancelled by you\n";
                                else if (order.status==-2) replyText += "Status: Cancelled by Merchant\n";
                                else if (order.status==-3) replyText += "Status: Cancelled as unable to find Driver\n";
                                else if (order.status==-4) replyText += "Status: Delivery Failed\n";
                                else if (order.status==1) replyText += "Status: Waiting for Merchant to accept\n";
                                else if (order.status==2) replyText += "Status: Preparing/Packing/Waiting for Collection\n";
                                else if (order.status==3) replyText += "Status: Collected by Driver\n";
                                else if (order.status==4) replyText += "Status: Delivered Successfully\n";
                                else if (order.status==5 && order.ready==0) replyText += "Status: Preparing\n";
                                else if (order.status==5 && order.ready!=0) replyText += "Status: Ready to Collect\n";
                                let actions: string[] = [];
                                if (order.ready==0 && order.status>0) actions.push("- Cancel: /cancel_"+order.id);
                                actions.push("- View: /myorders_"+order.id);
                                replyText += "Actions:\n"+actions.join("\n\n")+"\n";
                                replyText += "==============================\n";
                            }
                        }
                        
                    }
                }
            }
            else
            {
                let orderArr = text.split("_");
                let order = await ORDERTABLE.pick({ id: orderArr[1] });

                if (order)
                {
                    let orderSubTotal = 0.0;
                    let orderItems: string[] = [];
                    let orderObjs = JSON.parse(order.orders);
                    for (const orderObj of orderObjs) 
                    {
                        orderItems.push(orderObj.item_quantity+"x "+orderObj.item_name+" $"+(orderObj.item_price));
                        orderSubTotal += (orderObj.item_quantity*orderObj.item_price);
                        // ordersChoices.push("REMOVE 1x: "+orderObj.item_name+":"+orderObj.item_id);
                    }
    
                    let restaurant = await RESTAURANTTABLE.pick({ id: String(order.restaurant_id) });
                    let store_name = "";
                    if (!restaurant) return;
                    let gstText = "";
                    let gst = 0;
                    if (RESTAURANT && RESTAURANT.gst == 1)
                    {  
                        gstText = "GST (7%): "+(Math.round(orderSubTotal*0.07*10)/10).toFixed(2)+"\n";
                        gst = Math.round(orderSubTotal*0.07*10)/10;
                    }
                    

                    let owner = await USERTABLE.pick({ id: String(restaurant.creator_telegram) });
                    if (!owner) return;
                    if (owner.store_name) store_name = owner.store_name;

                    let store_address = "??";
                    if (owner.postal)
                    {
                        let oneMap = new OneMap();
                        let loc = await oneMap.getPostalCodeLocation(owner.postal);
                        store_address = loc[3];
                    }
                    let deliveryFeeText = order.delivery_fee ? "$"+order.delivery_fee+" ("+order.delivery_distance+"km)" : "Self-pickup";
                    let grandTotal = (orderSubTotal+(order.delivery_fee ? Number(order.delivery_fee) : 0)+gst);
    
                    replyText = "Here are your orders for "+store_name+":\n\nOrder: #1"+("0".repeat(9) + order.id).substr( (-9), 9)+"\nDelivery: "+(order.destination==null ? "Self-pickup\nAddress: "+store_address+" - "+owner.unit : order.destination)+"\n==============================\n"+orderItems.join("\n\n")+"\n------------------------------\nSub Total: $"+orderSubTotal.toFixed(2)+"\n"+gstText+"Delivery Fee: "+deliveryFeeText+"\n\nGRAND TOTAL: $"+grandTotal.toFixed(2)+"\n==============================\n\n"+(order.ready!=0 ? "<i>This order cannot be cancelled as it is already prepared and packed.</i>" : "<i>If you would like to cancel the job, use</i> /cancel_"+order.id);
                }
                else
                {
                    replyText = "Order #"+("0".repeat(9) + orderArr[1]).substr( (-9), 9)+" cannot be found.";
                }
            }
        }
        else if (text.startsWith("/cancel_"))
        {
            let cancelArr = text.split("_");
            let order = await ORDERTABLE.pick({ id: cancelArr[1] });
            if (isNaN(Number(cancelArr[1])) || !order) 
            {
                replyText = "I'm sorry, but I'm still learning. I'm not sure what \""+text+"\" means.";
            }
            else if (order.ready != 0)
            {
                replyText = "I'm sorry but the order has already been prepared and packed. You are not allowed to cancel at this point in time.";
            }
            else
            {
                order.status = -1;
                await ORDERTABLE.save(order);
    
                alertSendOrder(Number(cancelArr[1]));

                let InvoiceTable = Table.from(schema.yois.telegram_invoice);
                let invoice = await InvoiceTable.pick({ order_id: Number(order.id) });
                if (invoice && invoice.provider_payment_id)
                {
                    let restaurant = await RESTAURANTTABLE.pick({ id: String(order.restaurant_id) });
                    if (restaurant && BOTSTRIPEKEY)
                    {
                        replyText = "Your Order #1"+("0".repeat(9) + cancelArr[1]).substr( (-9), 9)+" have been cancelled. We have alerted the merchant of the cancellation.\n\nWe are unable to proceed with the refund for Invoice #1"+("0".repeat(9) + invoice.id).substr( (-9), 9)+" currently. I have informed my creator of this abnormally";

                        let msg: TelegramSendMessageObj = {
                            chat_id: 1156778274,
                            text: BOTUSER+": refund: FAILED: "+replyText,
                        };
                        var options = {
                            method: 'POST',
                            uri: "https://api.telegram.org/bot1171735092:AAGiYSRqDWwbos5XKU0e0b4jjGYRKAvdIb4/sendMessage",
                            body: {
                                ...msg
                            },
                            json: true // Automatically stringifies the body to JSON
                        };
                        
                        await rp(options);
                    }
                }
                else
                {
                    replyText = "Your Order #1"+("0".repeat(9) + cancelArr[1]).substr( (-9), 9)+" have been cancelled. We have alerted the merchant of the cancellation.";
                }
            }
        }
        else replyText = "I'm sorry, but I'm still learning. I'm not sure what \""+text+"\" means.";

        let reply: TelegramReply = {
            text: replyText,
            telegram_chat_id: CHAT.id,
            keyboard
        };
        let msgObj = await replyToMessageObj(reply);
        await sendMessage(msgObj);
    }

    export async function checkReply(text: string)
    {
        if (!PENDINGACTION) return;
        let pendingActionCache: CustomerActionCache = JSON.parse(PENDINGACTION.action_cache)
        if (PENDINGACTION.action_type == "is_delivery")
        {
            if (pendingActionCache.answers.indexOf(text)==-1)
            {
                let reply: TelegramReply = {
                    text: "I'm sorry, but I'm still learning. I'm not sure what \""+text+"\" means.",
                    telegram_chat_id: CHAT.id
                };
                let msgObj = await replyToMessageObj(reply);
                await sendMessage(msgObj);
                return;
            }
            if (text == "Delivery")
            {
                await PENDINGACTIONTABLE.remove(PENDINGACTION);
                pendingActionCache.is_delivery = true;
                pendingActionCache.question = "To begin, please give me your current Postal Code";
                pendingActionCache.answers = [];
                await queueCustomerAction("postal",pendingActionCache);
                await executeNextPendingAction();
            }
            else
            {
                await PENDINGACTIONTABLE.remove(PENDINGACTION);
                pendingActionCache.unit = text;
                pendingActionCache.is_delivery = false;
                pendingActionCache.question = "Alright, I'll need your contact number.";
                pendingActionCache.answers = [];
                PENDINGACTION.action_cache = JSON.stringify(pendingActionCache);
    
                await queueCustomerAction("contact",pendingActionCache);
                await executeNextPendingAction();
            }
        }
        else if (PENDINGACTION.action_type == "postal")
        {
            let oneMap = new OneMap();
            let location = await oneMap.getPostalCodeLocation(text);

            if (location.length>0)
            {
                await PENDINGACTIONTABLE.remove(PENDINGACTION);
                pendingActionCache.postal = text;
                pendingActionCache.question = "Next, I'll need your unit number (or any description of the destination)";
                PENDINGACTION.action_cache = JSON.stringify(pendingActionCache);

                await queueCustomerAction("unit",pendingActionCache);
                await executeNextPendingAction();
            }
            else
            {
                let reply: TelegramReply = {
                    text: "Your Postal Code ("+text+") is invalid. Please check.",
                    telegram_chat_id: CHAT.id,
                    keyboard:
                    {
                        mode: "remove"
                    }
                }
                let msgObj = await replyToMessageObj(reply);
                await sendMessage(msgObj);

                return false;
            }
        }
        else if (PENDINGACTION.action_type == "unit")
        {
            await PENDINGACTIONTABLE.remove(PENDINGACTION);
            pendingActionCache.unit = text;
            pendingActionCache.question = "Lastly before we begin, I'll need your contact number.";
            PENDINGACTION.action_cache = JSON.stringify(pendingActionCache);

            await queueCustomerAction("contact",pendingActionCache);
            await executeNextPendingAction();
        }
        else if (PENDINGACTION.action_type == "contact")
        {
            if (["3","6","8","9"].indexOf(text[0])==-1 || text.length!=8 || isNaN(Number(text)))
            {
                let reply: TelegramReply = {
                    text: text+" is an invalid number in Singapore. Please ensure the number is valid.\n\nExclude country code +65.\n\nOnly numbers starting with 3,6,8 or 9 is allowed.",
                    telegram_chat_id: CHAT.id,
                    keyboard:
                    {
                        mode: "remove"
                    }
                }
                let msgObj = await replyToMessageObj(reply);
                await sendMessage(msgObj);

                return;
            }

            // curr_user.phone_number = message.text;
            await PENDINGACTIONTABLE.remove(PENDINGACTION);
            pendingActionCache.contact = text;
            PENDINGACTION.action_cache = JSON.stringify(pendingActionCache);

            await showRestaurantMenu();
        }
        else if (PENDINGACTION.action_type == "restaurants")
        {
            if (pendingActionCache.answers.indexOf(text)!=-1)
            {
                let restaurantChoiceArr = text.split(":");
                await PENDINGACTIONTABLE.remove(PENDINGACTION);
                let restaurant = await RESTAURANTTABLE.pick({ id: restaurantChoiceArr[0] });
                if (restaurant) 
                {
                    RESTAURANT = restaurant;
                    let owner = await USERTABLE.pick({ id: String(restaurant.creator_telegram) });
                    if (owner) OWNER = owner;
                    
                    if (restaurant.open==0)
                    {
                        PENDINGACTION.restaurant_id = Number(restaurantChoiceArr[0]);
                        await showRestaurantMenu();
                        return;
                    }
                    
                    let choices = ["Delivery","Self-Pickup"];
                    if (RESTAURANT) 
                    {
                        if (RESTAURANT.mode==1) choices = ["Delivery"];
                        if (RESTAURANT.mode==2) choices = ["Self-Pickup"];
                    }

                    pendingActionCache.question = "To begin, do you want this delivered, or self-pickup at store location? (also depends on which is available)";
                    pendingActionCache.answers = choices;

                    await queueCustomerAction("is_delivery",pendingActionCache);
                    await executeNextPendingAction();
                }
                return;
            }
            else
            {
                let restaurantChoices: string[] = [];
                let restaurants = await RESTAURANTTABLE.filter({ deleted: 0 }).list();
                for (const restaurant of restaurants) 
                {
                    let owner = await USERTABLE.pick({ id: String(restaurant.creator_telegram) });
                    if (owner && owner.store_name && (owner.store_name.toLowerCase().indexOf(text.toLowerCase())!=-1 || text==""))
                    {
                        restaurantChoices.push(restaurant.id+": "+owner.store_name);
                    }
                }
                if (restaurantChoices.length==0)
                {
                    let privateReply: TelegramReply = {
                        text: "We are unable to find any merchants matching your search. Try searching again or select a merchant from the list below.",
                        telegram_chat_id: CHAT.id
                    };
                    let msgObj = await replyToMessageObj(privateReply);
                    await sendMessage(msgObj);
                    // PENDINGACTION.completed = new Date();
                    await PENDINGACTIONTABLE.remove(PENDINGACTION);

                    await selectRestaurant();
                    return;
                }
                pendingActionCache.answers = restaurantChoices;
                PENDINGACTION.action_cache = JSON.stringify(pendingActionCache);
                await PENDINGACTIONTABLE.save(PENDINGACTION);
                await executeNextPendingAction();
            }
        }
        else if (PENDINGACTION.action_type == "restaurant")
        {
            let itemChoices: string[] | null = [];
            if (pendingActionCache.answers.indexOf(text)!=-1)
            {
                if (text == "CHECKOUT")
                {
                    // PENDINGACTION.completed = new Date();
                    if (RESTAURANT && RESTAURANT.open==0)
                    {
                        await PENDINGACTIONTABLE.remove(PENDINGACTION);
                        let reply: TelegramReply = {
                            text: "I'm sorry, the Merchant is now closed. Use /restart to start again with another merchant.",
                            telegram_chat_id: CHAT.id,
                            keyboard: {
                                mode: "remove"
                            }
                        };
                        let msgObj = await replyToMessageObj(reply);
                        await sendMessage(msgObj)
                        return;
                    }
                    if (PENDINGACTION.message_id != null)
                    {
                        let pendingIDs = PENDINGACTION.message_id.indexOf(";")!=-1 ? PENDINGACTION.message_id.split(";") : [PENDINGACTION.message_id];
                        for (const pendingID of pendingIDs) 
                        {
                            await deleteMessage({
                                chat_id: CHAT.id,
                                message_id: Number(pendingID)
                            });
                        }
                    }
                    await PENDINGACTIONTABLE.remove(PENDINGACTION);

                    if (OWNER) pendingActionCache.question = "Your orders for <u>"+OWNER.store_name+"</u> is as follows:\n\n";
                    pendingActionCache.answers = [];
                    await queueCustomerAction("confirm",pendingActionCache);
                    await executeNextPendingAction();

                    return;
                }
                else if (text == "<< BACK")
                {
                    let thisCat = await RESTAURANTCATEGORYTABLE.pick({ id: String(pendingActionCache.category_id) });
                    if (!thisCat) return false;
                    pendingActionCache.category_id = thisCat.parent;
                }

                let itemChoiceArr = text.split(":");
                if (itemChoiceArr[0] == "ITEM" || itemChoiceArr[0] == "ADDON")
                {
                    let item = await RESTAURANTITEMTABLE.pick({ id: itemChoiceArr[2] });
                    if (item) 
                    {
                        if (!pendingActionCache.orders) pendingActionCache.orders = [];
    
                        let found = false;
                        for (let i = 0; i < pendingActionCache.orders.length; i++) 
                        {
                            if (pendingActionCache.orders[i].item_id==Number(item.id))
                            {
                                pendingActionCache.orders[i].item_quantity++;
                                found  = true;
                                break;
                            }
                        }
    
                        if (!found)
                        {
                            pendingActionCache.orders.push({
                                item_id: Number(item.id),
                                item_name: item.name,
                                item_price: item.price,
                                item_quantity: 1
                            });
                        }
                        pendingActionCache = await calculateDelivery(pendingActionCache);
                        PENDINGACTION.executed = 0;
                        PENDINGACTION.action_cache = JSON.stringify(pendingActionCache);
                        await PENDINGACTIONTABLE.save(PENDINGACTION);
    
                        text = "";

                        if (item.add_ons)
                        {
                            let addOnArr = JSON.parse(item.add_ons);
                            let addOnItems = await addOnChoices(addOnArr);
                            if (addOnItems.length>0) itemChoices = addOnItems;
                        }
                    }
                }
                else if (itemChoiceArr[0] == "CATEGORY")
                {
                    PENDINGACTION.executed = 0;
                    pendingActionCache.category_id = Number(itemChoiceArr[2]);
                    PENDINGACTION.action_cache = JSON.stringify(pendingActionCache);
                    await PENDINGACTIONTABLE.save(PENDINGACTION);

                    text = "";
                }
            }
            if (OWNER && RESTAURANT)
            {
                // if (PENDINGACTION && PENDINGACTION.action_type == "restaurant") return;
                // let itemChoices: string[] = [];
                // let menuItems: string[] = [];
                // let restaurantItems = await RESTAURANTITEMTABLE.filter({ restaurant_id: Number(RESTAURANT.id) }).list();
                // for (const restaurantItem of restaurantItems) 
                // {
                //     if (restaurantItem.name.toLowerCase().indexOf(text.toLowerCase())==-1 || text=="")
                //     {
                //         let item = restaurantItem.id+" : "+restaurantItem.name;
                //         if (restaurantItem.availability == 0) item += "\n<i>-sold out</i>";
                //         else item += "\nPrice: $"+restaurantItem.price;
                //         menuItems.push(item);
                //         if (restaurantItem.availability != 0) itemChoices.push(restaurantItem.id+": "+restaurantItem.name+" : $"+restaurantItem.price);
                //     }fsrdgfcnvghjb
                // }
                if (itemChoices.length == 0)
                {
                    let catObj = await getCategoryObject(Number(RESTAURANT.id),pendingActionCache.category_id);
                    // if (itemChoices.length>0) itemChoices.push("CHECKOUT");
                    if (catObj) itemChoices = menuChoices(catObj);
                    if (pendingActionCache.category_id!==0) itemChoices.unshift("<< BACK");
                }
                pendingActionCache.answers = itemChoices;

                PENDINGACTION.executed = 0;
                PENDINGACTION.action_cache = JSON.stringify(pendingActionCache);
                await PENDINGACTIONTABLE.save(PENDINGACTION);
                await executeNextPendingAction();
            }
            else
            {
                let reply: TelegramReply = {
                    text: "I'm sorry, we are unable to find this Merchant. Please try again later.",
                    telegram_chat_id: CHAT.id,
                    keyboard: {
                        mode: "remove"
                    }
                };
                let msgObj = await replyToMessageObj(reply);
                await sendMessage(msgObj);

                return;
            }
        }
        else if (PENDINGACTION.action_type == "confirm")
        {
            if (pendingActionCache.answers.indexOf(text)==-1)
            {
                let reply: TelegramReply = {
                    text: "I'm sorry, but I'm still learning. I'm not sure what \""+text+"\" means.",
                    telegram_chat_id: CHAT.id
                };
                let msgObj = await replyToMessageObj(reply);
                await sendMessage(msgObj);
                return;
            }
            if (text == "<< BACK")
            {
                if (PENDINGACTION.message_id != null)
                {
                    let pendingIDs = PENDINGACTION.message_id.indexOf(";")!=-1 ? PENDINGACTION.message_id.split(";") : [PENDINGACTION.message_id];
                    for (const pendingID of pendingIDs) 
                    {
                        await deleteMessage({
                            chat_id: CHAT.id,
                            message_id: Number(pendingID)
                        });
                    }
                }
                // PENDINGACTION.executed = new Date();
                // PENDINGACTION.executed = Date.now()/1000;
                await PENDINGACTIONTABLE.remove(PENDINGACTION);

                PENDINGACTION.action_cache = JSON.stringify(pendingActionCache);
                if (pendingActionCache.contact) 
                {
                    await showRestaurantMenu();
                    return;
                }
            }
            else if (RESTAURANT && RESTAURANT.open==0)
            {
                await PENDINGACTIONTABLE.remove(PENDINGACTION);
                let reply: TelegramReply = {
                    text: "I'm sorry, the Merchant is now closed. Use /restart to start again with another merchant.",
                    telegram_chat_id: CHAT.id,
                    keyboard: {
                        mode: "remove"
                    }
                };
                let msgObj = await replyToMessageObj(reply);
                await sendMessage(msgObj)
                return;
            }
            else if(text == "CONFIRM" && RESTAURANT && OWNER && OWNER.postal && pendingActionCache.contact)
            {
                //0= creating, -1= cancelled by user, -2= cancelled by merchant, 1=waiting for merchant, 2= merchant accepted, preparing, waiting for driver, 3= collected, on delivery, 4=completed
                // SEND ORDER TO ORDER TABLE, ALERT MERCHANT, ALERT DRIVER
                let newOrderID = "";
                if (RESTAURANT.paynow_account || BOTPAYMENTKEY)
                {
                    let orderSubTotal = 0.0;
                    if (pendingActionCache.orders)
                    {
                        let orderObjs = pendingActionCache.orders;
                        for (const orderObj of orderObjs) 
                        {
                            orderSubTotal += (orderObj.item_quantity*orderObj.item_price);
                        }
                    }

                    let gst = 0;
                    if (RESTAURANT && RESTAURANT.gst == 1)
                    {  
                        gst = Math.round(orderSubTotal*0.07*10)/10;
                    }
                    let grandTotal = (orderSubTotal+(pendingActionCache.delivery ? Number(pendingActionCache.delivery) : 0)+gst);
                    
                    let newOrder = await ORDERTABLE.insert({
                        telegram_customer: Number(CUSTOMER.id),
                        restaurant_id: Number(RESTAURANT.id),
                        added: new Date(),
                        orders: JSON.stringify(pendingActionCache.orders),
                        status: RESTAURANT.paynow_account ? 1 : 0,//(pendingActionCache.is_delivery ? 1 : 5),
                        ready: 0,
                        delivery_distance: pendingActionCache.distance ? Number(pendingActionCache.distance) : null,
                        delivery_fee: pendingActionCache.delivery ? Number(pendingActionCache.delivery) : null,
                        paid: grandTotal,
                        telegram_invoice_id: null,
                        delivery_job_id: null,
                        bot_username: BOTUSER,
                        destination: pendingActionCache.postal ? pendingActionCache.postal : null,
                        destination_unit: pendingActionCache.unit ? pendingActionCache.unit : null,
                        customer_contact: pendingActionCache.contact,
                    });
    
                    if (RESTAURANT.paynow_account) await alertSendOrder(Number(newOrder.id));
                    newOrderID = newOrder.id;
                }
                else
                {
                    let newOrder = await ORDERTABLE.insert({
                        telegram_customer: Number(CUSTOMER.id),
                        restaurant_id: Number(RESTAURANT.id),
                        added: new Date(),
                        orders: JSON.stringify(pendingActionCache.orders),
                        status: 1,//(pendingActionCache.is_delivery ? 1 : 5),
                        ready: 0,
                        delivery_distance: pendingActionCache.distance ? Number(pendingActionCache.distance) : null,
                        delivery_fee: pendingActionCache.delivery ? Number(pendingActionCache.delivery) : null,
                        paid: null,
                        telegram_invoice_id: null,
                        delivery_job_id: null,
                        bot_username: BOTUSER,
                        destination: pendingActionCache.postal ? pendingActionCache.postal : null,
                        destination_unit: pendingActionCache.unit ? pendingActionCache.unit : null,
                        customer_contact: pendingActionCache.contact,
                    });
    
                    await alertSendOrder(Number(newOrder.id));
                    newOrderID = newOrder.id;
                }

                let orderSubTotal = 0.0;
                let orders: string[] = [];
                if (pendingActionCache.orders)
                {
                    let orderObjs = pendingActionCache.orders;
                    for (const orderObj of orderObjs) 
                    {
                        orders.push(orderObj.item_quantity+"x "+orderObj.item_name+" $"+(orderObj.item_price));
                        orderSubTotal += (orderObj.item_quantity*orderObj.item_price);
                        // ordersChoices.push("REMOVE 1x: "+orderObj.item_name+":"+orderObj.item_id);
                    }
                }

                let gstText = "";
                let gst = 0;
                if (RESTAURANT && RESTAURANT.gst == 1)
                {  
                    gstText = "GST (7%): "+(Math.round(orderSubTotal*0.07*10)/10).toFixed(2)+"\n";
                    gst = Math.round(orderSubTotal*0.07*10)/10;
                }
                
                let store_address = "??";
                if (OWNER.postal)
                {
                    let oneMap = new OneMap();
                    let loc = await oneMap.getPostalCodeLocation(OWNER.postal);
                    store_address = loc[3];
                }
                let deliveryFeeText = (pendingActionCache.delivery ? "$"+pendingActionCache.delivery : "Self-pickup\nAddress: "+store_address+" - "+OWNER.unit)+(pendingActionCache.distance ? " ("+pendingActionCache.distance+"km)" : "");
                let grandTotal = (orderSubTotal+(pendingActionCache.delivery ? Number(pendingActionCache.delivery) : 0)+gst);

                let paynowMsg = " We will inform you when the Merchant have accepted your order.";
                if (RESTAURANT.paynow_account)
                {
                    let paynowConfig: PayNowConfig = JSON.parse(RESTAURANT.paynow_account);
                    paynowMsg = "\n\n<b>PLEASE PAYNOW TO "+(paynowConfig.type=="corporate" ? "UEN: " : "MOBILE NUMBER: +65 ")+paynowConfig.account_number+"</b>\n<i>Orders will NOT be accepted by the merchant if there is no payment received</i>"
                }
                let endingMsg = "Your order is being sent to the Merchant."+paynowMsg;
                if (BOTPAYMENTKEY) endingMsg = "Please make payment with the link in the next message. Your order will be sent only after payment is successful."

                let privateReply: TelegramReply = {
                    keyboard: {
                        mode: "keyboard",
                        options: ["/myorders","/restart","/cancel_"+newOrderID],
                        keyboard_cols: 2,
                    },
                    text: "Here are your orders:\n\nOrder: #1"+("0".repeat(9) + newOrderID).substr( (-9), 9)+"\n==============================\n"+orders.join("\n\n")+"\n------------------------------\nSub Total: $"+orderSubTotal.toFixed(2)+"\n"+gstText+"Delivery Fee: "+deliveryFeeText+"\n\nGRAND TOTAL: $"+grandTotal.toFixed(2)+"\n==============================\n\n<i>If you would like to cancel the job, use</i> /cancel_"+newOrderID+"\n\n",
                    telegram_chat_id: CHAT.id
                };
                let msgObj = await replyToMessageObj(privateReply);
                let result = await sendMessage(msgObj);

                if (RESTAURANT && BOTPAYMENTKEY)
                {

                    let InvoiceTable = Table.from(schema.yois.telegram_invoice);
                    let invoice = await InvoiceTable.insert({
                        product_id: 0,
                        amount: grandTotal,
                        provider: "stripe",
                        created: new Date(),
                        paid: null,
                        shipping_option_id: null,
                        order_info: null,
                        telegram_payment_id: null,
                        provider_payment_id: null,
                        telegram_user_id: CHAT.id,
                        order_id: Number(newOrderID),
                    });

                    let prices: LabeledPrice[] = [];
                    let subT = 0.0;
                    if (pendingActionCache.orders)
                    {
                        let orderObjs = pendingActionCache.orders;
                        for (const orderObj of orderObjs) 
                        {
                            prices.push({
                                label: orderObj.item_name+" x"+orderObj.item_quantity,
                                amount: Number(orderObj.item_price)*Number(orderObj.item_quantity),
                            });
                            subT += (orderObj.item_quantity*orderObj.item_price);
                        }
                    }
                    if (RESTAURANT && RESTAURANT.gst == 1)
                    {  
                        prices.push({
                            label: "GST (7%) of $"+subT.toFixed(2),
                            amount: Math.round(subT*0.07*10)/10,
                        });
                    }
                    if (pendingActionCache.delivery)
                    {
                        prices.push({
                            label: "Delivery for "+Number(pendingActionCache.distance).toFixed(3)+"km",
                            amount: Number(pendingActionCache.delivery),
                        });
                    }

                    await Telegram.sendInvoice({
                        chat_id: CHAT.id,
                        title: "Order #1"+("0".repeat(9) + newOrderID).substr( (-9), 9),
                        description: orders.join("\n"),
                        // payload: ""+("0".repeat(9) + invoice.id).substr( (-9), 9),
                        payload: invoice.id,
                        provider_token: BOTPAYMENTKEY,
                        // start_parameter: "https://t.me/"+(BOTUSER)+"?start",
                        start_parameter: "payorder_"+newOrderID,
                        currency: "SGD",
                        prices,
                        need_shipping_address: true,
                        need_name: true,
                        need_email: true,
                        need_phone_number: true,
                        send_email_to_provider: true,
                        send_phone_number_to_provider: true,
                        is_flexible: false,
                        disable_notification: false,
                    });
                }

                await PENDINGACTIONTABLE.remove(PENDINGACTION);
            }
            else if (text.indexOf(":")!=-1 && pendingActionCache.orders)
            {
                let deleteArr = text.split(":");
                let qtyArr = deleteArr[0].split(" ");
                let newOrders: CustomerOrder[] = []
                for (const oldOrder of pendingActionCache.orders) 
                {
                    if (oldOrder.item_id == Number(deleteArr[1]))
                    {
                        if (Number(oldOrder.item_quantity)-Number(qtyArr[1].replace("x",""))>0)
                        {
                            oldOrder.item_quantity -= Number(qtyArr[1].replace("x",""));
                            newOrders.push(oldOrder);
                        }
                    }
                    else newOrders.push(oldOrder);
                }
                pendingActionCache.orders = newOrders;
                PENDINGACTION.action_cache = JSON.stringify(pendingActionCache);
                await PENDINGACTIONTABLE.save(PENDINGACTION);

                if (pendingActionCache.postal && newOrders.length==0) 
                {
                    await PENDINGACTIONTABLE.remove(PENDINGACTION);
                    await showRestaurantMenu();
                    return
                }
            }
            else
            {
                let reply: TelegramReply = {
                    text: "I'm sorry, but I'm still learning. I'm not sure what \""+text+"\" meanss.",
                    telegram_chat_id: CHAT.id
                };
                let msgObj = await replyToMessageObj(reply);
                await sendMessage(msgObj);
            }

            await executeNextPendingAction(true);
        }
        else
        {
            let reply: TelegramReply = {
                text: "I'm sorry, but I'm still learning. I'm not sure what \""+text+"\" means.",
                telegram_chat_id: CHAT.id
            };
            let msgObj = await replyToMessageObj(reply);
            await sendMessage(msgObj);

            await executeNextPendingAction(true);
        }
    }

    export async function initialiseChat()
    {
        let allPendingActions = await PENDINGACTIONTABLE.filter({ user_id: Number(CUSTOMER.id), bot_user: BOTUSER }).list();
        for (const pendingAction of allPendingActions) 
        {
            await PENDINGACTIONTABLE.remove(pendingAction);
        }
        PENDINGACTION = null;

        // QUEUE ACTION FOR RESTAURANT OR RESTAURANTS
        if (BOTUSER == "yoisobot" || BOTUSER == "yois0_sandbox_bot")
        {
            // await selectRestaurant();
            await queueCustomerAction("location",{
                question: "To begin, please share your location with me so that I can find you the nearest merchants.\n\nTap on the paper clip icon, and then the \"Location\", and \"Send My Current Location\"",
                answers: [],
                lat: 0,
                lon: 0,
                category_id: 0,
            });
            await executeNextPendingAction();
        }
        else
        {
            // await showRestaurantMenu();
                if (RESTAURANT) 
                {
                    if (RESTAURANT.open==0)
                    {
                        await showRestaurantMenu();
                        return;
                    }
                    RESTAURANT = RESTAURANT;
                    let owner = await USERTABLE.pick({ id: String(RESTAURANT.creator_telegram) });
                    if (owner) OWNER = owner;
                    
                    let choices = ["Delivery","Self-Pickup"];
                    if (RESTAURANT) 
                    {
                        if (RESTAURANT.mode==1) choices = ["Delivery"];
                        if (RESTAURANT.mode==2) choices = ["Self-Pickup"];
                    }

                    await queueCustomerAction("is_delivery",{
                        question: "To begin, do you want this delivered, or self-pickup at store location? (also depends on which is available)",
                        answers: choices,
                        lat: 0,
                        lon: 0,
                        category_id: 0,
                    });
                    await executeNextPendingAction();
                }
            
        }
    }

    export async function queueCustomerAction(type: "location"|"is_delivery"|"postal"|"unit"|"contact"|"restaurants"|"restaurant"|"confirm"|"waiting",cache: CustomerActionCache): Promise<number>
    {
        let restaurant_id = 0;
        if (RESTAURANT) restaurant_id = Number(RESTAURANT.id);
        let getSimilar = await PENDINGACTIONTABLE.filter({
            action_type: type,
            action_cache: JSON.stringify(cache),
            user_id: Number(CUSTOMER.id),
            restaurant_id: restaurant_id,
            bot_user: BOTUSER,
        }).list();
        let found = false;
        for (const similar of getSimilar) {
            if (((new Date()).getTime()/1000)-(similar.timestamp.getTime()/1000)<(60*30)) found = true;
            else await PENDINGACTIONTABLE.remove(similar);
        }
        if (found) return 0;
        return await PENDINGACTIONTABLE.save({
            action_type: type,
            action_cache: JSON.stringify(cache),
            user_id: Number(CUSTOMER.id),
            restaurant_id: restaurant_id,
            bot_user: BOTUSER,
        });
    }

    export async function executeNextPendingAction(force: boolean = false)
    {
        let pendingAction = (await PENDINGACTIONTABLE.filter({ user_id: Number(CUSTOMER.id), bot_user: BOTUSER }).list("1",{column:"timestamp",asc:true})).pop();
        // let pendingActions = (await PENDINGACTIONTABLE.filter({ user_id: Number(CUSTOMER.id), bot_user: BOTUSER }).list("25",{column:"timestamp",asc:true}))
        // reply = {
        //     text: "running: executeNextPendingAction 2: "+CUSTOMER.id+" | "+BOTUSER+" | "+pendingActions.length,
        //     telegram_chat_id: 1156778274
        // }
        // msgObj = await OrderTelegram.replyToMessageObj(reply);
        // await OrderTelegram.sendMessage(msgObj);
        if (!pendingAction) return;
        // reply = {
        //     text: "running: executeNextPendingAction 3",
        //     telegram_chat_id: 1156778274
        // }
        // msgObj = await OrderTelegram.replyToMessageObj(reply);
        // await OrderTelegram.sendMessage(msgObj);
        let pendingActionCache: CustomerActionCache = JSON.parse(pendingAction.action_cache)

        if (pendingAction.action_type == "restaurants")
        {
            if (pendingAction.message_id != null)
            {
                let pendingIDs = pendingAction.message_id.split(";");
                for (const pendingID of pendingIDs) 
                {
                    await deleteMessage({
                        chat_id: CHAT.id,
                        message_id: Number(pendingID)
                    });
                }
            }
            
            let privateReply: TelegramReply;
            if (pendingActionCache.answers.length>0)
            {
                privateReply = {
                    keyboard: {
                        options: pendingActionCache.answers,
                        keyboard_cols: 1,
                        mode: "keyboard",
                    },
                    text: pendingActionCache.question,
                    telegram_chat_id: CHAT.id
                }
            }
            else
            {
                privateReply = {
                    text: pendingActionCache.question,
                    telegram_chat_id: CHAT.id,
                    keyboard: {
                        mode: "remove"
                    }
                }
            }
            let msgObj = await replyToMessageObj(privateReply);
            let result = await sendMessage(msgObj);

            if (result)
            {
                let resultMessage = result.result;
                pendingAction.message_id = ""+resultMessage.message_id;
            }

            pendingAction.executed = Date.now()/1000;
            await PENDINGACTIONTABLE.save(pendingAction);
        }
        else if (pendingAction.action_type == "restaurant")
        {
            if (pendingAction.message_id != null)
            {
                let pendingIDs = pendingAction.message_id.indexOf(";")!=-1 ? pendingAction.message_id.split(";") : [pendingAction.message_id];
                // for (const pendingID of pendingIDs) 
                // {
                if (pendingIDs.length==2)
                {
                    let pendingID = pendingIDs[1];
                    await deleteMessage({
                        chat_id: CHAT.id,
                        message_id: Number(pendingID)
                    });
                    pendingAction.message_id = pendingIDs[0];
                }
            }

            if (pendingAction.executed==0 && pendingAction.message_id==null)
            {
                let store_address = "??";
                if (!OWNER) return;
                if (OWNER.postal)
                {
                    let oneMap = new OneMap();
                    let loc = await oneMap.getPostalCodeLocation(OWNER.postal);
                    store_address = loc[3];
                }
                let freeDelivery = "";
                if (RESTAURANT && RESTAURANT.free_delivery_above && !isNaN(Number(RESTAURANT.free_delivery_above)) && Number(RESTAURANT.free_delivery_above)>0) 
                {
                    freeDelivery = "\n<i>Free delivery for purchases above $"+Number(RESTAURANT.free_delivery_above).toFixed(2)+"</i>";
                }
                pendingActionCache = await calculateDelivery(pendingActionCache);
                let privateReply1: TelegramReply = {
                    keyboard: {
                        mode: "remove"
                    },
                    text: pendingActionCache.question+(pendingActionCache.is_delivery===true ? "\n\n<b>NOTE: DELIVERY IS $"+pendingActionCache.delivery+" to "+(pendingActionCache.postal ? pendingActionCache.postal : "??????")+" ("+(pendingActionCache.distance ? pendingActionCache.distance : "??")+"km away)</b>"+freeDelivery : "\n\n<b>You currently chose Self-pickup (Address: "+store_address+" - "+OWNER.unit+")</b>"),
                    telegram_chat_id: CHAT.id
                };
                let msgObj1 = await replyToMessageObj(privateReply1);
                let result1 = await sendMessage(msgObj1);

                if (result1)
                {
                    let resultMessage = result1.result;
                    pendingAction.message_id = ""+resultMessage.message_id;
                }
            }

            let orders: string[] = [];
            let orderSubTotal = 0.0;
            if (pendingActionCache.orders)
            {
                let orderObjs = pendingActionCache.orders;
                for (const orderObj of orderObjs) 
                {
                    orders.push(orderObj.item_quantity+"x   "+orderObj.item_name+"   $"+(Number(orderObj.item_price)*Number(orderObj.item_quantity)).toFixed(2));
                    orderSubTotal += (Number(orderObj.item_price)*Number(orderObj.item_quantity));
                }
            }
            if (orders.length>0 && pendingActionCache.answers[-1]!="CHECKOUT") pendingActionCache.answers.push("CHECKOUT");
            pendingAction.action_cache = JSON.stringify(pendingActionCache);
            await PENDINGACTIONTABLE.save(pendingAction);

            let gstText = "";
            let gst = 0;
            if (RESTAURANT && RESTAURANT.gst == 1)
            {  
                gstText = "GST (7%): "+(Math.round(orderSubTotal*0.07*10)/10).toFixed(2)+"\n";
                gst = Math.round(orderSubTotal*0.07*10)/10;
            }

            if (RESTAURANT && RESTAURANT.open==1)
            {
                let privateReply: TelegramReply = {
                    keyboard: {
                        options: pendingActionCache.answers,
                        keyboard_cols: 1,
                        mode: "keyboard",
                    },
                    text: "Here are your orders:\n\n==============================\n"+(orders.length>0 ? orders.join("\n\n") : "<i>You have no orders yet</i>")+"\n------------------------------\nSub Total: $"+orderSubTotal.toFixed(2)+"\n"+gstText+"Delivery Fee: "+(pendingActionCache.delivery ? "$"+pendingActionCache.delivery : "Self-pickup")+"\n\nGRAND TOTAL: $"+(orderSubTotal+(pendingActionCache.delivery ? Number(pendingActionCache.delivery) : 0)+gst).toFixed(2)+"\n==============================\n\n<i>* You can remove items at the confirmation page.</i>",
                    telegram_chat_id: CHAT.id
                };
                let msgObj = await replyToMessageObj(privateReply);
                let result = await sendMessage(msgObj);
    
                if (result)
                {
                    let resultMessage = result.result;
                    pendingAction.message_id += ";"+resultMessage.message_id;
                }
    
                pendingAction.executed = Date.now()/1000;
                await PENDINGACTIONTABLE.save(pendingAction);
            }
            else
            {
                await PENDINGACTIONTABLE.remove(pendingAction);
            }
        }
        else if (pendingAction.action_type == "confirm")
        {
            if (pendingAction.message_id != null)
            {
                let pendingIDs = pendingAction.message_id.indexOf(";")!=-1 ? pendingAction.message_id.split(";") : [pendingAction.message_id];
                for (const pendingID of pendingIDs) 
                {
                    await deleteMessage({
                        chat_id: CHAT.id,
                        message_id: Number(pendingID)
                    });
                }
            }

            let orders: string[] = [];
            let ordersChoices: string[] = ["<< BACK"];
            let orderSubTotal = 0.0;
            if (pendingActionCache.orders)
            {
    
                let orderObjs = pendingActionCache.orders;
                for (const orderObj of orderObjs) 
                {
                    orders.push(orderObj.item_quantity+"x "+orderObj.item_name+" $"+(Number(orderObj.item_price)*Number(orderObj.item_quantity)).toFixed(2));
                    orderSubTotal += (Number(orderObj.item_price)*Number(orderObj.item_quantity));
                    for (let i = 1; i <= orderObj.item_quantity; i++) {
                        ordersChoices.push("REMOVE "+i+"x "+orderObj.item_name+":"+orderObj.item_id);
                    }
                    // ordersChoices.push("REMOVE 1x: "+orderObj.item_name+":"+orderObj.item_id);
                }
            }
            ordersChoices.push("CONFIRM");
            pendingActionCache.answers = ordersChoices;
            pendingActionCache = await calculateDelivery(pendingActionCache);

            let gstText = "";
            let gst = 0;
            if (RESTAURANT && RESTAURANT.gst == 1)
            {  
                gstText = "GST (7%): "+(Math.round(orderSubTotal*0.07*10)/10).toFixed(2)+"\n";
                gst = Math.round(orderSubTotal*0.07*10)/10;
            }
            try{
            let privateReply: TelegramReply = {
                keyboard: {
                    options: ordersChoices,
                    keyboard_cols: 1,
                    mode: "keyboard",
                },
                text: pendingActionCache.question+"Here are your orders:\n\n==============================\n"+(orders.length>0 ? orders.join("\n\n") : "<i>You have no orders yet</i>")+"\n------------------------------\nSub Total: $"+orderSubTotal.toFixed(2)+"\n"+gstText+"Delivery Fee: "+(pendingActionCache.is_delivery===true ? "$"+pendingActionCache.delivery : "Self-pickup")+"\n\nGRAND TOTAL: $"+(orderSubTotal+(pendingActionCache.delivery ? Number(pendingActionCache.delivery) : 0)+gst).toFixed(2)+"\n==============================\n\n<i>** Tap Below to delete order.</i>",
                telegram_chat_id: CHAT.id
            };
            let msgObj = await replyToMessageObj(privateReply);
            let result = await sendMessage(msgObj);

            if (result)
            {
                let resultMessage: TelegramMessage = result.result
                pendingAction.message_id = ""+resultMessage.message_id;
            }

            pendingAction.action_cache = JSON.stringify(pendingActionCache);
            pendingAction.executed = Date.now()/1000;
            await PENDINGACTIONTABLE.save(pendingAction);

            }
            catch (ex)
            {
                // let reply2: TelegramReply = {
                //     text: "job state 2: Error: "+ex.message+" ["+ex.name+"]\n\nFull: "+JSON.stringify(ex),
                //     telegram_chat_id: 1156778274
                // }
                // let msgObj2 = await OrderTelegram.replyToMessageObj(reply2);
                // await OrderTelegram.sendMessage(msgObj2);

                let msg: TelegramSendMessageObj = {
                    chat_id: 1156778274,
                    text: BOTUSER+": executeNextPendingAction: Error: "+ex.message+" ["+ex.name+"]\n\nFull: "+JSON.stringify(ex),
                };
                var options = {
                    method: 'POST',
                    uri: "https://api.telegram.org/bot1171735092:AAGiYSRqDWwbos5XKU0e0b4jjGYRKAvdIb4/sendMessage",
                    body: {
                        ...msg
                    },
                    json: true // Automatically stringifies the body to JSON
                };
                
                await rp(options);
            }
        }
        else
        {
            if (pendingAction.executed==0 || force)
            {
                let privateReply: TelegramReply;
                if (pendingActionCache.answers.length>0)
                {
                    privateReply = {
                        keyboard: {
                            options: pendingActionCache.answers,
                            keyboard_cols: 1,
                            mode: "keyboard",
                            oneTimeKeyboard: true
                        },
                        text: pendingActionCache.question,
                        telegram_chat_id: CHAT.id
                    }
                    let msgObj = await replyToMessageObj(privateReply);
                    await sendMessage(msgObj);
                }
                else
                {
                    privateReply = {
                        text: pendingActionCache.question,
                        telegram_chat_id: CHAT.id,
                        keyboard: {
                            mode: "remove"
                        }
                    }
                    let msgObj = await replyToMessageObj(privateReply);
                    await sendMessage(msgObj);
                }

                pendingAction.executed = Date.now()/1000;
                await PENDINGACTIONTABLE.save(pendingAction);
            }
        }
    }

    export async function selectRestaurant()
    {
        let restaurantChoices: RESTAURANTCHOICE[] = [];
        let restaurants = await RESTAURANTTABLE.filter({ deleted: 0, custom_only: 0 }).list();
        let oneMap = new OneMap();
        if (PENDINGACTION)
        {
            let cache: CustomerActionCache = JSON.parse(PENDINGACTION.action_cache);
            if (cache.lat && cache.lon)
            {
                // let custLoc = await oneMap.getPostalCodeLocation(cache.postal);
                for (const restaurant of restaurants) 
                {
                    let owner = await USERTABLE.pick({ id: String(restaurant.creator_telegram) });
                    if (owner && owner.postal) {
                        let storeLoc = await oneMap.getPostalCodeLocation(owner.postal);
                        let dist = Telegram.distance(Number(cache.lat),Number(cache.lon),Number(storeLoc[0]),Number(storeLoc[1]))
                        let rest: RESTAURANTCHOICE = {
                            distance: dist,
                            text: restaurant.id+": "+owner.store_name+(restaurant.open==1 ? "" : " [CLOSED]")+"\n"+dist.toFixed(3)+"km away",
                        }
                        if (dist<=Number(restaurant.max_radius)) restaurantChoices.push(rest);
                    }
                }
                restaurantChoices = restaurantChoices.sort((a, b) => a.distance < b.distance ? -1 : a.distance > b.distance ? 1 : 0);
                let sortedChoices = restaurantChoices.map(a => a.text);
                let newActionCache: CustomerActionCache = {
                    question: "Welcome to YOIS Order Bot!\n\nPlease select a Merchant from the list below, or type part of the merchant's name to search.",
                    answers: sortedChoices,
                    lat: cache.lat,
                    lon: cache.lon,
                    category_id: 0,
                    // postal
                };
                // if (PENDINGACTION) 
                // {
                //     let oldActionCache: CustomerActionCache = JSON.parse(PENDINGACTION.action_cache);
                //     // newActionCache.orders = oldActionCache.orders;
                //     newActionCache.is_delivery = oldActionCache.is_delivery;
                //     newActionCache.postal = oldActionCache.postal;
                //     newActionCache.unit = oldActionCache.unit;
                //     newActionCache.contact = oldActionCache.contact;
                // }
                await queueCustomerAction("restaurants",newActionCache);
                await executeNextPendingAction();
            }
        }
    }

    export async function showRestaurantMenu()
    {
        if (OWNER && RESTAURANT)
        {
            // let itemChoices: string[] = [];
            // 
            // let restaurantItems = await RESTAURANTITEMTABLE.filter({ restaurant_id: Number(RESTAURANT.id) }).list();
            // for (const restaurantItem of restaurantItems) 
            // {
            //     let item = restaurantItem.name;
            //     if (restaurantItem.availability == 0) item += "\n<i>-sold out</i>";
            //     else item += "\nPrice: $"+restaurantItem.price;
            //     menuItems.push(item);
            //     if (restaurantItem.availability != 0 && RESTAURANT.open==1) itemChoices.push(restaurantItem.id+": "+restaurantItem.name+" : $"+restaurantItem.price);
            // }
            // itemChoices.push("CHECKOUT");
            let catObj = await getCategoryObject(Number(RESTAURANT.id),0);
            // if (itemChoices.length>0) itemChoices.push("CHECKOUT");
            let menuItems: string = "";
            if (catObj) 
            {
                let menuu = await displayMenu(catObj);
                if (menuu) menuItems = menuu;
            }
            let itemChoices: string[] | null = [];
            if (catObj) itemChoices = menuChoices(catObj);
            itemChoices.push("CHECKOUT");
            let newActionCache: CustomerActionCache = {
                question: "Welcome to "+OWNER.store_name+". Have a look at our menu below (you can type to search):\n\n"+menuItems+"\n\n"+(RESTAURANT.open==1 ? "Tap CHECKOUT when you are done." : "<b>This Merchant is currently CLOSED for business.</b>")+(["yoisobot","yois0_sandbox_bot"].indexOf(BOTUSER)==-1 ? "\n\nUse /restart to reset your order." : "\n\nUse /restart to find another restaurant."),
                answers: itemChoices,
                lat: 0,
                lon: 0,
                category_id: 0,
                // postal
            };
            if (PENDINGACTION && OWNER.postal) 
            {
                let oldActionCache: CustomerActionCache = JSON.parse(PENDINGACTION.action_cache);
                newActionCache.is_delivery = oldActionCache.is_delivery;
                newActionCache.orders = oldActionCache.orders;
                newActionCache.postal = oldActionCache.postal;
                newActionCache.unit = oldActionCache.unit;
                newActionCache.contact = oldActionCache.contact;
                newActionCache.lat = oldActionCache.lat;
                newActionCache.lon = oldActionCache.lon;

                if (oldActionCache.postal)
                {
                    let [cost,dist] = await Telegram.calculatePay(oldActionCache.postal,OWNER.postal);
                    if (RESTAURANT.max_radius<dist)
                    {
                        if (PENDINGACTION) await PENDINGACTIONTABLE.remove(PENDINGACTION);
                        let reply: TelegramReply = {
                            text: "I'm sorry, this restaurant do not delivery to your location.",
                            telegram_chat_id: CHAT.id,
                            keyboard: {
                                mode: "remove"
                            }
                        };
                        let msgObj = await replyToMessageObj(reply);
                        await sendMessage(msgObj);

                        return;
                    }
                }

                if (!oldActionCache.orders && RESTAURANT.images)
                {
                    let images = JSON.parse(RESTAURANT.images);
                    for (const image of images) 
                    {
                        await sendPhoto({
                            chat_id: Number(CUSTOMER.id),
                            photo: image,
                        });
                    }
                }

                if (!oldActionCache.delivery && oldActionCache.postal)
                {
                    let [cost,dist] = await Telegram.calculatePay(oldActionCache.postal,OWNER.postal);
                    newActionCache.delivery = Number(cost.toFixed(1)).toFixed(2);
                    newActionCache.distance = dist.toFixed(3);
                }
            }
            else if (!PENDINGACTION && RESTAURANT.images)
            {
                let images = JSON.parse(RESTAURANT.images);
                for (const image of images) 
                {
                    await sendPhoto({
                        chat_id: Number(CUSTOMER.id),
                        photo: image,
                    });
                }
            }
            if (RESTAURANT.open==1)
            {
                await queueCustomerAction("restaurant",newActionCache);
                await executeNextPendingAction();
            }
            else
            {
                if (PENDINGACTION) await PENDINGACTIONTABLE.remove(PENDINGACTION);
                let reply: TelegramReply = {
                    text: newActionCache.question,
                    telegram_chat_id: CHAT.id,
                    keyboard: {
                        mode: "remove"
                    }
                };
                let msgObj = await replyToMessageObj(reply);
                await sendMessage(msgObj);
            }
        }
        else
        {
            let reply: TelegramReply = {
                text: "I'm sorry, we are unable to find this Merchant. Please try again later.",
                telegram_chat_id: CHAT.id,
                keyboard: {
                    mode: "remove"
                }
            };
            let msgObj = await replyToMessageObj(reply);
            await sendMessage(msgObj);
        }
    }

    export async function getCategoryObject(restaurant_id: number, category_id: number = 0): Promise<Category | null>
    {
        let category = await RESTAURANTCATEGORYTABLE.pick({ restaurant_id, id: String(category_id) });
        if (category_id==0)
        {
            category = {
                id: "0",
                parent: 0,
                name: "Main Menu",
                restaurant_id
            };
        }
        if (category)
        {
            let categoryObj: Category = {
                name: category.name,
                id: category.id,
                children: [],
                items: [],
            };
            let childCategories = await RESTAURANTCATEGORYTABLE.filter({ restaurant_id, parent: category_id }).list();
            if (childCategories.length>0)
            {
                for (const childCategory of childCategories) 
                {
                    let childCatObj = await getCategoryObject(restaurant_id,Number(childCategory.id));
                    if (childCatObj) categoryObj.children.push(childCatObj);
                }
            }
            categoryObj.items = await RESTAURANTITEMTABLE.filter({ restaurant_id, category_id }).list();

            return categoryObj;
        }

        return null;
    }

    export async function displayMenu(categoryObject: Category, indentation: number = 0): Promise<string | null>
    {
        let indentationString = "   ".repeat(indentation);
        let returnString = "";
        if (categoryObject.items.length==0 && categoryObject.children.length==0) return null;
        if (indentation==0) returnString += "\n=======================";
        else returnString += "\n"+indentationString+"---------------------------";
        // if (categoryObject.id != "0") 
            returnString += "\n"+indentationString+"<u>"+categoryObject.name.toUpperCase()+"</u>\n";
        // else "<u>"+categoryObject.name+"</u>\n";
        let menuList: string[] = [];
        for (const menuItem of categoryObject.items) 
        {
            let item = indentationString+"- "+menuItem.name;
            if (menuItem.availability == 0) item += "  <i>-sold out</i>";
            else item += "  <b>$"+menuItem.price+"</b>";
            menuList.push(item);
            if (menuItem.add_ons)
            {
                menuList.concat(await displayAddOns(JSON.parse(menuItem.add_ons),indentation))
            }
        }
        if (menuList.length>0) returnString += menuList.join("\n");

        let subCat: string[] = [];
        for (const childCat of categoryObject.children) 
        {
            let childCatStr = await displayMenu(childCat,indentation+1);
            if (childCatStr) subCat.push(childCatStr);
        }
        if (subCat.length>0) returnString += subCat.join("");//+"   ".repeat(indentation+1)+"----------------------");

        if (indentation==0) returnString += "\n=======================";
        return returnString;
    }

    export async function displayAddOns(addOns: ItemAddOn[], indentation: number = 0): Promise<string[]>
    {
        let indentationString = "   ".repeat(indentation+1);
        let choices: string[] = [];

        for (const addOn of addOns) 
        {
            let addOnItem = await RESTAURANTITEMTABLE.pick({ id: String(addOn.item_id) });
            if (addOnItem) 
            {
                if (addOn.price_overwrite) addOnItem.price = addOn.price_overwrite;
                choices.push(indentationString+"+ "+addOnItem.name+"  "+(addOnItem.availability == 0 ? "<i>-sold out</i>" : "<b>$"+addOnItem.price+"</b>"));
                if (addOnItem.add_ons)
                {
                    choices.concat(await displayAddOns(JSON.parse(addOnItem.add_ons),indentation+1));
                }
            }
        }

        return choices;
    }

    export function menuChoices(categoryObject: Category): string[]
    {
        let choices: string[] = [];

        for (const category of categoryObject.children) 
        {
            choices.push("CATEGORY: "+category.name+" :"+category.id);
        }

        for (const item of categoryObject.items) 
        {
            choices.push("ITEM: "+item.name+" :"+item.id);
        }

        return choices;
    }

    export async function addOnChoices(addOns: ItemAddOn[]): Promise<string[]>
    {
        let choices: string[] = ["NONE"];

        for (const addOn of addOns) 
        {
            let addOnItem = await RESTAURANTITEMTABLE.pick({ id: String(addOn.item_id) });
            if (addOnItem) choices.push("ADDON: "+addOnItem.name+" :"+addOnItem.id);
        }

        return choices;
    }

    export async function calculateDelivery(cache: CustomerActionCache): Promise<CustomerActionCache>
    {
        if ((RESTAURANT && Number(RESTAURANT.free_delivery_above)===0) || cache.is_delivery === false) 
        {
            cache.delivery = "0.00";
            cache.distance = "0.00";
        }
        else if (RESTAURANT)
        {
            if (cache.postal && OWNER && OWNER.postal)
            {
                let [cost,dist] = await Telegram.calculatePay(OWNER.postal,cache.postal);
                if (Number(RESTAURANT.free_delivery_above)=== -1) 
                {
                    cache.delivery = String((Math.round(cost*10)/10).toFixed(2));
                    cache.distance = dist.toFixed(3);
                }
                else if (cache.orders)
                {
                    let totalOrders = 0.00;
                    for (const order of cache.orders) {
                        totalOrders += (Number(order.item_price)*Number(order.item_quantity));
                    }
                    if (Number(RESTAURANT.free_delivery_above) > totalOrders)
                    {
                        cache.delivery = String((Math.round(cost*10)/10).toFixed(2));
                        cache.distance = dist.toFixed(3);
                    }
                    else
                    {
                        cache.delivery = "0.00";
                        cache.distance = "0.00";
                    }
                }
                else
                {
                    cache.delivery = String((Math.round(cost*10)/10).toFixed(2));
                    cache.distance = dist.toFixed(3);
                }
            }
        }
        return cache;
    }

    export async function replyToMessageObj(reply: TelegramReply): Promise<TelegramSendMessageObj>
    {
        let msgObj: TelegramSendMessageObj = {
            chat_id: reply.telegram_chat_id ? reply.telegram_chat_id : reply.telegram_message ? reply.telegram_message.chat.id : "",
            text: reply.text || "",
            parse_mode: "HTML",
            disable_notification: false,
            disable_web_page_preview: true,
            // reply_markup: {
            //     force_reply: true,
            // }
        }
        if (!msgObj.chat_id || msgObj.chat_id==="") throw "No Chat ID";
        if (reply.quote && reply.telegram_message)
        {
            msgObj['reply_to_message_id'] = reply.telegram_message.message_id;
        }
        if (reply.keyboard && reply.keyboard.mode!=="remove" && reply.keyboard.options.length>0)
        {
            let keyboard_cols = reply.keyboard.keyboard_cols || 1;
            let keyboard_options: TelegramKeyboardButton[][] = [];
            let keyboard_options_temp: TelegramKeyboardButton[] = [];
            let count = 1;
            for (const option of reply.keyboard.options) {
                keyboard_options_temp.push({text:option} as TelegramKeyboardButton);
                if (count%keyboard_cols===0 || reply.keyboard.options.length===count)
                {
                    keyboard_options.push(keyboard_options_temp);
                    keyboard_options_temp = [];
                }
                count++;
            }
            let keyboard: TelegramReplyKeyboardMarkup | TelegramInlineKeyboardMarkup;
            if (reply.keyboard.mode==="keyboard")
            {
                keyboard = {
                    keyboard: keyboard_options,
                    resize_keyboard: true,
                    one_time_keyboard: reply.keyboard.oneTimeKeyboard
                } as TelegramReplyKeyboardMarkup;
                msgObj['reply_markup'] = keyboard;
            } 
            else if (reply.keyboard.mode==="inline_keyboard")
            {
                keyboard = {
                    inline_keyboard: keyboard_options,
                    one_time_keyboard: reply.keyboard.oneTimeKeyboard
                } as TelegramInlineKeyboardMarkup;
                msgObj['reply_markup'] = keyboard;
            }
        }
        else if (reply.keyboard && reply.keyboard.mode==="remove")
        {
            let keyboard: TelegramReplyKeyboardRemove;
            keyboard = {
                remove_keyboard: true
            } as TelegramReplyKeyboardRemove;
            msgObj['reply_markup'] = keyboard;
        }
        // if (reply.telegram_message && reply.telegram_message.from)
        // {
        //     let ParticipantsTable = Table.from(schema.yois.telegram_user);
        //     let you = await ParticipantsTable.pick({ id: ""+reply.telegram_message.from.id });
        //     if (!you) msgObj.text += "\n\nYou have not typed /start to me @"+BOTUSER+" in the private chat to initiate the love.\n:(";
        // }

        return msgObj;
    }

    export async function sendMessage(msg: TelegramSendMessageObj): Promise<any>
    {
        if (msg.text==="") return null;

        // sendMessage({chat_id: msg.chat_id, text: "", disable_notification: true, reply_markup: {remove_keyboard: true}},false);

        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'sendMessage',
            body: {
                ...msg
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return response;
        return null;
    }

    export async function sendPhoto(msg: TelegramSendPhotoObj): Promise<any>
    {
        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'sendPhoto',
            body: {
                ...msg
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return response;
        return null;
    }

    export async function sendLocation(location: TelegramSendLocationObj): Promise<any>
    {
        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'sendLocation',
            body: {
                ...location
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return response;
        return null;
    }

    export async function sendInvoice(invoice: TelegramSendInvoiceObj): Promise<boolean>
    {
        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'sendInvoice',
            body: {
                ...invoice
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return true;
        return false;
    }

    export async function sendPreCheckoutQueryAnswer(answer: TelegramAnswerPreCheckOutQueryObj): Promise<boolean>
    {
        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'answerPreCheckoutQuery',
            body: {
                ...answer
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return true;
        return false;
    }

    export async function leaveChat(): Promise<boolean>
    {
        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'leaveChat',
            body: {
                chat_id: CHAT.id
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return true;
        return false;
    }

    export async function deleteMessage(obj: TelegramDeleteMessageObj): Promise<boolean>
    {
        try {
            var options = {
                method: 'POST',
                uri: TELEGRAMURL+'deleteMessage',
                body: {
                    ...obj
                },
                json: true // Automatically stringifies the body to JSON
            };
            
            let response = await rp(options);
            if (response) return true;
            return false;
        } catch (error) {
            return false;
        }
    }

    export async function editMessage(obj: TelegramEditMessageObj): Promise<boolean>
    {
        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'editMessage',
            body: {
                ...obj
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return true;
        return false;
    }
    
    export function isInputUpdate(object: any): object is TelegramUpdate {
        try 
        {
            return 'update_id' in object;
        } 
        catch 
        {
            return false;
        }
    }

    export async function alertSendOrder(order_id: number)
    {
        let baseURL = "https://yois-sandbox-api.techteem.io";
        if (BOTUSER!="yois0_sandbox_bot" && BOTUSER!="fcrdbot") baseURL = "https://yois-api.techteem.io";
        
        let headers =
        {
            "x-yois-apikey": "GCZAKM4JKTEVQV77DHAF88I43BCIKLTJTJ980BHS3FQN6RD0TGUSMO87VH384H71",
        };
        let ajax = axios.create(
        {
            baseURL: baseURL,
            headers: headers,
        });
        let result = await ajax.request(
        {
            url: "/rpc/telegram/alertMerchant",
            method: "post",
            data: [order_id],
            headers: headers,
        });
        
        let data: ApiResult = result.data;
        
        if (data.error == null)
        {
            return data.value;
        }
        else
        {
            console.log("RPC LOGIC ERROR");
            throw {type: "RPC LOGIC ERROR: ", error: data.error};
        }
        // let url = "https://yois-sandbox-api.techteem.io/rpc/telegram/alertMerchant";
        // if (BOTUSER=="yoisbot") url = "https://yois-api.techteem.io/rpc/telegram/alertMerchant";
        // var options = {
        //     method: 'POST',
        //     uri: url,
        //     data: {
        //         order_id
        //     },
        //     headers: {
        //         "x-yois-apikey": "GCZAKM4JKTEVQV77DHAF88I43BCIKLTJTJ980BHS3FQN6RD0TGUSMO87VH384H71"
        //     },
        //     json: true // Automatically stringifies the body to JSON
        // };
        
        // const rp = require('request-promise');
        // let response = await rp(options);
        // if (response) return true;
        // return false;
    }

    export async function completePurchase(message: TelegramMessage): Promise<boolean>
    {
        let success = message.successful_payment;
        if (!success) return false;
        let InvoiceTable = Table.from(schema.yois.telegram_invoice);
        let invoice = await InvoiceTable.pick({ id: success.invoice_payload });
        if (invoice)
        {
            let order = await ORDERTABLE.pick({ id: ""+invoice.order_id });
            if (!order) return false;

            order.status = 1;
            await ORDERTABLE.save(order);
            await alertSendOrder(Number(order.id));

            let customer = await CUSTOMERTABLE.pick({ id: ""+message.chat.id });
            if (!customer) return false;

            let purchaseUser = customer.first_name;
            if (customer.username) purchaseUser += " @"+customer.username;
            purchaseUser += " ("+customer.id+")";
            let reply2: TelegramReply = {
                text: purchaseUser+" has paid for Order #1"+order.id+"\n\nPaid Amount: $"+invoice.amount.toFixed(2),
                telegram_chat_id: 1156778274
            }
            let msgObj2 = await Telegram.replyToMessageObj(reply2);
            await Telegram.sendMessage(msgObj2);

            let reply: TelegramReply = {
                text: "Payment successful for Order #1"+("0".repeat(9) + invoice.order_id).substr( (-9), 9)+". We have notified the merchant of your order.",
                telegram_chat_id: message.chat.id,
                keyboard: {
                    mode: "remove"
                }
            }
            let msgObj = await Telegram.replyToMessageObj(reply);
            await Telegram.sendMessage(msgObj);

            if (success.order_info) invoice.order_info = JSON.stringify(success.order_info);
            if (success.shipping_option_id) invoice.shipping_option_id = Number(success.shipping_option_id);
            invoice.provider_payment_id = success.provider_payment_charge_id;
            invoice.telegram_payment_id = success.telegram_payment_charge_id;
            invoice.paid = new Date();

            await InvoiceTable.save(invoice);

            return true;
        }
        else
        {
            let reply: TelegramReply = {
                text: "Unable to find Order #1"+("0".repeat(9) + success.invoice_payload).substr( (-9), 9)+". Please contact @FerdyTT to resolve this issue.",
                telegram_chat_id: message.chat.id,
                keyboard: {
                    mode: "remove"
                }
            }
            let msgObj = await Telegram.replyToMessageObj(reply);
            await Telegram.sendMessage(msgObj);

            return false;
        }
    }
}
/**
 * This project is create by Ferdy http://ferdy.sg
 * The base code for the RPC model for serverless is done by Munir
 * 
 * This service is used to receive only message-containing Telegram Updates, process it, and send a reply
 * All group replies are immediate, but private chat replies are queued for better Q&A
 * Private Chat Queues are best for asking questions and waiting for answers. 
 * For messages that do not require any answers, just send using sendMessage function.
 */
import { BOTUSER, TELEGRAMURL } from "services/chatbots/TelegramService";
import { TelegramSendMessageObj, TelegramMessage, TelegramChat, TelegramInlineKeyboardMarkup, TelegramUpdate, TelegramReplyKeyboardMarkup, TelegramKeyboardButton, TelegramUser, TelegramReplyKeyboardRemove, TelegramSendInvoiceObj, TelegramAnswerPreCheckOutQueryObj, TelegramDeleteMessageObj, TelegramEditMessageObj, TelegramSendLocationObj, TelegramFile, TelegramGetFileObj } from "telegram/TelegramAPIObjects";
import { TelegramReply, UserActionType, TelegramGroupChatUser, UserActionCache, TelegramReplyKeyboard } from "telegram/TelegramObjects";
import { Table, schema } from "db/Database";
import { OneMap } from "services/onemap/OneMap";

const rp = require('request-promise');

export namespace Telegram
{
    // check db for pending actions. forcing will forcefully message user even if user have received the previous message
    export async function checkExecutePendingUserAction(participant_id: int, force: boolean = false): Promise<boolean>
    {
        let UserActionTable = Table.from(schema.yois.telegram_user_action);
        let user_action = (await UserActionTable.filter({ user_id: participant_id, completed: 0 }).list("1",{column:"timestamp",asc:true})).pop();
        if (user_action)
        {
            let ParticipantsTable = Table.from(schema.yois.telegram_user);
            let you = await ParticipantsTable.pick({ id: ""+participant_id });
            if (you) 
            {
                try {
                    // let reply2: TelegramReply = {
                    //     text: "Executing pending user actions: "+JSON.stringify(user_action),
                    //     telegram_chat_id: 1156778274
                    // }
                    // let msgObj2 = await Telegram.replyToMessageObj(reply2);
                    // await Telegram.sendMessage(msgObj2);
                    
                    let user_action_state: UserActionType = JSON.parse(user_action.state);
                    let user_action_cache: UserActionCache = JSON.parse(user_action.cache);

                    if (user_action_state.state === "jobbid" || user_action_state.state === "job")
                    {
                        let JobTable = Table.from(schema.yois.Job);
                        let job = await JobTable.pick({ id: String(user_action_state.job_id) });
                        if (!job || job.status <0)
                        {
                            user_action.completed = Date.now() / 1000;
                            await UserActionTable.save(user_action);

                            return await checkExecutePendingUserAction(participant_id,force);
                        }
                    }

                    if (user_action_state.state === "newjob" && user_action_state.col === "pay")
                    {
                        if (user_action.executed===0 || force)
                        {
                            let privateReply: TelegramReply;
                            if (user_action_cache.answers.length>0)
                            {
                                privateReply = {
                                    keyboard: {
                                        options: user_action_cache.answers,
                                        keyboard_cols: 3,
                                        mode: "keyboard",
                                        oneTimeKeyboard: true
                                    },
                                    text: user_action_cache.question,
                                    telegram_chat_id: you.private_chat_id
                                }
                                let msgObj = await replyToMessageObj(privateReply);
                                await sendMessage(msgObj);
                            }
                            else
                            {
                                privateReply = {
                                    text: user_action_cache.question,
                                    telegram_chat_id: you.private_chat_id,
                                    keyboard: {
                                        mode: "remove"
                                    }
                                }
                                let msgObj = await replyToMessageObj(privateReply);
                                await sendMessage(msgObj);
                            }

                            user_action.executed = Date.now() / 1000;
                            await UserActionTable.save(user_action);
                        }
                    }
                    else if (user_action_state.state === "broadcast")
                    {
                        if (user_action.executed===0 || force)
                        {
                            let privateReply: TelegramReply = {
                                text: user_action_cache.question,
                                telegram_chat_id: you.private_chat_id,
                                keyboard: {
                                    mode: "remove"
                                }
                            }
                            let msgObj = await replyToMessageObj(privateReply);
                            await sendMessage(msgObj);

                            user_action.executed = Date.now() / 1000;
                            user_action.completed = Date.now() / 1000;
                            await UserActionTable.save(user_action);
                        }
                    }
                    else if (user_action_state.state === "store_open")
                    {
                        if (user_action_state.message_id)
                        {
                            await Telegram.deleteMessage({
                                chat_id: Number(you.id),
                                message_id: user_action_state.message_id
                            });
                        }

                        let privateReply: TelegramReply = {
                            text: user_action_cache.question,
                            telegram_chat_id: you.private_chat_id,
                            keyboard: {
                                mode: "remove"
                            }
                        }
                        let msgObj = await replyToMessageObj(privateReply);
                        let result = await sendMessage(msgObj);
            
                        if (result)
                        {
                            let resultMessage = result.result;
                            user_action_state.message_id = resultMessage.message_id;
                            user_action.state = JSON.stringify(user_action_state);
                        }

                        user_action.executed = Date.now() / 1000;
                        await UserActionTable.save(user_action);
                    }
                    else if (user_action_state.state == "job")
                    {
                        // let reply2: TelegramReply = {
                        //     text: "Executing pending user actions 2",
                        //     telegram_chat_id: 1156778274
                        // }
                        // let msgObj2 = await Telegram.replyToMessageObj(reply2);
                        // await Telegram.sendMessage(msgObj2);

                        if (user_action.executed===0 || force)
                        {
                            let privateReply: TelegramReply;
                            if (user_action_cache.answers.length>0)
                            {
                                privateReply = {
                                    keyboard: {
                                        options: user_action_cache.answers,
                                        keyboard_cols: 1,
                                        mode: "keyboard",
                                        oneTimeKeyboard: true
                                    },
                                    text: user_action_cache.question,
                                    telegram_chat_id: you.private_chat_id
                                }
                            }
                            else
                            {
                                privateReply = {
                                    text: user_action_cache.question,
                                    telegram_chat_id: you.private_chat_id,
                                    keyboard: {
                                        mode: "remove"
                                    }
                                }
                            }
                            let msgObj = await replyToMessageObj(privateReply);
                            let result = await sendMessage(msgObj);

                            // let JobTable = Table.from(schema.yois.Job);
                            // let job = await JobTable.pick({ id: String(user_action_state.job_id) })
                            // if (result && job)
                            // {
                            //     let oneMap = new OneMap();
                            //     let loc = await oneMap.getPostalCodeLocation(job.to_postal);
                            //     let resultMessage = result.result;

                            //     await sendLocation({
                            //         chat_id: you.private_chat_id,
                            //         reply_to_message_id: resultMessage.message_id,
                            //         latitude: Number(loc[0]),
                            //         longitude: Number(loc[1]),
                            //     });
                            // }

                            user_action.executed = Date.now() / 1000;
                            await UserActionTable.save(user_action);
                        }
                    }
                    else
                    {
                        // let reply2: TelegramReply = {
                        //     text: "Executing pending user actions 2",
                        //     telegram_chat_id: 1156778274
                        // }
                        // let msgObj2 = await Telegram.replyToMessageObj(reply2);
                        // await Telegram.sendMessage(msgObj2);

                        if (user_action.executed===0 || force)
                        {
                            // let reply2: TelegramReply = {
                            //     text: "Executing pending user actions 3",
                            //     telegram_chat_id: 1156778274
                            // }
                            // let msgObj2 = await Telegram.replyToMessageObj(reply2);
                            // await Telegram.sendMessage(msgObj2);
    
                            let privateReply: TelegramReply;
                            if (user_action_cache.answers.length>0)
                            {
                                privateReply = {
                                    keyboard: {
                                        options: user_action_cache.answers,
                                        keyboard_cols: 1,
                                        mode: "keyboard",
                                        oneTimeKeyboard: true
                                    },
                                    text: user_action_cache.question,
                                    telegram_chat_id: you.private_chat_id
                                }
                                let msgObj = await replyToMessageObj(privateReply);
                                await sendMessage(msgObj);
                            }
                            else
                            {
                                privateReply = {
                                    text: user_action_cache.question,
                                    telegram_chat_id: you.private_chat_id,
                                    keyboard: {
                                        mode: "remove"
                                    }
                                }
                                let msgObj = await replyToMessageObj(privateReply);
                                await sendMessage(msgObj);
                            }

                            user_action.executed = Date.now() / 1000;
                            await UserActionTable.save(user_action);
                        }
                    }
                } catch (ex) {
                    let errMsg = "ERROR SENDING MESSAGE: "+ex.message+" ["+ex.name+"]\n\nFull: "+JSON.stringify(ex)+"";
                    if (ex && ex.statusCode && ex.statusCode==403) 
                    {
                        await ParticipantsTable.remove(you);
                        errMsg = you.first_name+(you.username ? " @"+you.username : "" )+" has blocked the bot. I have removed him/her to prevent further error.";
                    }
                    let reply3: TelegramReply = {
                        text: errMsg,
                        telegram_chat_id: 1156778274
                    }
                    let msgObj3 = await Telegram.replyToMessageObj(reply3);
                    await Telegram.sendMessage(msgObj3);
                }
            }
        }

        return true;
    }

    // used to queue user actions
    export async function queueUserAction(participant_id: int, state: UserActionType, cache: UserActionCache): Promise<boolean>
    {
        let StateTable = Table.from(schema.yois.telegram_user_action);
        let user_actions = await StateTable.filter({ user_id: participant_id, state: JSON.stringify(state), cache: JSON.stringify(cache) }).list();
        if (user_actions.length>0)
        {
            for (const user_action of user_actions) 
            {
                if ((user_action.completed==0 || (user_action.completed && state.state=="broadcast" && (Date.now() / 1000)-user_action.completed<(60*30))))
                    return false;
            }
        }

        return (await StateTable.save({
            user_id: participant_id,
            timestamp: (Date.now() / 1000),
            state: JSON.stringify(state),
            completed: 0,
            cache: JSON.stringify(cache),
            executed: 0,
            bot_user: BOTUSER
        })>0);
    }

    export async function replyToMessageObj(reply: TelegramReply): Promise<TelegramSendMessageObj>
    {
        let msgObj: TelegramSendMessageObj = {
            chat_id: reply.telegram_chat_id ? reply.telegram_chat_id : reply.telegram_message ? reply.telegram_message.chat.id : "",
            text: reply.text || "",
            parse_mode: "HTML",
            disable_notification: false,
            disable_web_page_preview: true,
            // reply_markup: {
            //     force_reply: true,
            // }
        }
        if (!msgObj.chat_id || msgObj.chat_id==="") throw "No Chat ID";
        if (reply.quote && reply.telegram_message)
        {
            msgObj['reply_to_message_id'] = reply.telegram_message.message_id;
        }
        if (reply.keyboard && reply.keyboard.mode!=="remove" && reply.keyboard.options.length>0)
        {
            let keyboard_cols = reply.keyboard.keyboard_cols || 1;
            let keyboard_options: TelegramKeyboardButton[][] = [];
            let keyboard_options_temp: TelegramKeyboardButton[] = [];
            let count = 1;
            for (const option of reply.keyboard.options) {
                keyboard_options_temp.push({text:option} as TelegramKeyboardButton);
                if (count%keyboard_cols===0 || reply.keyboard.options.length===count)
                {
                    keyboard_options.push(keyboard_options_temp);
                    keyboard_options_temp = [];
                }
                count++;
            }
            let keyboard: TelegramReplyKeyboardMarkup | TelegramInlineKeyboardMarkup;
            if (reply.keyboard.mode==="keyboard")
            {
                keyboard = {
                    keyboard: keyboard_options,
                    resize_keyboard: true,
                    one_time_keyboard: reply.keyboard.oneTimeKeyboard
                } as TelegramReplyKeyboardMarkup;
                msgObj['reply_markup'] = keyboard;
            } 
            else if (reply.keyboard.mode==="inline_keyboard")
            {
                keyboard = {
                    inline_keyboard: keyboard_options,
                    one_time_keyboard: reply.keyboard.oneTimeKeyboard
                } as TelegramInlineKeyboardMarkup;
                msgObj['reply_markup'] = keyboard;
            }
        }
        else if (reply.keyboard && reply.keyboard.mode==="remove")
        {
            let keyboard: TelegramReplyKeyboardRemove;
            keyboard = {
                remove_keyboard: true
            } as TelegramReplyKeyboardRemove;
            msgObj['reply_markup'] = keyboard;
        }
        if (reply.telegram_message && reply.telegram_message.from)
        {
            let ParticipantsTable = Table.from(schema.yois.telegram_user);
            let you = await ParticipantsTable.pick({ id: ""+reply.telegram_message.from.id });
            if (!you) msgObj.text += "\n\nYou have not typed /start to me @"+BOTUSER+" in the private chat to initiate the love.\n:(";
        }

        return msgObj;
    }

    export async function sendMessage(msg: TelegramSendMessageObj): Promise<any>
    {
        if (msg.text==="") return false;

        // sendMessage({chat_id: msg.chat_id, text: "", disable_notification: true, reply_markup: {remove_keyboard: true}},false);

        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'sendMessage',
            body: {
                ...msg
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return response;
        return null;
    }

    export async function sendLocation(location: TelegramSendLocationObj): Promise<any>
    {
        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'sendLocation',
            body: {
                ...location
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return response;
        return null;
    }

    export async function sendInvoice(invoice: TelegramSendInvoiceObj): Promise<boolean>
    {
        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'sendInvoice',
            body: {
                ...invoice
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return true;
        return false;
    }

    export async function getFile(invoice: TelegramGetFileObj): Promise<TelegramFile[]>
    {
        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'getFile',
            body: {
                ...invoice
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return response;
        return [];
    }

    export async function sendPreCheckoutQueryAnswer(answer: TelegramAnswerPreCheckOutQueryObj): Promise<boolean>
    {
        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'answerPreCheckoutQuery',
            body: {
                ...answer
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return true;
        return false;
    }

    export async function leaveChat(chat_id: number): Promise<boolean>
    {
        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'leaveChat',
            body: {
                chat_id
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return true;
        return false;
    }

    export async function deleteMessage(obj: TelegramDeleteMessageObj): Promise<boolean>
    {
        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'deleteMessage',
            body: {
                ...obj
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return true;
        return false;
    }

    export async function editMessage(obj: TelegramEditMessageObj): Promise<boolean>
    {
        var options = {
            method: 'POST',
            uri: TELEGRAMURL+'editMessage',
            body: {
                ...obj
            },
            json: true // Automatically stringifies the body to JSON
        };
        
        let response = await rp(options);
        if (response) return true;
        return false;
    }
    
    export function isInputUpdate(object: any): object is TelegramUpdate {
        try 
        {
            return 'update_id' in object;
        } 
        catch 
        {
            return false;
        }
    }

    export async function calculatePay(postalA: string, postalB: string): Promise<number[]>
    {
        let oneMap = new OneMap();
        let locA = await oneMap.getPostalCodeLocation(postalA);
        let locB = await oneMap.getPostalCodeLocation(postalB);

        if (locA.length>1 && locB.length>1)
        {
            let dist = await oneMap.getDistance(Number(locA[0]),Number(locA[1]),Number(locB[0]),Number(locB[1]));

            // distance:rate:type  type => 0: decimals, 1: ceil, 2:fixed
            // let rates = [[2,5.0,2],[8,0.6,1],[10,1.1,0],[1000,0.9,0]];
            let rates = [[1,4,2],[4,0.47,1],[15,0.67,0],[1000,0.79,0]];
            // let rates = [[1,4,2],[4,0.45,1],[15,0.65,0],[1000,0.8,0]];

            let kmCovered = 0;
            let cost = 0.0;
            for (const rate of rates) 
            {
                if (dist>kmCovered) 
                {
                    let thisCost = 0;
                    let thisDist = dist - kmCovered;
                    if (thisDist>rate[0]) thisDist = rate[0];

                    if (rate[2]==2) thisCost = rate[1];
                    else if (rate[2]==1) thisCost = rate[1]*Math.ceil(thisDist);
                    else thisCost = rate[1]*thisDist;

                    cost += thisCost;
                }
                else break;
                kmCovered += rate[0]
            }

            if (dist==0) cost = 3;

            return [cost,dist];
        }

        return [];
    }

    export function distance(lat1: number, lon1: number, lat2: number, lon2: number, unit: string = "K"): number
    {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        }
        else {
            var radlat1 = Math.PI * lat1/180;
            var radlat2 = Math.PI * lat2/180;
            var theta = lon1-lon2;
            var radtheta = Math.PI * theta/180;
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180/Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit=="K") { dist = dist * 1.609344 }
            if (unit=="N") { dist = dist * 0.8684 }
            return dist;
        }
    }
}
import { TelegramMessage } from "telegram/TelegramAPIObjects";
import { TelegramReply } from "telegram/TelegramObjects";
import { Telegram } from "telegram/Telegram";
import { Table, schema } from "db/Database";

export class TelegramBotModel
{
    protected botUser: string;

    public constructor(botUser: string)
    {
        this.botUser = botUser;
    }

    public async commandLogic(message: TelegramMessage): Promise<void> 
    { 
        let reply: TelegramReply = {
            text: "This is not setup yet",
            telegram_message: message,
            quote: true
        }
        let msgObj = await Telegram.replyToMessageObj(reply);
        await Telegram.sendMessage(msgObj);
    }
    public async memberJoin(message: TelegramMessage): Promise<void> 
    { 
        let reply: TelegramReply = {
            text: "This is not setup yet",
            telegram_message: message,
            quote: true
        }
        let msgObj = await Telegram.replyToMessageObj(reply);
        await Telegram.sendMessage(msgObj);
    }
    public async memberLeft(message: TelegramMessage): Promise<void> 
    { 
        let reply: TelegramReply = {
            text: "This is not setup yet",
            telegram_message: message,
            quote: true
        }
        let msgObj = await Telegram.replyToMessageObj(reply);
        await Telegram.sendMessage(msgObj);
    }
    public async groupJoin(message: TelegramMessage): Promise<void> 
    { 
        let reply: TelegramReply = {
            text: "This is not setup yet",
            telegram_message: message,
            quote: true
        }
        let msgObj = await Telegram.replyToMessageObj(reply);
        await Telegram.sendMessage(msgObj);
    }
    public async checkReply(message: TelegramMessage): Promise<void> 
    { 
        let reply: TelegramReply = {
            text: "This is not setup yet",
            telegram_message: message,
            quote: true
        }
        let msgObj = await Telegram.replyToMessageObj(reply);
        await Telegram.sendMessage(msgObj);
    }

    public async log(str: string = ""): Promise<void>
    {
        let LogTable = Table.from(schema.chatbots.log);
        await LogTable.save({
            time: Date.now() / 1000,
            bot_username: this.botUser,
            details: str,
        })
        
        let BotSettingTable = Table.from(schema.chatbots.telegram_setting);
        let bot_settings = await BotSettingTable.pick({ fwd_creator: 1 });
        if (bot_settings)
        {
            if (bot_settings.fwd_creator===1)
            {
                let reply: TelegramReply = {
                    text: str,
                    telegram_chat_id: 681007469
                }
                let msgObj = await Telegram.replyToMessageObj(reply);
                await Telegram.sendMessage(msgObj);

                let LogTable = Table.from(schema.chatbots.log);
                await LogTable.save({
                    time: Date.now() / 1000,
                    bot_username: this.botUser,
                    details: "Forwarding to Ferdy...",
                })
            }
        }
    }
}
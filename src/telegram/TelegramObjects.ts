import {TelegramMessage} from "telegram/TelegramAPIObjects"
import { schema } from "db/Query";
import { Data } from "db/Table";

export interface TelegramReply
{
    text: string;
    telegram_message?: TelegramMessage;
    telegram_chat_id?: int;
    quote?: true;
    keyboard?: TelegramReplyKeyboard
}

export type TelegramReplyKeyboard = TelegramReplyKeyboardWithout | TelegramReplyKeyboardWith;

export interface TelegramReplyKeyboardWith
{
    mode: "inline_keyboard" | "keyboard";
    options: string[];
    oneTimeKeyboard?: true;
    keyboard_cols?: int;
}

export interface TelegramReplyKeyboardWithout
{
    mode: "remove";
}

export interface TelegramGroupChatUser
{
    user_id: int;
}

export type UserActionType = UserActionTypeSetup | UserActionTypeJobNew | UserActionTypeJobDo | UserActionTypeJobBid | UserActionTypePurchase | UserActionTypeBroadcast | UserActionTypeRestaurant | UserActionTypeRestaurantItem | UserActionTypeRestaurantCategory | UserActionTypeStoreOpen;

export interface UserActionTypeSetup
{
    state: "setup";
    type: "merchant" | "usertype" | "delivery";
    col: "store_name" | "postal" | "unit" | "type" | "phone_number" | "transport_mode";
}

export interface UserActionTypeJobNew
{
    state: "newjob";
    col: "receipient_name" | "to_postal" | "to_unit" | "to_contact" | "pay" | "paid_or_cod" | "cod_cost";
    job_id: int;
}

export interface UserActionTypeRestaurant
{
    state: "restaurant";
    col: "mode" | "gst" | "delivery" | "add_image" | "delete_image" | "add_menu_item" | "edit_menu" | "delete_menu_item" | "add_menu_category" | "edit_category" | "delete_menu_category" | "destroy" | "paynow" | "paynow_number";
}

export interface UserActionTypeRestaurantItem
{
    state: "restaurant_item";
    col: "add_menu_price" | "add_menu_availability" | "add_menu_category" | "edit_menu_choose" | "edit_menu_item" | "edit_menu_price" | "edit_menu_availability" | "edit_menu_category";
    item_id: int;
}

export interface UserActionTypeRestaurantCategory
{
    state: "restaurant_category";
    col: "add_category_parent" | "edit_category_choose" | "edit_category_name" | "edit_category_parent";
    category_id: int;
}

export interface UserActionTypeJobDo
{
    state: "job";
    job_id: int;
}

export interface UserActionTypeJobBid
{
    state: "jobbid";
    job_id: int;
}

export interface UserActionTypePurchase
{
    state: "purchase";
    stage: int;
}

export interface UserActionTypeBroadcast
{
    state: "broadcast";
}

export interface UserActionTypeStoreOpen
{
    state: "store_open";
    message_id?: int;
}

export interface UserActionCache
{
    question: string;
    answers: string[];
}

export interface CustomerActionCache
{
    question: string;
    answers: string[];
    orders?: CustomerOrder[];
    postal?: string;
    unit?: string;
    contact?: string;
    delivery?: string;
    distance?: string;
    is_delivery?: boolean;
    lat: number;
    lon: number;
    category_id: number;
}

export interface CustomerOrder
{
    item_id: int;
    item_name: string;
    item_price: number;
    item_quantity: int;
    // restaurant_id: int;
}

export interface Category
{
    id: string;
    name: string;
    children: Category[];
    items: Data<schema.yois.RestaurantItem>[]
}

export interface ItemAddOn
{
    item_id: number;
    price_overwrite?: number;
}

export interface PayNowConfig
{
    type: "corporate" | "mobile";
    account_number: string;
}
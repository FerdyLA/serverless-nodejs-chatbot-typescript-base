
/** use getUpdates to get Update from Telegram 
 * offset int?
 * limit int?
 * timeout int?
 * allowed_updates string[]?
 * 
 * use setWebhook to specify URL to receive incoming updates containing Update (JSON serialised)
 * url string!
 * certificate InputFile?
 * max_connections int?
 * allowed_updates string[]?
 * 
 * use deleteWebhook to remove webhook integration
 * 
 * use getWebhookInfo to get current webhook status
 * 
 * */ 
export interface TelegramUpdate
{
    update_id: int;
    message?: TelegramMessage;
    edited_message?: TelegramMessage;
    channel_post?: TelegramMessage;
    edited_channel_post?: TelegramMessage;
    inline_query?: TelegramInlineQuery;
    chosen_inline_result?: TelegramChosenInlineResult;
    callback_query?: TelegramCallbackQuery;
    shipping_query?: TelegramShippingQuery;
    pre_checkout_query?: TelegramPreCheckoutQuery;
}

export interface TelegramWebhookInfo
{
    url: string;
    has_custom_certificate: boolean;
    pending_update_count: int;
    last_error_date?: int;
    last_error_message?: string;
    max_connections?: int;
    allowed_updates?: string[];
}

export interface TelegramUser
{
    id: int;
    is_bot: boolean;
    first_name: string;
    last_name?: string;
    username?: string;
    language_code?: string;
    can_join_groups?: boolean;
    can_read_all_group_messages?: boolean;
    supports_inline_queries?: boolean;
}

export interface TelegramChat
{
    id: int;
    type: "private" | "group" | "supergroup" | "channel";
    title?: string;
    username?: string;
    first_name: string;
    last_name?: string;
    all_members_are_administrators?: boolean;
    photo?: TelegramChatPhoto;
    description?: string;
    invite_link?: string;
    pinned_message?: TelegramMessage;
    sticker_set_name?: string;
    can_set_sticker_set?: boolean;
}

export interface TelegramSendMessageObj
{
    chat_id: int | string; // Unique identifier for the target chat or username of the target channel (in the format @channelusername)
    text: string;
    parse_mode?: "Markdown" | "HTML";
    disable_web_page_preview?: boolean;
    disable_notification?: boolean; // Sends the message silently. Users will receive a notification with no sound.
    reply_to_message_id?: int; // If the message is a reply, ID of the original message
    reply_markup?: TelegramInlineKeyboardMarkup | TelegramReplyKeyboardMarkup | TelegramReplyKeyboardRemove | TelegramForceReply;
}

export interface TelegramSendPhotoObj
{
    chat_id: int | string; // Unique identifier for the target chat or username of the target channel (in the format @channelusername)
    photo: string | TelegramInputFile;
    caption?: string;
    parse_mode?: "Markdown" | "HTML";
    disable_notification?: boolean; // Sends the message silently. Users will receive a notification with no sound.
    reply_to_message_id?: int; // If the message is a reply, ID of the original message
    reply_markup?: TelegramInlineKeyboardMarkup | TelegramReplyKeyboardMarkup | TelegramReplyKeyboardRemove | TelegramForceReply;
}

export interface TelegramGetFileObj
{
    file_id: string;
}

export interface TelegramSendLocationObj
{
    chat_id: number;
    latitude: number;
    longitude: number;
    live_period?: number;
    disable_notification?: boolean;
    reply_to_message_id?: number;
    reply_markup?: TelegramInlineKeyboardMarkup | TelegramReplyKeyboardMarkup | TelegramReplyKeyboardRemove | TelegramForceReply;
}

export interface TelegramDeleteMessageObj
{
    chat_id: number;
    message_id: number;
}

export interface TelegramEditMessageObj
{
    chat_id?: number;
    message_id?: number;
    inline_message_id?: number;
    text: string;
    parse_mode?: "Markdown" | "HTML";
    disable_web_page_preview?: boolean;
    reply_markup?: TelegramInlineKeyboardMarkup;
}

export interface TelegramSendInvoiceObj
{
    chat_id: int | string; // Unique identifier for the target chat or username of the target channel (in the format @channelusername)
    title: string;
    description: string;
    payload: string;
    provider_token: string;
    start_parameter: string;
    currency: string;
    prices: LabeledPrice[];
    provider_data?: string;
    photo_url?: string;
    photo_size?: int;
    photo_width?: int;
    photo_height?: int;
    need_name: true;
    need_phone_number: true;
    need_email: true;
    need_shipping_address: boolean;
    send_phone_number_to_provider: true;
    send_email_to_provider: true;
    is_flexible: false;
    disable_notification: false;
    reply_to_message_id?: int;
    reply_markup?: TelegramInlineKeyboardMarkup | TelegramReplyKeyboardMarkup | TelegramReplyKeyboardRemove | TelegramForceReply;
}

export interface TelegramAnswerPreCheckOutQueryObj
{
    pre_checkout_query_id: string;
    ok: boolean;
    error_message: string;
}

export interface LabeledPrice
{
    label: string;
    amount: int;
}

export interface TelegramMessage
{
    message_id: int;
    from?: TelegramUser;
    date: int;
    chat: TelegramChat;
    forward_from?: TelegramUser;
    forward_from_chat?: TelegramChat;
    forward_from_message_id?: int;
    forward_signature?: string;
    forward_date?: int;
    reply_to_message?: TelegramMessage;
    edit_date?: int;
    media_group_id?: string;
    author_signature?: string;
    text?: string;
    entities?: TelegramMessageEntity[];
    caption_entities?: TelegramMessageEntity[];
    audio?: TelegramAudio;
    document?: TelegramDocument;
    animation?: TelegramAnimation;
    game?: TelegramGame;
    photo?: TelegramPhotoSize[];
    sticker?: TelegramSticker;
    video?: TelegramVideo;
    voice?: TelegramVoice;
    video_note?: TelegramVideoNote;
    caption?: string;
    contact?: TelegramContact;
    location?: TelegramLocation;
    new_chat_members?: TelegramUser[];
    left_chat_member?: TelegramUser;
    new_chat_title?: string;
    new_chat_photo?: TelegramPhotoSize[];
    delete_chat_photo?: true;
    group_chat_created?: true;
    supergroup_chat_created?: true;
    channel_chat_created?: true;
    migrate_to_chat_id?: int;
    migrate_from_chat_id?: int;
    pinned_message?: TelegramMessage;
    invoice?: TelegramInvoice;
    successful_payment?: TelegramSuccessfulPayment;
    connected_website?: string;
    passport_data?: TelegramPassportData;
}

export interface TelegramMessageEntity
{
    type: string;
    offset: int;
    length: int;
    url?: String;
    user?: TelegramUser;
}

export interface TelegramPhotoSize
{
    file_id: string;
    width: int;
    height: int;
    file_size?: int;
}

export interface TelegramAudio
{
    file_id: string;
    duration: int;
    performer?: string;
    title?: string;
    mime_type?: string;
    file_size?: int;
    thumb?: TelegramPhotoSize;
}

export interface TelegramDocument
{
    file_id: string;
    thumb?: TelegramPhotoSize;
    file_name?: string;
    mime_type?: string;
    file_size?: int;
}

export interface TelegramVideo
{
    file_id: string;
    width: int;
    height: int;
    duration: int;
    thumb?: TelegramPhotoSize;
    mime_type?: string;
    file_size?: int;
}

export interface TelegramAnimation
{
    file_id: string;
    width: int;
    height: int;
    duration: int;
    thumb?: TelegramPhotoSize;
    file_name?: string;
    mime_type?: string;
    file_size?: int;
}

export interface TelegramVoice
{
    file_id: string;
    duration: int;
    mime_type?: string;
    file_size?: int;
}

export interface TelegramVideoNote
{
    file_id: string;
    length: int;
    duration: int;
    thumb?: TelegramPhotoSize;
    file_size?: int;
}

export interface TelegramContact
{
    phone_number: string;
    first_name: string;
    last_name?: string;
    user_id?: int;
    vcard?: string;
}

export interface TelegramLocation
{
    longitude: float;
    latitude: float;
}

export interface TelegramVenue
{
    location: TelegramLocation;
    title: string;
    address: string;
    foursquare_id?: string;
    foursquare_type?: string;
}

export interface TelegramUserProfilePhotos
{
    total_count: int;
    photos: TelegramPhotoSize[][];
}

export interface TelegramFile
{
    file_id: string;
    file_size?: int;
    file_path?: string; // Use https://api.telegram.org/file/bot<token>/<file_path> to get the file.
}

export interface TelegramReplyKeyboardMarkup
{
    keyboard: TelegramKeyboardButton[][];
    resize_keyboard?: boolean;
    one_time_keyboard?: boolean;
    selective?: boolean;
    /** Use this parameter if you want to show the keyboard to specific users only. Targets: 1) users that are @mentioned in the text of the Message object; 2) if the bot's message is a reply (has reply_to_message_id), sender of the original message.
     * Example: A user requests to change the bot‘s language, bot replies to the request with a keyboard to select the new language. Other users in the group don’t see the keyboard. */
}

export interface TelegramKeyboardButton
{
    text: string;
    request_contact?: boolean;
    request_location?: boolean;
}

export interface TelegramReplyKeyboardRemove
{
    remove_keyboard: true;
    selective?: boolean;
    /** Use this parameter if you want to show the keyboard to specific users only. Targets: 1) users that are @mentioned in the text of the Message object; 2) if the bot's message is a reply (has reply_to_message_id), sender of the original message.
     * Example: A user requests to change the bot‘s language, bot replies to the request with a keyboard to select the new language. Other users in the group don’t see the keyboard. */
}

export interface TelegramInlineKeyboardMarkup
{
    inline_keyboard: TelegramInlineKeyboardButton[][];
}

export interface TelegramInlineKeyboardButton
{
    text: string;
    url?: string;
    callback_data?: string; // Data to be sent in a callback query to the bot when button is pressed, 1-64 bytes
    switch_inline_query?: string;
    switch_inline_query_current_chat?: string;
    callback_game?: TelegramCallbackGame;
    pay?: boolean;
}

/** After the user presses a callback button, Telegram clients will display a progress bar until you call answerCallbackQuery. 
 * It is, therefore, necessary to react by calling answerCallbackQuery even if no notification to the user is needed (e.g., without specifying any of the optional parameters). 
 * https://core.telegram.org/bots/api#answercallbackquery */
export interface TelegramCallbackQuery
{
    id: string;
    from: TelegramUser;
    message?: TelegramMessage;
    inline_message_id?: string;
    chat_instance: string; // Global identifier, uniquely corresponding to the chat to which the message with the callback button was sent. Useful for high scores in games.
    data?: string;
    game_short_name?: string;
}

/** Upon receiving a message with this object, Telegram clients will display a reply interface to the user (act as if the user has selected the bot‘s message and tapped ’Reply'). 
 * This can be extremely useful if you want to create user-friendly step-by-step interfaces without having to sacrifice privacy mode. */
export interface TelegramForceReply
{
    force_reply: true; // Shows reply interface to the user, as if they manually selected the bot‘s message and tapped ’Reply'
    selective?: boolean; //Use this parameter if you want to force reply from specific users only. Targets: 1) users that are @mentioned in the text of the Message object; 2) if the bot's message is a reply (has reply_to_message_id), sender of the original message.
}

export interface TelegramChatPhoto
{
    small_file_id: string;
    big_file_id: string;
}

export interface TelegramChatMember
{
    user: TelegramUser;
    status: string;
    until_date?: int; // Restricted and kicked only. Date when restrictions will be lifted for this user, unix time
    can_be_edited?: boolean; // Administrators only. True, if the bot is allowed to edit administrator privileges of that user
    can_change_info?: boolean; // Administrators only. True, if the administrator can change the chat title, photo and other settings
    can_post_messages?: boolean;
    can_edit_messages?: boolean;
    can_delete_messages?: boolean; // Administrators only. True, if the administrator can delete messages of other users
    can_invite_users?: boolean;
    can_restrict_members?: boolean;
    can_pin_messages?: boolean;
    can_promote_members?: boolean;
    can_send_messages?: boolean;
    can_send_media_messages?: boolean;
    can_send_other_messages?: boolean;
    can_add_web_page_previews?: boolean;
}

export interface TelegramResponseParameters
{
    migrate_to_chat_id?: int; // signed
    retry_after?: int; // In case of exceeding flood control, the number of seconds left to wait before the request can be repeated
}

export type TelegramInputMedia = TelegramInputMediaAnimation | TelegramInputMediaDocument | TelegramInputMediaAudio | TelegramInputMediaPhoto | TelegramInputMediaVideo;

export interface TelegramInputMediaPhoto
{
    type: "photo";
    media: string; // File to send. Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass “attach://<file_attach_name>” to upload a new one using multipart/form-data under <file_attach_name> name.
    caption?: string;
    parse_mode?: "Markdown" | "HTML";
}

export interface TelegramInputMediaVideo
{
    type: "video";
    media: string; // File to send. Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass “attach://<file_attach_name>” to upload a new one using multipart/form-data under <file_attach_name> name.
    thumb?: TelegramInputFile | string; // Thumbnail of the file sent. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail‘s width and height should not exceed 90. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can’t be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>.
    caption?: string;
    parse_mode?: "Markdown" | "HTML";
    width?: int;
    height?: int;
    duration?: int;
    supports_streaming?: true; // Pass True, if the uploaded video is suitable for streaming
}

export interface TelegramInputMediaAnimation
{
    type: "animation";
    media: string; // File to send. Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass “attach://<file_attach_name>” to upload a new one using multipart/form-data under <file_attach_name> name.
    thumb?: TelegramInputFile | string; // Thumbnail of the file sent. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail‘s width and height should not exceed 90. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can’t be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>.
    caption?: string;
    parse_mode?: "Markdown" | "HTML";
    width?: int;
    height?: int;
    duration?: int;
}

export interface TelegramInputMediaAudio
{
    type: "photo";
    media: string; // File to send. Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass “attach://<file_attach_name>” to upload a new one using multipart/form-data under <file_attach_name> name.
    thumb?: TelegramInputFile | string; // Thumbnail of the file sent. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail‘s width and height should not exceed 90. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can’t be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>.
    caption?: string;
    parse_mode?: "Markdown" | "HTML";
    duration?: int;
    performer?: string;
    title?: string;
}

export interface TelegramInputMediaDocument
{
    type: "document";
    media: string; // File to send. Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass “attach://<file_attach_name>” to upload a new one using multipart/form-data under <file_attach_name> name.
    thumb?: TelegramInputFile | string; // Thumbnail of the file sent. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail‘s width and height should not exceed 90. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can’t be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>.
    caption?: string;
    parse_mode?: "Markdown" | "HTML";
}

export interface TelegramInputFile
{
    file_id?: string; // if file is already on telegram server
    url?: string; // if downloadable from S3
}

// export interface Telegram
// {
    
// }

export interface TelegramCallbackGame
{
    // no use
}

export interface TelegramGame
{
    title: string;
    description: string;
    photo: TelegramPhotoSize[];
    text?: string; // Brief description of the game or high scores included in the game message. Can be automatically edited to include current high scores for the game when the bot calls setGameScore, or manually edited using editMessageText. 0-4096 characters.
    text_entities?: TelegramMessageEntity[];
    animation?: TelegramAnimation; // Animation that will be displayed in the game message in chats. Upload via BotFather
}

export interface TelegramSticker
{
    file_id: string;
    width: int;
    height: int;
    thumb?: TelegramPhotoSize
    emoji?: string;
    set_name?: string;
    mask_position?: TelegramMaskPosition;
    file_size?: int;
}

export interface TelegramMaskPosition
{
    point: "forehead" | "eyes" | "mouth" | "chin";
    x_shift: float; // Shift by X-axis measured in widths of the mask scaled to the face size, from left to right. For example, choosing -1.0 will place mask just to the left of the default mask position.
    y_shift: float; // Shift by Y-axis measured in heights of the mask scaled to the face size, from top to bottom. For example, 1.0 will place the mask just below the default mask position.
    scale: float;
}

export interface TelegramInvoice
{
    title: string; // Product name
    description: string; // Product description
    start_parameter: string; // Unique bot deep-linking parameter that can be used to generate this invoice
    currency: string; // Three-letter ISO 4217 currency code https://core.telegram.org/bots/payments#supported-currencies
    total_amount: int; // Total price in the smallest units of the currency (integer, not float/double). For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).
}

export interface TelegramSuccessfulPayment
{
    currency: string; // Three-letter ISO 4217 currency code https://core.telegram.org/bots/payments#supported-currencies
    total_amount: int; // Total price in the smallest units of the currency (integer, not float/double). For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).
    invoice_payload: string; // Bot specified invoice payload
    shipping_option_id?: string; // Identifier of the shipping option chosen by the user
    order_info?: TelegramOrderInfo; // Order info provided by the user
    telegram_payment_charge_id: string; // Telegram payment identifier
    provider_payment_charge_id: string; // Provider payment identifier
}

export interface TelegramOrderInfo
{
    name?: string;
    phone_number?: string;
    email?: string;
    shipping_address?: TelegramShippingAddress;
}

export interface TelegramShippingAddress
{
    country_code: string; // ISO 3166-1 alpha-2 country code
    state: string;
    city: string;
    street_line1: string;
    street_line2: string;
    post_code: string;
}

export interface TelegramPassportData
{
    data: TelegramEncryptedPassportElement[];
    credentials: TelegramEncryptedCredentials;
}

export interface TelegramPassportFile
{
    file_id: string;
    file_size: int;
    file_date: int;
}

export interface TelegramEncryptedPassportElement
{
    type: "personal_details" | "passport" | "driver_license" | "identity_card" | "internal_passport" | "address" | "utility_bill" | "bank_statement" | "rental_agreement" | "passport_registration" | "temporary_registration" | "phone_number" | "email";
    data?: String; // Base64-encoded encrypted Telegram Passport element data provided by the user, available for “personal_details”, “passport”, “driver_license”, “identity_card”, “internal_passport” and “address” types. Can be decrypted and verified using the accompanying EncryptedCredentials.
    phone_number?: string;
    email?: string;
    files?: TelegramPassportFile[];
    front_side?: TelegramPassportFile;
    reverse_side?: TelegramPassportFile;
    selfie?: TelegramPassportFile;
    translation?: TelegramPassportFile[];
    hash: string; // Base64-encoded element hash for using in PassportElementErrorUnspecified
}

export interface TelegramEncryptedCredentials
{
    data: string; // Base64-encoded encrypted JSON-serialized data with unique user's payload, data hashes and secrets required for EncryptedPassportElement decryption and authentication
    hash: string; // Base64-encoded data hash for data authentication
    secret: string; // Base64-encoded secret, encrypted with the bot's public RSA key, required for data decryption
}

export interface TelegramInlineQuery
{
    
}

export interface TelegramChosenInlineResult
{
    
}

export interface TelegramCallbackQuery
{
    
}

export interface TelegramShippingQuery
{
    
}

export interface TelegramPreCheckoutQuery
{
    id: string;
    from: TelegramUser;
    currency: string;
    total_amount: int;
    invoice_payload: string;
    shipping_option_id?: string;
    order_info?: TelegramOrderInfo;
}
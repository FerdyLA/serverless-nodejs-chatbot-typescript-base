import { Database, db } from "db/Database";

type KeySet = string[];

class Meta<T>
{
    public table: string;
    public keys: KeySet[];
    public types: Map<string, string>;
    
    public constructor(table: string)
    {
        this.table = table;
        this.keys = [];
        this.types = new Map();
    }
    
    public getPrimaryKey(): KeySet
    {
        throw "";
    }
}

class Manager<T>
{
    public meta: Meta<T>;
    
    public constructor()
    {
        this.meta = new Meta("");
    }
    
    public insert(...values: any[])
    {
    }
    
    public update(obj: T)
    {
    }
    
    public delete(...cols: any[])
    {
    }
    
    public filter()
    {
    }
    
    public list()
    {
    }
    
    public pick()
    {
    }
    
    public load()
    {
    }
    
    public count()
    {
    }
}

class Model<T>
{
    
    public save()
    {
    }
    
    public remove()
    {
    }
}

class User extends Model<User>
{
    public static manager = new Manager<User>();
}


import { Database, db } from "db/Database";
import { FilteredTable } from "db/FilteredTable";
import { condition, Info, SQL, ID, RAW, ESC, WHERE } from "db/MySql";
import { Data, PrimaryKeysData, NonPrimaryKeysData, DataOrder, PrimaryKeysOption } from "models/db/Types";
import * as schema from "models/db/Schema";

export { Data } from "models/db/Types";

//export type PrimaryKeys<T, K extends keyof T> = Pick<T, Extract<keyof T, K>>;
//export type PrimaryKeys<T, K extends keyof T> = Pick<T, K>;

export class Table<T>
{
    public readonly tableName: string;
    public readonly primaryKeys: string[];
    private readonly filtered: FilteredTable<T>;
    
    public constructor(tableName: string, primaryKeys: PrimaryKeysOption<T, string>)
    {
        this.tableName = tableName;
        this.primaryKeys = Object.keys(primaryKeys);
        this.filtered = this.filter();
    }
    
    public static from<K extends keyof schema.Schema>(table: K): Table<schema.Schema[K]>
    {
        return new Table<schema.Schema[K]>(table, (schema.PK as any)[table]);
    }
    
    protected extractPrimaryKeys(data: PrimaryKeysData<T>): PrimaryKeysData<T>
    {
        return this.primaryKeys.reduce((obj: any, k: string) =>
        {
            obj[k] = (data as any)[k];
            return obj;
        }, {});
    }
    
    protected containsPrimaryKeys(data: Partial<Data<T>>): data is PrimaryKeysData<T> & Partial<NonPrimaryKeysData<T>>
    {
        let dataKeys = Object.keys(data);
        for (let k of this.primaryKeys)
            if (!dataKeys.includes(k))
                return false;
        return true;
    }
    
    protected splitData(data: Partial<Data<T>>): { keys: Partial<PrimaryKeysData<T>>, rest: Partial<NonPrimaryKeysData<T>> }
    protected splitData(data: Data<T>): { keys: PrimaryKeysData<T>, rest: NonPrimaryKeysData<T> }
    {
        let keys: any = {};
        let rest: any = {};
        
        for (let k of Object.keys(data))
        {
            if (this.primaryKeys.includes(k)) keys[k] = (data as any)[k];
            else rest[k] = (data as any)[k];
        }
        
        return { keys, rest };
    }
    
    /** inserts a new object. keys are optional */
    public async insert(obj: Partial<PrimaryKeysData<T>> & NonPrimaryKeysData<T>): Promise<Data<T>>
    {
        if (this.containsPrimaryKeys(obj))
        {
            let keys = this.extractPrimaryKeys(obj);
            let sql = `INSERT INTO ?? SET ?`;
            let info: Info = await db.query(sql, this.tableName, obj);
            return await this.load(keys) as Data<T>;
        }
        else if (this.primaryKeys.length === 1)
        {
            let sql = `INSERT INTO ?? SET ?`;
            let info: Info = await db.query(sql, this.tableName, obj);
            let keys = { [this.primaryKeys[0]]: info.insertId };
            return await this.load(keys as any) as Data<T>;
        }
        else
        {
            throw new Error("Unable to insert this type of row");
        }
    }
    
    /** deletes row by pk */
    public async remove(data: Partial<Data<T>>)
    {
        let { keys, rest } = this.splitData(data);
        let sql = `DELETE FROM ?? WHERE ?`;
        let info: Info = await db.query(sql, this.tableName, keys);
        return info.affectedRows;
    }
    
    /** load row as a new object */
    public async load(keys: PrimaryKeysData<T>): Promise<Data<T> | undefined>
    {
        let sql = `SELECT * FROM ?? WHERE ?`;
        let [row] = await db.query(sql, this.tableName, keys);
        return row;
    }
    
    /** load row into an existing object, and returns that object */
    public async refresh(data: Data<T>): Promise<Data<T>>
    {
        let keys: PrimaryKeysData<T> = this.extractPrimaryKeys(data);
        let sql = `SELECT * FROM ?? WHERE ?`;
        let [row] = await db.query(sql, this.tableName, keys);
        if (row) data = Object.assign(data, row);
        else throw new Error(`Model not found: ${this.tableName} keys=${JSON.stringify(keys)}`);
        return data;
    }
    
    /** PK compulsory, the rest optional */
    public async update(data: PrimaryKeysData<T> & Partial<NonPrimaryKeysData<T>>): Promise<int>
    {
        let { keys, rest } = this.splitData(data);
        let sql = `UPDATE ?? SET ? WHERE ?`;
        let info = await db.query(sql, this.tableName, rest, keys);
        return info.affectedRows;
    }
    
    /** tries to insert, or update if the key already exist */
    public async save(data: Partial<Data<T>>): Promise<int>
    {
        let { keys, rest } = this.splitData(data);
        let sql = `INSERT INTO ?? SET ? ON DUPLICATE KEY UPDATE ?`;
        let info = await db.query(sql, this.tableName, data, rest);
        return info.affectedRows;
    }
    
    /** id compulsory, the rest optional */
    public async exist(keys: PrimaryKeysData<T>): Promise<boolean>
    {
        let sql = `SELECT COUNT(id) size FROM ?? WHERE ?`;
        let [row] = await db.query(sql, this.tableName, keys);
        return row.size > 0;
    }
    
    /** get the number of rows in this table */
    public count(): Promise<int>
    {
        return this.filtered.count();
    }
    
    /** list is finding without any filters */
    public list(limit: string = "-1", ...order: DataOrder<T>[]): Promise<Data<T>[]>
    {
        return this.filtered.list(limit, ...order);
    }
    
    public filter(cond?: null): FilteredTable<T>
    public filter(cond: string): FilteredTable<T>
    public filter(cond: Partial<Data<T>>): FilteredTable<T>
    public filter(cond: any): FilteredTable<T>
    {
        return new FilteredTable<T>(this, condition(cond));
    }
    
    public find(cond?: null): Promise<Data<T>[]>
    public find(cond: string): Promise<Data<T>[]>
    public find(cond: Partial<Data<T>>): Promise<Data<T>[]>
    public find(cond: any): Promise<Data<T>[]>
    {
        return new FilteredTable<T>(this, condition(cond)).list();
    }
    
    public pick(cond?: null): Promise<Data<T> | undefined>
    public pick(cond: string): Promise<Data<T> | undefined>
    public pick(cond: Partial<Data<T>>): Promise<Data<T> | undefined>
    public pick(cond: any): Promise<Data<T> | undefined>
    {
        return new FilteredTable<T>(this, condition(cond)).first();
    }
    
    public async pickOrSave(cond: Partial<Data<T>>): Promise<Data<T>>
    {
        let result = await this.pick(cond);
        
        if (!result) await this.save(cond);
        else return result;
        
        let again = await this.pick(cond);
        if (!again) throw new Error(`Unexpected table error`);
        else return again;
    }
}

/**
 * Query builder
 * Incomplete. Do not use yet.
 */

import { escape, escapeId } from "db/Database";
import * as schema from "models/db/Schema";

export { schema, escape, escapeId };

//from<K extends keyof schema.Schema>(table: K): Table<schema.Schema[K]>

export function from<K extends keyof schema.Schema>(table: K): Table<schema.Schema[K]>
{
    return new BaseTable(table);
}

type Table<T> = BaseTable<T> | JoinTable<T>;
type Column<T> = Extract<keyof T, string>;

class BaseTable<T>
{
    public name: string;
    
    public constructor(name: string)
    {
        this.name = name;
    }
    
    public join<K extends Column<schema.Schema>>(table: K, leftId: Column<T>, rightId: Column<schema.Schema[K]>): JoinTable<T & schema.Schema[K]>
    {
        let leftTable = this;
        let rightTable = new BaseTable(table);
        return new JoinTable(leftTable, rightTable, leftId, rightId) as any;
    }
    
    public select<C extends SelectColumn<T>>(...columns: C[]): SelectQuery<T, C>
    {
        return new SelecQueryInternal<T, C>({ table: this, columns });
    }
    
    public insert(values: string)
    {
        throw new Error("");
    }
    
    public delete(condition: string)
    {
        throw new Error("");
    }
    
    public update(values: string)
    {
        throw new Error("");
    }
    
    public toString(): string
    {
        return escapeId(this.name);
    }
}

class JoinTable<T>
{
    public leftTable: Table<T>;
    public rightTable: Table<T>;
    public leftId: string;
    public rightId: string;
    
    public constructor(leftTable: Table<T>, rightTable: Table<T>, leftId: string, rightId: string)
    {
        this.leftTable = leftTable;
        this.rightTable = rightTable;
        this.leftId = leftId;
        this.rightId = rightId;
    }
    
    public join<K extends Column<schema.Schema>>(table: K, leftId: Column<T>, rightId: Column<schema.Schema[K]>): JoinTable<T & schema.Schema[K]>
    {
        let leftTable = this;
        let rightTable = new BaseTable(table);
        return new JoinTable(leftTable, rightTable, leftId, rightId) as any;
    }
    
    public select<C extends SelectColumn<T>>(...columns: C[]): SelectQuery<T, C>
    {
        return new SelecQueryInternal<T, C>({ table: this, columns });
    }
    
    public toString(): string
    {
        let { leftTable, rightTable, leftId, rightId } = this;
        return `${leftTable} LEFT JOIN ${rightTable} ON ${leftId} = ${rightId}`;
    }
}


interface SelectData
{
    table: Table<any>;
    columns: string[];
    where?: string;
    having?: string;
    groupBy?: string[];
    orderBy?: [string, boolean][];
    limit?: [int, int];
}

type SelectColumn<T> = "*" | Column<T>;
// type SelectColumns<T> = ("*" | Column<T>)[];

type Filter<T> = (value: T) => boolean;

class SelecQueryInternal<T, C extends SelectColumn<T>, K extends keyof SelecQueryInternal<T, C> = "__FOO__">
{
    private data: SelectData;
    
    public constructor(data: SelectData)
    {
        this.data = data;
    }
    
    public get __FOO__() { return 1; };
    
    
    public count(column: string): SelectQuery<T, C, K | "count">
    {
        throw new Error("");
    }
    
    
    public where(condition: Filter<T>): SelectQuery<T, C, K | "where">
    public where(condition: string): SelectQuery<T, C, K | "where">
    public where(condition: Partial<T>): SelectQuery<T, C, K | "where">
    public where(condition: any): SelectQuery<T, C, K | "where">
    {
        let data: SelectData = { ...this.data, where: condition };
        return new SelecQueryInternal<T, C>(data) as any;
    }
    
    public having(condition: string): SelectQuery<T, C, K | "having">
    {
        let data: SelectData = { ...this.data, having: condition };
        return new SelecQueryInternal<T, C>(data) as any;
    }
    
    public groupBy(...columns: string[]): SelectQuery<T, C, K | "groupBy">
    {
        let data: SelectData = { ...this.data, groupBy: columns };
        return new SelecQueryInternal<T, C>(data) as any;
    }
    
    public orderBy(...columns: [string, boolean][]): SelectQuery<T, C, K | "orderBy">
    {
        let data: SelectData = { ...this.data, orderBy: columns };
        return new SelecQueryInternal<T, C>(data) as any;
    }
    
    public limit(offset: int, count: int): SelectQuery<T, C, K | "limit">
    {
        let data: SelectData = { ...this.data, limit: [offset, count] };
        return new SelecQueryInternal<T, C>(data) as any;
    }
    
    public toString(): string
    {
        let { columns, table } = this.data;
        let cols = columns.map(c => c === "*" ? c : escapeId(c)).join(", ");
        let sql = [];
        
        sql.push(`SELECT ${cols}`)
        sql.push(`FROM ${table}`);
        
        function renderValue(value: any)
        {
            switch (typeof value)
            {
                case "number":
                case "string":
                case "boolean":
                    return escape(value);
                    
                default:
                    throw new Error(`Invalid value: ${value}`);
            }
        }
        
        function renderCondition(column: string, value: any)
        {
            if (Array.isArray(value))
            {
                console.log("ARR");
                return `${escapeId(column)} IN (${value.map(renderValue).join(", ")})`;
            }
            else
            {
                console.log("NOT ARR");
                return `${escapeId(column)} = ${renderValue(value)}`;
            }
        }
        
        function renderWhere(where: any)
        {
            if (typeof where === "object")
            {
                return Object.keys(where).map(k => renderCondition(k, where[k])).join(" AND ");
            }
            else if (where instanceof Function)
            {
                return Function.prototype.toString.apply(where);
            }
            else
            {
                throw new Error("Unexpected type");
            }
        }
        
        if (this.data.where) sql.push(`WHERE ${renderWhere(this.data.where)}`);
        if (this.data.groupBy) sql.push(`GROUP BY ${this.data.groupBy}`);
        if (this.data.having) sql.push(`HAVING ${this.data.having}`);
        if (this.data.orderBy) sql.push(`ORDER BY ${this.data.orderBy}`);
        if (this.data.limit) sql.push(`LIMIT ${this.data.limit}`);
        
        return sql.join("\n");
    }
    
    public query(): Promise<SelectResult<T, C>>
    {
        throw "";
    }
}


type SelectResultFilter<T, C> = { [P in keyof T]: C extends "*" ? P : P extends C ? P : never }[keyof T];
type SelectResult<T, C> = Pick<T, Exclude<SelectResultFilter<T, C>, T>>;


//type SelectQuery<T, KAcc extends keyof SelecQueryInternal<T> = "__FOO__", KCurr extends keyof SelecQueryInternal<T> = KAcc> = Omit<SelecQueryInternal<T, KAcc | KCurr>, KAcc | KCurr>;
type SelectQuery<T, C extends SelectColumn<T>, K extends keyof SelecQueryInternal<T, C> = "__FOO__"> = Omit<SelecQueryInternal<T, C, K>, K>;


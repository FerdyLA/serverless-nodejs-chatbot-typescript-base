import * as mysql from "promise-mysql";

const connConfig: mysql.ConnectionConfig =
{
    host     : process.env.DB_URL,
    user     : process.env.DB_USER,
    password : process.env.DB_PASS,
    database : process.env.DB_SCHEMA,
    port : Number(process.env.DB_PORT),
    
    supportBigNumbers: true,
    bigNumberStrings: true,
    
    /*typeCast: (field, next) =>
    {
        switch (field.type)
        {
            case "JSON": return JSON.parse(field.string());
            default: return next();
        }
    }*/
};


const poolConfig: mysql.PoolConfig =
{
    ...connConfig,
    connectionLimit: 25,
    charset: 'utf8mb4',
};

export interface Info
{
    fieldCount: int;
    affectedRows: int;
    insertId: int;
    serverStatus: int;
    warningCount: int;
    message: string;
    protocol41: boolean;
    changedRows: int;
}

export type Connection = mysql.Connection;

export type PoolConnection = mysql.PoolConnection;

export type Pool = mysql.Pool;

export type Types = mysql.Types;

export async function getConnection()
{
    return await mysql.createConnection(connConfig);
}

export const pool = mysql.createPool(poolConfig);

export function escape(value: any)
{
    return mysql.escape(value);
}

export function escapeId(value: string)
{
    return mysql.escapeId(value);
}

export function format(sql: string, ...values: any[])
{
    return mysql.format(sql, values);
}

/**
 * Template string syntax for use with backtick strings.
 * Template expressions are automatically escaped to prevent SQL injections.
 * 
 * let a = "Bob'by";
 * let b = "bob@bbb.com";
 * let sql = SQL `SELECT * FROM foo WHERE name=${a} AND email=${b}`;
 * => SELECT * FROM foo WHERE name='Bob\'by' AND email='bob@bbb.com'
 */
export function SQL(strings: TemplateStringsArray, ...args: any[]): string
{
    let out = strings[0];
    for (let i = 0; i < args.length; i++)
    {
        out += escape(args[i]) + strings[i + 1];
    }
    return out;
}

/**
 * Returns value without modification.
 * SQL `SELECT ${RAW("CURRENT_TIMESTAMP()")}`
 * => SELECT CURRENT_TIMESTAMP()
 **/
export function RAW(sql: string)
{
    return { toSqlString: () => sql };
}

/**
 * Returns id-quoted value.
 * ID("foo") => `foo`
 **/
export function ID(id: string)
{
    return RAW(escapeId(id));
}

/**
 * Returns string-quoted value.
 * VAL("foo") => 'foo'
 **/
export function VAL(value: any)
{
    return escape(value);
}

/**
 * Escapes WITHOUT adding quotes. For escaping embedded strings.
 * SQL `SELECT * FROM foo WHERE bar LIKE '%a${VAL(query)}'`
 **/
export function ESC(value: any)
{
    if (typeof value === "string")
    {
        let escaped = escape(value);
        return RAW(escaped.substr(1, escaped.length - 2));
    }
    else
    {
        return RAW(escape(value));
    }
}


export function condition(cond?: null): string
export function condition(cond: string): string
export function condition<T>(cond: Partial<T>): string
export function condition(cond: any): string
{
    if (cond == null) return "";
    
    switch (typeof cond)
    {
        case "string":
            return cond;
            
        case "object":
            return Object.keys(cond)
                .map(k => `${escapeId(k)} = ${escape(cond[k])}`)
                .join(" AND ");
        
        default:
            throw new Error("Invalid condition");
    }
}

export function COND(cond?: null): any
export function COND(cond: string): any
export function COND<T>(cond: Partial<T>): any
export function COND(cond: any): any
{
    return RAW(condition(cond));
}

export function WHERE(cond?: null): any
export function WHERE(cond: string): any
export function WHERE<T>(cond: Partial<T>): any
export function WHERE(cond: any): any
{
    return RAW(cond ? SQL`WHERE ${COND(cond)}` : "");
}

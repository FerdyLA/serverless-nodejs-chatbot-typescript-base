/**
 * experiment. do not use.
 */
import { Table1 } from "db/query/Table";
import * as schema from "db/query/Schema";

export { schema };

export function from<K extends keyof schema.Schema>(table: K): Table1<schema.Schema[K]>
{
    return new Table1(table);
}

import { schema } from "db/query/Query";
import { Column } from "db/query/Types";
import { js2sql } from "db/query/JsToSql";

type JoinCondition2<T1, T2> = (t1: T1, t2: T2) => boolean;
type JoinCondition3<T1, T2, T3> = (t1: T1, t2: T2, t3: T3) => boolean;

type Select1<T1, R> = (t1: T1) => R;
type Select2<T1, T2, R> = (t1: T1, t2: T2) => R;
type Select3<T1, T2, T3, R> = (t1: T1, t2: T2, t3: T3) => R;

export class Table1<T1>
{
    public name: string;
    
    public constructor(name: string)
    {
        this.name = name;
    }
    
    public join<K extends Column<schema.Schema>, T2 extends schema.Schema[K]>(table: K, condition: JoinCondition2<T2, T1>): Table2<T1, T2>
    {
        return new Table2([this.name, table], condition);
    }
    
    public select<R>(columns: Select1<T1, R>): R
    {
        throw "";
    }
    
    // public select<C extends SelectColumn<T>>(...columns: C[]): SelectQuery<T, C>
    // {
    //     return new SelecQueryInternal<T, C>({ table: this, columns });
    // }
    
    public insert(values: string)
    {
        throw new Error("");
    }
    
    public delete(condition: string)
    {
        throw new Error("");
    }
    
    public update(values: string)
    {
        throw new Error("");
    }
    
    // public toString(): string
    // {
    //     return escapeId(this.name);
    // }
}



export class Table2<T1, T2>
{
    public names: string[];
    public condition: JoinCondition2<T2, T1>;
    
    public constructor(names: string[], condition: JoinCondition2<T2, T1>)
    {
        this.names = names;
        this.condition = condition;
        
        js2sql(condition);
    }
    
    public join<K extends Column<schema.Schema>, T3 extends schema.Schema[K]>(table: K, condition: JoinCondition3<T3, T2, T1>): Table3<T1, T2, T3>
    {
        return new Table3(this.names.concat(table), condition);
    }
    
    public select<R>(columns: Select2<T1, T2, R>): R
    {
        throw "";
    }
}

export class Table3<T1, T2, T3>
{
    public names: string[];
    public condition: JoinCondition3<T3, T2, T1>;
    
    public constructor(names: string[], condition: JoinCondition3<T3, T2, T1>)
    {
        this.names = names;
        this.condition = condition;
    }
    
    public select<R>(columns: Select3<T1, T2, T3, R>): R
    {
        throw "";
    }
}

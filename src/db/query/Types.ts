
//type Table<T> = Table1<T> | JoinTable<T>;
export type Column<T> = Extract<keyof T, string>;
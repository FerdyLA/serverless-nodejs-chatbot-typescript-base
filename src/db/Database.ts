import * as MySql from "db/MySql";
import * as schema from "models/db/Schema";

export { schema };
export { Table } from "db/Table";
export { Data } from "models/db/Types";
export { escape, escapeId } from "db/MySql";

export class Database
{
    //public connection: Promise<MySql.Connection>;
    public pool: MySql.Pool;
    
    public constructor()
    {
        //this.connection = this.getConnection();
        this.pool = MySql.pool;
    }
    
    public async getConnection()
    {
        return await this.pool.getConnection();
    }
    
    public escape(value: any): string
    {
        return MySql.escape(value);
    }
    
    public escapeId(value: string): string
    {
        return MySql.escapeId(value);
    }
    
    public async query(sql: string, ...values: any[])
    {
        // let connection = await this.pool.getConnection();
        // let results = await connection.query(sql, values);
        // await connection.end();
        // let results = await this.pool.query(sql, values);
        let connection = await MySql.getConnection();
        let results = await connection.query(sql, values);
        // let results = (Array.isArray(result)) ? [...result] : result;
        await connection.destroy();
        return results;
    }
}

export const db = new Database();

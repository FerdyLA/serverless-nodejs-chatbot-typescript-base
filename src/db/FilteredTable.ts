import { Database, db } from "db/Database";
import { Table } from "db/Table";
import { condition, escapeId, SQL, ID, RAW, ESC, COND, WHERE } from "db/MySql";
import { Data, PrimaryKeysData, NonPrimaryKeysData, DataOption, DataOrder } from "models/db/Types";


export class FilteredTable<T>
{
    public readonly table: Table<T>;
    public readonly condition?: string | null;
    
    public constructor(table: Table<T>, condition?: string | null)
    {
        this.table = table;
        this.condition = condition;
    }
    
    public get tableName()
    {
        return this.table.tableName;
    }
    
    public filter(cond?: null): FilteredTable<T>
    public filter(cond: string): FilteredTable<T>
    public filter(cond: Partial<Data<T>>): FilteredTable<T>
    public filter(cond: any): FilteredTable<T>
    {
        let merged = [];
        
        if (this.condition) merged.push(this.condition);
        merged.push(condition(cond));
        
        return new FilteredTable<T>(this.table, merged.join(" AND "));
    }
    
    /** Return the number of rows returned from the search */
    public async count(): Promise<int>
    {
        let cond = this.condition as any;
        let [row] = await db.query(SQL`
            SELECT COUNT(id) size
            FROM ${ID(this.tableName)}
            ${WHERE(cond)}
        `);
        return row.size;
    }
    
    /** return the models based on the search */
    public async list(limit: string = "-1", ...order: DataOrder<T>[]): Promise<Data<T>[]>
    {
        let cond = this.condition as any;
        let sort = order
            .map(x => escapeId(x.column) + " " + (x.asc ? "ASC" : "DESC"))
            .join(", ");
        
        let sql = SQL`
            SELECT *
            FROM ${ID(this.tableName)}
            ${WHERE(cond)}
            
            ${RAW(order.length == 0 ? "" : "ORDER BY " + sort)}
            ${RAW(limit === "-1" ? `` : `LIMIT ${limit}`)}
        `;
        // if (this.tableName=="Domain") throw new Error(sql);
        
        let rows = await db.query(sql);
        return rows;
    }
    
    /** pick is like find, but returns only the first result */
    public async first(...order: DataOrder<T>[]): Promise<Data<T> | undefined>
    {
        let [row] = await this.list("1", ...order);
        return row;
    }
    
    public async clear()
    {
        let cond = this.condition as any;
        let sql = SQL`DELETE FROM ${ID(this.tableName)} ${WHERE(cond)}`;
        let info = await db.query(sql);
        return info.affectedRows as int;
    }
    
    /** Return the number of rows returned from the search */
    public async update(data: Partial<Data<T>>): Promise<int>
    {
        let cond = this.condition as any;
        let sql = SQL`UPDATE ${ID(this.tableName)} SET ${data} ${WHERE(cond)}`;
        let info = await db.query(sql);
        return info.affectedRows;
    }
}

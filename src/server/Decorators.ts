import { Permission } from "models/app/Permission";
import { Service } from "server/Service";
import { AnalysisSchemeLanguage } from "aws-sdk/clients/cloudsearch";

const SERVICE = "Service";
const SERVICE_LEN = SERVICE.length;

function getServiceName(className: string)
{
    if (!className.endsWith(SERVICE)) throw new Error("Invalid class name");
    return className.substr(0, className.length - SERVICE_LEN).toLowerCase();
}


export class ServiceMeta
{
    public static services: Map<string, ServiceMeta> = new Map();
    
    public ServiceClass: Class<Service>;
    public serviceName: string;
    public methods: Map<string, MethodMeta>;
    
    public constructor(serviceName: string, ServiceClass: Class<Service>)
    {
        this.ServiceClass = ServiceClass;
        this.serviceName = serviceName;
        this.methods = new Map();
    }
    
    public static of(serviceName: string, ServiceClass: Class<Service>): ServiceMeta
    {
        let service = this.services.get(serviceName);
        if (!service)
        {
            service = new ServiceMeta(serviceName, ServiceClass);
            this.services.set(serviceName, service);
        }
        return service;
    }
    
    public method(methodName: string): MethodMeta
    {
        let method = this.methods.get(methodName);
        if (!method)
        {
            method = new MethodMeta(methodName);
            this.methods.set(methodName, method);
        }
        return method;
    }
}

export class MethodMeta
{
    public methodName: string;
    public permission: Permission;
    
    public constructor(methodName: string)
    {
        this.methodName = methodName;
        this.permission =
        {
            permissions: [],
            allow: [],
            deny: [],
        };
    }
    
    public allow(patterns: string[])
    {
        if (!this.permission.allow) this.permission.allow = [];
        (this.permission.allow as string[][]).push(patterns);
    }
    
    public deny(patterns: string[])
    {
        if (!this.permission.deny) this.permission.deny = [];
        (this.permission.deny as string[][]).push(patterns);
    }
}


export function allow(...patterns: string[])
{
    return function(target: any, key: string, descriptor: TypedPropertyDescriptor<any>)
    {
        let ServiceClass = target.constructor as Class<Service>;
        let className = ServiceClass.name;
        let methodName = key;
        let serviceName = getServiceName(className);
        ServiceMeta.of(serviceName, ServiceClass).method(methodName).allow(patterns);
    }
}

export function deny(...patterns: string[])
{
    return function(target: any, key: string, descriptor: TypedPropertyDescriptor<any>)
    {
        let ServiceClass = target.constructor as Class<Service>;
        let className = ServiceClass.name;
        let methodName = key;
        let serviceName = getServiceName(className);
        ServiceMeta.of(serviceName, ServiceClass).method(methodName).deny(patterns);
    }
}


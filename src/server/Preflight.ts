import { APIGatewayEvent, Callback, Context, Handler } from "aws-lambda";

export async function preflight(event: APIGatewayEvent, context: Context)
{
    return {
        statusCode: 200,
        headers:
        {
            "Access-Control-Allow-Headers": "content-type, x-ferdy-apikey,x-ferdy-domain,x-ferdy-project,x-ferdy-accesstoken,* ",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
        },
    }
}

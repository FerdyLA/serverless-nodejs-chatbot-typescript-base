import { APIGatewayEvent, Callback, Context, Handler } from "aws-lambda";
import { Service, ServiceContext, fail } from "server/Service";
import * as SelectedServices from "server/SelectedServices";
import { ServiceMeta } from "server/Decorators";
import { OrderTelegram, OrderService } from "services/chatbots/OrderService";

export async function chatbots(event: APIGatewayEvent, awsContext: Context)
{
    if (!event.pathParameters || !event.pathParameters["chat_service"] || !event.pathParameters["bot_name"]) return fail({ message: "Missing Variables", context: `` });

    if (event.pathParameters["chat_service"]=="telegram")
    {
        try
        {
            let body = JSON.parse(event.body || "null");
            // let args =
            // Array.isArray(body) ? body :                        // ["foo", "bar", 123]
            // (body && body.args) ? body.args :           // { args: ["foo", "bar", 123] }
            // (body) ? [body] : 
            // OrderTelegram.isInputUpdate(body) ? [body] : 
            // [];
            if (Array.isArray(body)) throw "Cannot be array";
            else if (!isNaN(Number(body))) throw "Cannot be number";
            else if (typeof body == "string") throw "Cannot be string";
            else if (typeof body == "object")
            {
                await OrderTelegram.customBot(event.pathParameters["bot_name"],body);
            }
            else throw "Only objects is allowed";
        }
        catch (ex)
        {
            return fail({ message: "Problem in function", context: `${ex}` });
        }
    }

    return fail({ message: "Other chatbots not done yet.", context: `` });
}

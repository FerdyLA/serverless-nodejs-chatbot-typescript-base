import { APIGatewayEvent, Callback, Context, Handler } from "aws-lambda";
import { Service, ServiceContext, fail } from "server/Service";
import * as SelectedServices from "server/SelectedServices";
import { ServiceMeta } from "server/Decorators";

export async function rpc(event: APIGatewayEvent, awsContext: Context)
{
    SelectedServices; // NOTE: needed to prevent dead-code elimination
    
    try
    {
        var context = new ServiceContext(event, awsContext);
    }
    catch (ex)
    {
        return fail({ message: "Cannot start context", context: `${ex}` });
    }
    
    let meta = ServiceMeta.services.get(context.service);
    if (!meta) return fail(`Invalid service: ${context.service}`);
    
    let ServiceClass = meta.ServiceClass;
    
    //let ServiceClass = services.get(context.service);
    //if (!ServiceClass) return fail(`Invalid service: ${context.service}`);
    
    return Service.run(ServiceClass, context);
}

import { APIGatewayEvent, Callback, Context, Handler } from "aws-lambda";
import { Api } from "api/Api";
import { verify, fetchApiKeyOptions } from "services/auth/Verify";
import { ApiResult, ApiKeyOptions } from "models/api/Api";
import { User } from "models/app/User";
import { Permission } from "models/app/Permission";
import { ServiceMeta } from "server/Decorators";
import { ClientModel, Model } from "api/Client";
import { AnalysisSchemeStatus } from "aws-sdk/clients/cloudsearch";
import { inflect, camel, spaces } from "moon7/util/Inflect"
import { resolve } from "path";

export type ServiceModel<T extends Model> = Pick<ClientModel<T>, Exclude<keyof ClientModel<T>, "api" | "service">>;


interface RpcRequest
{
    service?: string;
    method?: string;
    args: any[];
}

function escapeRegex(str: string): string
{
    str = str.replace(/[.+?^${}()|[\]\\]/g, "\\$&");
    str = str.replace(/\*/g, "(.*)");
    return str;
}

export function respond(result: ApiResult)
{
    return {
        statusCode: 200,
        headers:
        {
            "Access-Control-Allow-Headers": "content-type, x-ferdy-apikey,x-ferdy-domain,x-ferdy-project,x-ferdy-accesstoken,* ",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
        },
        body: JSON.stringify(result),
    }
}

export function fail(error: any)
{
    if (error === undefined) error = { message: `Unexpected error -`, context: null };
    return respond({ value: null, error });
}

export function pass(value: any)
{
    if (value === undefined) value = null;
    return respond({ value, error: null });
}




export class ServiceContext
{
    public readonly event: APIGatewayEvent;
    public readonly awsContext: Context;
    public readonly request: RpcRequest | null;
    
    // public api: Api;
    public apiKeyOptions: ApiKeyOptions | null;
    
    public readonly service: string;
    public readonly method: string;
    public readonly args: any[];
    
    public user: User | undefined;
    
    public constructor(event: APIGatewayEvent, awsContext: Context)
    {
        this.event = event;
        this.awsContext = awsContext;
        this.request = JSON.parse(event.body || "null");
        this.user = undefined;
        this.apiKeyOptions = null;
        
        // this.api = new Api("caviot", "", "sys/9SqweUQ50G2V29WRAjjCfBTU");
        
        this.service =
            (this.event.pathParameters && this.event.pathParameters["service"])
            || (this.request && this.request.service)
            || raise("No service defined");
            
        this.method =
            (this.event.pathParameters && this.event.pathParameters["method"])
            || (this.request && this.request.method)
            || raise("No method defined");
            
        this.args =
            Array.isArray(this.request) ? this.request :                        // ["foo", "bar", 123]
            (this.request && this.request.args) ? this.request.args :           // { args: ["foo", "bar", 123] }
            (this.request) ? [this.request] : 
            [];
    }
    
    public async initialize()
    {
        let apiKey = this.apiKey;
        if (apiKey) this.apiKeyOptions = await fetchApiKeyOptions(apiKey);

        if (this.accessToken)
        {
            try
            {
                let { user } = await verify(this.project, this.domain, this.accessToken);
                this.user = user;
                console.log(`Token is valid: Hello ${user.key}`);

                // let apiKey = this.apiKey;
                // if (apiKey) this.apiKeyOptions = await fetchApiKeyOptions(apiKey);
            }
            catch (ex)
            {
                this.user = undefined;
                console.log(`Token is invalid`);
                // throw new Error("Token is invalid");
            }
        }
        else
        {
            console.log(`Token is missing`);
            // throw new Error("Token is missing");
        }
    }
    
    public *permissions(): IterableIterator<string>
    {
        if (this.apiKeyOptions)
        {
            if (this.user)
            {
                for (let role of this.apiKeyOptions.permissions.baseMemberPerms)
                    yield role;
            }
            else
            {
                for (let role of this.apiKeyOptions.permissions.baseApiPerms)
                yield role;
            }
        }
        
            
        if (this.user)
        {
            for (let role of this.user.roles)
                for (let perm of role.permission)
                    yield perm;

            if (this.user.members)
            {
                // yield "inUser";
                for (let member of this.user.members)
                {
                    // yield "inMember";
                    if (member.permission)
                    {
                        // yield "inMemberPerms";
                        for (let perm of member.permission)
                            yield perm;
                    }
                    for (let role of member.roles) {
                        if (role.permission)
                        {
                            // yield "inMemberRolePerms";
                            for (let perm of role.permission)
                                yield perm;
                        }
                    }
                }
            }
        }
    }
    
    public get isLoggedIn(): boolean
    {
        return !!this.user;
    }
    
    public get project(): string
    {
        return this.event.headers["x-ferdy-project"] || raise("No project defined");
    }
    
    public get domain(): string
    {
        return this.event.headers["x-ferdy-domain"] || "";
    }
    
    public get apiKey(): string
    {
        return this.event.headers["x-ferdy-apikey"] || raise("No API Key defined");
    }
    
    public get accessToken(): string | undefined
    {
        return this.event.headers["x-ferdy-accesstoken"];
    }
    
    public listAllowedMethods(): string[]
    {
        let result = [];
        
        for (let service of ServiceMeta.services.values())
        {
            for (let method of service.methods.values())
            {
                if (this.canCallMethod(method.permission))
                {
                    result.push(`${service.serviceName}.${method.methodName}`);
                }
            }
        }
        
        return result;
    }
    
    public listFunctionPermissions(): object
    {
        let permissions: any = {};
        
        for (let service of ServiceMeta.services.values())
        {
            if (service.serviceName.includes("test")) continue;
            for (let method of service.methods.values())
            {
                if (method.methodName.includes("test")) continue;
                let allowList = Array.isArray(method.permission.allow) ? method.permission.allow : typeof method.permission.allow === "string" ? [method.permission.allow] : [];
                for (let rules of allowList)
                {
                    let allowRules = Array.isArray(rules) ? rules : typeof rules === "string" ? [rules] : [];
                    for (let rule of allowRules)
                    {
                        if (rule.includes("function"))
                        {
                            if (Object.keys(permissions).indexOf(`${service.serviceName}.${method.methodName}`)==-1)
                                permissions[`${service.serviceName}.${method.methodName}`] = {
                                    service: capitalizeFirstLetter(service.serviceName),
                                    method: inflect(method.methodName, camel, spaces.mixed),
                                    // name: capitalizeFirstLetter(service.serviceName)+": "+capitalizeFirstLetter(method.methodName.replace(/([a-z])([A-Z])/g, '$1 $2').replace(/([A-Z])([A-Z])/g, '$1 $2')),
                                    permission: [rule],
                                };
                            else
                                (permissions[`${service.serviceName}.${method.methodName}`] as any).permission.push(rule);
                        }
                    }
                }
            }
        }
        
        return permissions;
    }
    
    public canCallMethod(permission: Permission): boolean
    {
        let context: ServiceContext = this;
        let { permissions, allow, deny } = permission;
        
        let denyList = Array.isArray(deny) ? deny : typeof deny === "string" ? [deny] : [];
        let allowList = Array.isArray(allow) ? allow : typeof allow === "string" ? [allow] : [];
        
        // check if any of the deny patterns reject your roles
        for (let rules of denyList)
        {
            let result: boolean[] = [];
            let denyRules = Array.isArray(rules) ? rules : typeof rules === "string" ? [rules] : [];
            for (let rule of denyRules)
            {
                if (rule==="all") return false;
                result.push(Permission.matchAny(rule, context.permissions()));
            }
            if (result.indexOf(false)===-1) return false;
            // throw new Error(JSON.stringify(permission));
        }
        
        // deny without an allow. since there wasn't a rejection, then accept
        if (denyList.length > 0 && allowList.length == 0)
        {
            return true;
        }
        
        // check if any of the allow patterns accept your roles
        for (let rules of allowList)
        {
            let result: boolean[] = [];
            let allowRules = Array.isArray(rules) ? rules : typeof rules === "string" ? [rules] : [];
            for (let rule of allowRules)
            {
                if (rule==="all") return true;
                result.push(Permission.matchAny(rule, context.permissions()));
            }
            if (result.indexOf(false)===-1) return true;
        }
        
        // reject any other scenarios
        return false;
    }
}

export class Service
{
    public context: ServiceContext;
    
    public constructor(context: ServiceContext)
    {
        this.context = context;
    }
    
    public static async run<T extends Service>(ServiceClass: Class<T>, context: ServiceContext)
    {
        try
        {
            let service = new ServiceClass(context);
            await service.context.initialize();
            
            let serviceMeta = ServiceMeta.services.get(context.service);
            if (!serviceMeta) throw new Error("Invalid service");
            
            let methodMeta = serviceMeta.methods.get(context.method);
            if (!methodMeta) throw new Error("Invalid method");
            
            if (!context.canCallMethod(methodMeta.permission)) throw new Error("Access denied: "+JSON.stringify(context.permissions()));
            
            let fn: Function | undefined = (service as any)[context.method];
            
            if (fn && fn instanceof Function)
            {
                try
                {
                    let value = await fn.apply(service, context.args);
                    return pass(value);
                }
                catch (ex)
                {
                    return fail({ message: "Call error", context: `${ex}` });
                }
            }
            else
            {
                return fail({ message: "Not a method", context: context.method });
            }
        }
        catch (ex)
        {
            return fail({ message: "Unexpected error --", context: `${ex}` });
        }
    }
    
    public static rpc<T extends Service>(ServiceClass: Class<T>)
    {
        return async function(event: APIGatewayEvent, awsContext: Context)
        {
            try
            {
                let context = new ServiceContext(event, awsContext)
                return await Service.run(ServiceClass, context);
            }
            catch (ex)
            {
                fail({ message: "Unable to run service", context: `${ex}` });
            }
        }
    }
}

function capitalizeFirstLetter(string: string): string
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

import { Client } from "api/Client";
import { User } from "models/app/User";

export class UserClient extends Client
{
    /** Return true if deleted. Require only id */
    public async view(id: string ): Promise<User>
	{
        return await this.api.call("component.delete",id);
    } 
    
}

import { Api } from "api/Api";

export class Client
{
    protected readonly api: Api;
    protected readonly service: string;
    
    public constructor(api: Api, service: string)
    {
        this.api = api;
        this.service = service;
    }
}

export interface Model
{
    key: string;
}

export class ClientModel<T extends Model> extends Client
{
    public constructor(api: Api, service: string)
    {
        super(api, service);
    }
    
    public async load(key: string): Promise<T | undefined>
    {
        return this.api.call(`${this.service}.load`, key);
    }
    
    public async save(key: string, value: T): Promise<boolean>
    {
        return this.api.call(`${this.service}.save`, key, value);
    }
    
    public async remove(key: string): Promise<boolean>
    {
        return this.api.call(`${this.service}.remove`, key);
    }
    
    public async exist(key: string): Promise<boolean>
    {
        return this.api.call(`${this.service}.exist`, key);
    }
    
    public async keys(): Promise<string[]>
    {
        return this.api.call(`${this.service}.keys`);
    }
    
    public async list(): Promise<T[]>
    {
        return this.api.call(`${this.service}.list`);
    }
}

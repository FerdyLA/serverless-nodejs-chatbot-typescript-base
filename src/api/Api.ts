import axios, { AxiosInstance } from "axios";
import { ApiResult, isSuccess } from "models/api/Api";

import { UserClient } from "api/services/UserClient";


import { runInThisContext } from "vm";
// import { StorageClient } from "api/services/PageClient";

const URLs =
{
    "prod":"https://bepynnktc3.execute-api.ap-southeast-1.amazonaws.com",
};

const stage: keyof typeof URLs = "prod";

export interface ApiHeaders
{
    "x-ferdy-project": string;
    "x-ferdy-domain": string;
    "x-ferdy-apikey": string;
    "x-ferdy-accesstoken"?: string;
}


export class Api
{
    public readonly ajax: AxiosInstance;
    
    public readonly project: string;
    public domain: string;
    public readonly apiKey: string;
    public readonly stage: string;
    public readonly headers: ApiHeaders;
    
    public readonly user: UserClient;
    
    // public readonly storage: StorageClient;
    public readonly url: string;
    
    constructor(project: string, domain: string, apiKey: string)
    {
        this.url = URLs[stage];
        
        this.project = project;
        this.apiKey = apiKey;
        this.stage = stage;
        this.domain = domain;
        
        let baseURL = `${this.url}/${stage}`;
        
        this.headers =
        {
            "x-ferdy-project": project,
            "x-ferdy-domain": domain,
            "x-ferdy-apikey": apiKey,
        };
        
        this.ajax = axios.create(
        {
            baseURL: baseURL,
            headers: this.headers,
        });
        
        this.user = new UserClient(this, "user");
    }
    
    public setDomain(domain: string)
    {
        this.domain = domain;
        this.headers["x-ferdy-domain"] = domain;
    }
    
    public async call<T = any>(method: string, ...args: any[]): Promise<T>
    {
        let [service, fn] = method.split(".");
        
        try
        {
            //let result = await this.ajax.post(url, { method: fn, args });
            let result = await this.ajax.request(
            {
                url: `/rpc/${service}/${fn}`,
                method: "post",
                data: args,
                headers: this.headers,
            });
            
            let data: ApiResult = result.data;
            
            if (isSuccess(data))
            {
                return data.value;
            }
            else
            {
                console.log("RPC LOGIC ERROR");
                throw data.error;
            }
        }
        catch (ex)
        {
            console.log("RPC CALL ERROR:");
            throw ex;
        }
    }
}

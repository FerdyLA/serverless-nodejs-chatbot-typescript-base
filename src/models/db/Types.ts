export interface DbType { __DBTYPE__: string };

export interface Id { __DBTYPE__: "Id" };
export interface Pk { __DBTYPE__: "Pk" };
export interface Json { __DBTYPE__: "Json" };

export type Data<T> =
{
    [P in keyof T]
        : T[P] extends Pk ? string
        : T[P] extends Id ? string
        : T[P] extends Json ? any
        : T[P] extends (Id | null) ? (string | null)
        : T[P]
};

export type Column<T> = Extract<keyof T, string>;

export type PrimaryKeys<T> = { [P in keyof T]: T[P] extends Pk ? P : never }[Column<T>];
export type NonPrimaryKeys<T> = { [P in keyof T]: T[P] extends Pk ? never : P }[Column<T>];

export type PrimaryKeysData<T> = Data<Pick<T, Exclude<PrimaryKeys<T>, T>>>;
export type NonPrimaryKeysData<T> = Data<Pick<T, Exclude<NonPrimaryKeys<T>, T>>>;

export type DataOption<T, V> = { [P in keyof Data<T>]: V };
export type PrimaryKeysOption<T, V> = { [P in keyof PrimaryKeysData<T>]: V };
export type NonPrimaryKeysOption<T, V> = { [P in keyof NonPrimaryKeysData<T>]: V };

export type DataOrder<T> = { column: Column<T>, asc: boolean };

/**
 * This file is generated by running: npm run schema:pull
 * That script should be run whenever changes are made to the database.
 * Do not directly modify this file.
 */
import { Pk, Id } from "models/db/Types";

export interface Schema
{
    "yois.Api": yois.Api;
    "yois.Domain": yois.Domain;
    "yois.Job": yois.Job;
    "yois.Member": yois.Member;
    "yois.Order": yois.Order;
    "yois.Registry": yois.Registry;
    "yois.Restaurant": yois.Restaurant;
    "yois.RestaurantCategory": yois.RestaurantCategory;
    "yois.RestaurantItem": yois.RestaurantItem;
    "yois.Role": yois.Role;
    "yois.TransportLoad": yois.TransportLoad;
    "yois.TransportMode": yois.TransportMode;
    "yois.User": yois.User;
    "yois.UserRole": yois.UserRole;
    "yois.UserSession": yois.UserSession;
    "yois.UserTelegram": yois.UserTelegram;
    "yois.telegram_agent": yois.telegram_agent;
    "yois.telegram_bot": yois.telegram_bot;
    "yois.telegram_customer": yois.telegram_customer;
    "yois.telegram_customer_action": yois.telegram_customer_action;
    "yois.telegram_invoice": yois.telegram_invoice;
    "yois.telegram_product": yois.telegram_product;
    "yois.telegram_settings": yois.telegram_settings;
    "yois.telegram_user": yois.telegram_user;
    "yois.telegram_user_action": yois.telegram_user_action;
}

export const PK =
{
    "yois.Api": { id: "" },
    "yois.Domain": { id: "" },
    "yois.Job": { id: "" },
    "yois.Member": { id: "" },
    "yois.Order": { id: "" },
    "yois.Registry": { id: "" },
    "yois.Restaurant": { id: "" },
    "yois.RestaurantCategory": { id: "" },
    "yois.RestaurantItem": { id: "" },
    "yois.Role": { id: "" },
    "yois.TransportLoad": { id: "" },
    "yois.TransportMode": { id: "" },
    "yois.User": { id: "" },
    "yois.UserRole": { id: "" },
    "yois.UserSession": { id: "" },
    "yois.UserTelegram": { id: "" },
    "yois.telegram_agent": { id: "" },
    "yois.telegram_bot": { id: "" },
    "yois.telegram_customer": { id: "" },
    "yois.telegram_customer_action": { id: "" },
    "yois.telegram_invoice": { id: "" },
    "yois.telegram_product": { id: "" },
    "yois.telegram_settings": { id: "" },
    "yois.telegram_user": { id: "" },
    "yois.telegram_user_action": { id: "" },
}

export namespace yois
{
    export interface Api
    {
        id: Pk;
        key: string;
        name: string;
        options: string;
        callback_url: string;
    }
    
    export const Api = "yois.Api";

    export interface Domain
    {
        id: Pk;
        name: string;
        key: string;
        desc: string | null;
    }
    
    export const Domain = "yois.Domain";

    export interface Job
    {
        id: Pk;
        creator_telegram: int | null;
        creator: int | null;
        creator_api_key_id: int | null;
        accepter_telegram: int | null;
        accepter: int | null;
        from_name: string;
        from_postal: string;
        from_unit: string;
        from_contact: string;
        receipient_name: string;
        to_postal: string;
        to_unit: string;
        to_contact: string;
        pay: float;
        status: int;
        created: Date;
        completed: Date | null;
        paid_or_cod: int | null;
        cod_cost: float | null;
        driver_cost: float;
    }
    
    export const Job = "yois.Job";

    export interface Member
    {
        id: Pk;
        userId: int;
        domainId: int;
        roleId: int;
        permission: string;
    }
    
    export const Member = "yois.Member";

    export interface Order
    {
        id: Pk;
        telegram_customer: int;
        restaurant_id: int;
        added: Date;
        orders: string;
        destination: string | null;
        destination_unit: string | null;
        customer_contact: string;
        status: int;
        ready: int;
        delivery_fee: float | null;
        delivery_distance: float | null;
        paid: float | null;
        telegram_invoice_id: int | null;
        delivery_job_id: int | null;
        bot_username: string;
    }
    
    export const Order = "yois.Order";

    export interface Registry
    {
        id: Pk;
        key: string;
        data: string;
    }
    
    export const Registry = "yois.Registry";

    export interface Restaurant
    {
        id: Pk;
        gst: int;
        open: int;
        creator_telegram: int;
        custom_bot_user: string | null;
        custom_bot_update_id: int | null;
        custom_bot_update_time: Date | null;
        deleted: int;
        mode: int;
        custom_only: int;
        max_radius: float;
        free_delivery_above: float;
        images: string | null;
        paynow_account: string | null;
    }
    
    export const Restaurant = "yois.Restaurant";

    export interface RestaurantCategory
    {
        id: Pk;
        parent: int;
        name: string;
        restaurant_id: int;
    }
    
    export const RestaurantCategory = "yois.RestaurantCategory";

    export interface RestaurantItem
    {
        id: Pk;
        name: string;
        price: float;
        availability: int;
        restaurant_id: int;
        category_id: int;
        add_ons: string | null;
    }
    
    export const RestaurantItem = "yois.RestaurantItem";

    export interface Role
    {
        id: Pk;
        key: string;
        name: string;
        data: string | null;
        permission: string | null;
        domainId: int;
    }
    
    export const Role = "yois.Role";

    export interface TransportLoad
    {
        id: Pk;
        name: string;
        max_weight: float;
        max_size: float;
        group: int;
        rate: float;
    }
    
    export const TransportLoad = "yois.TransportLoad";

    export interface TransportMode
    {
        id: Pk;
        title: string;
        description: string;
        mode: string;
        max_distance: int;
        max_load_group: int;
    }
    
    export const TransportMode = "yois.TransportMode";

    export interface User
    {
        id: Pk;
        key: string;
        email: string;
        mobile: string | null;
        pwdHash: string;
        publicData: string;
        privateData: string;
    }
    
    export const User = "yois.User";

    export interface UserRole
    {
        id: Pk;
        roleId: int;
        userId: int;
    }
    
    export const UserRole = "yois.UserRole";

    export interface UserSession
    {
        id: Pk;
        userId: int;
        refreshToken: string;
        timestamp: Date;
    }
    
    export const UserSession = "yois.UserSession";

    export interface UserTelegram
    {
        id: Pk;
    }
    
    export const UserTelegram = "yois.UserTelegram";

    export interface telegram_agent
    {
        id: Pk;
        reward_credit: int;
        referree_bonus_credit: int;
    }
    
    export const telegram_agent = "yois.telegram_agent";

    export interface telegram_bot
    {
        id: Pk;
        bot_name: string;
        bot_token: string;
        bot_payment_token: string;
        bot_outlets: string | null;
        stripe_key: string | null;
    }
    
    export const telegram_bot = "yois.telegram_bot";

    export interface telegram_customer
    {
        id: Pk;
        first_name: string;
        username: string;
        added: int;
    }
    
    export const telegram_customer = "yois.telegram_customer";

    export interface telegram_customer_action
    {
        id: Pk;
        user_id: int;
        restaurant_id: int;
        timestamp: Date;
        action_type: string;
        action_cache: string;
        executed: int;
        bot_user: string;
        message_id: string | null;
    }
    
    export const telegram_customer_action = "yois.telegram_customer_action";

    export interface telegram_invoice
    {
        id: Pk;
        telegram_user_id: int;
        product_id: int;
        order_id: int;
        amount: float;
        created: Date;
        provider: string;
        shipping_option_id: int | null;
        order_info: string | null;
        telegram_payment_id: string | null;
        provider_payment_id: string | null;
        paid: Date | null;
    }
    
    export const telegram_invoice = "yois.telegram_invoice";

    export interface telegram_product
    {
        id: Pk;
        key: string | null;
        name: string;
        description: string | null;
        cost: float;
        reward_type: string;
        reward_amount: int | null;
        is_active: int;
    }
    
    export const telegram_product = "yois.telegram_product";

    export interface telegram_settings
    {
        id: Pk;
        last_update_id: int | null;
        last_updated_time: Date | null;
        onemap_token: string | null;
        onemap_token_created: Date | null;
        yoiso_last_id: int | null;
        yoiso_last_time: Date | null;
        referral_reward: float;
        signup_initial_credits: float;
        driver_commision: float;
        merchant_commission: float;
    }
    
    export const telegram_settings = "yois.telegram_settings";

    export interface telegram_user
    {
        id: Pk;
        user_id: int | null;
        first_name: string;
        username: string | null;
        added: int;
        private_chat_id: int;
        user_type: string | null;
        user_credits: float;
        bot_username: string;
        store_name: string | null;
        postal: string | null;
        unit: string | null;
        lat: float | null;
        lon: float | null;
        last_updated: Date | null;
        transport_mode: int | null;
        phone_number: string | null;
        joined: Date;
        last_alert: Date | null;
        referred_by: int | null;
    }
    
    export const telegram_user = "yois.telegram_user";

    export interface telegram_user_action
    {
        id: Pk;
        user_id: int;
        timestamp: int;
        state: string;
        cache: string;
        executed: int | null;
        completed: int | null;
        bot_user: string;
    }
    
    export const telegram_user_action = "yois.telegram_user_action";
}

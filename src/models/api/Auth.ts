import { User } from "models/app/User";
import { Permission } from "models/app/Permission";
import * as schema from "models/db/Schema";
import { Data } from "models/db/Types";

export interface LoginResult
{
    user: User;
    accessToken: string;
    refreshToken: string;
}

export interface VerifyResult
{
    user: User;
}


/** Contents of an AccessToken */
export interface TokenData
{
    iss?: any;      // issuer
    sub?: any;      // subject
    aud?: any;      // audience
    exp?: int;      // expiration: after this date, token cannot be used
    nbf?: int;      // not before: before this date, token cannot be used
    iat?: int;      // issued-at: when token is issued
    jti?: any;
}

/** Contents of the access token */
export interface AccessTokenData extends TokenData
{
    project: string;
    domain: string;
    user: User;
}

/** Contents of the refresh token */
export interface RefreshTokenData extends TokenData
{
    project: string;
    domain: string;
    user: User;
}

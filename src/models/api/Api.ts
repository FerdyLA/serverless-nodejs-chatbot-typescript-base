import { Permission } from "../app/Permission";

/** These are sent to server when you do an RPC call */
export interface ApiCall
{
    token: string | null;       // for authentication
    command: string;            // Service.method
    args: Array<any>;           // the arguments of the method
}


/** An API result is either a success or a failure */
export type ApiResult<V = any, E = any> = ApiSuccess<V> | ApiFailure<E>;

export interface ApiSuccess<V = any>
{
    value: V;
    error: null;
}

export interface ApiFailure<E = any>
{
    value: null;
    error: E;
}

interface MomentInputObject
{
    years?: number;
    year?: number;
    y?: number;

    months?: number;
    month?: number;
    M?: number;

    days?: number;
    day?: number;
    d?: number;

    dates?: number;
    date?: number;
    D?: number;

    hours?: number;
    hour?: number;
    h?: number;

    minutes?: number;
    minute?: number;
    m?: number;

    seconds?: number;
    second?: number;
    s?: number;

    milliseconds?: number;
    millisecond?: number;
    ms?: number;
}

interface DurationInputObject extends MomentInputObject
{
    quarters?: number;
    quarter?: number;
    Q?: number;

    weeks?: number;
    week?: number;
    w?: number;
}

export type Duration = DurationInputObject | number | string | null;

export interface ApiKeyOptions
{
    tokens:
    {
        accessDuration: Duration,
        refreshDuration: Duration,
    },
    permissions:
    {
        baseApiPerms: string[],             // what roles to give for anyone using this ApiKey
        baseMemberPerms: string[],          // what additional roles to give for logged in users of this ApiKey
    },
}

export function isSuccess(result: ApiResult): result is ApiSuccess
{
    return result.error == null;
}

import { Permission } from "models/app/Permission";
import { Table, Data, schema, db } from "db/Database";
import { UserClient } from "api/services/UserClient";

export interface User
{
    id: string;
    key: string;
    name: string;
    pic?: string;
    members?: Member[];
    data: any;
    roles: Role[];
}

export interface Member
{
    id: string;
    domain?: Domain;
    permission: string[];
    roles: Role[];
}

export interface Role
{
    id: string;
    key: string;
    name: string;
    data: string[];
    permission: string[];
    domainId: string;
}

export interface Domain
{
    id: string;
    key: string;
    name: string;
    desc: string | null | undefined;
}

export namespace User
{
    export function stringify(user: User): string
    {
        return user.data.name ? user.data.name : user.key.split("@")[0];
    }
}

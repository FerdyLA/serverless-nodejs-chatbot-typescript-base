/*
    Permissions
    
    permission:     is the permission given to a user/group/api and may contain PATTERNS
    resource:       a key, in the form of a path, and is a string WITHOUT patterns
    
    patterns:       any regex pattern is a valid pattern. some extra non-regex patterns available.
    
    e.g.
        match("role/(office|building)/staff", "role/office/staff")              matches (office or building) staff
        match("role/([a-f]+)/staff", "role/office/staff")                       matches (anything with characters a-f only) staff
        match("role/(b.*)/staff", "role/office/staff")                          matches (anything starting with b) staff
    
    non-regex patterns:
        
        % and %%  are extra non-regex patterns which converts to regex patterns
        %       means match anything except /                                   used for matching that folder ONLY
        %%      means match anything including /                                used for matching that folder and ALL SUB-FOLDERS
    
    e.g.
        match("role/office/%", "role/office/staff")                             true
        match("role/office/%", "role/office/staff/blah")                        false
        match("role/office/%%", "role/office/staff/blah")                       true
*/


export const enum PermissionType
{
    User = "user",
    Group = "group",
    Api = "api",
}

export interface Permission
{
    permissions: string[];            // concrete resource/registry keys
    allow: string | string[] | string[][] | null;       // pattern
    deny: string | string[] | string[][] | null;        // pattern
}



export namespace Permission
{
    function createPattern(pattern: string): RegExp
    {
        let rxPattern =
            "^"
            + pattern
            .replace(/%%/g, "(.*)")
            .replace(/%/g, "([^/]+)")
            + "$";
        
        let rx = new RegExp(rxPattern);
        //console.log(pattern, rx);
        return rx;
    }
    
    /** see if the pattern matches the resource */
    export function match(rule: string, pattern: string): boolean
    {
        let rx = createPattern(pattern);
        return rx.test(rule);
    }
    
    /** returns true if the pattern matches at least 1 resource */
    export function matchAny(rule: string, patterns: Iterable<string>): boolean
    {
        for (let pattern of patterns)
        {
            if (match(rule,pattern))
                return true;
        }
        return false;
    }
    
    /** returns true if the pattern matchs all resource */
    export function matchAll(rule: string, patterns: Iterable<string>): boolean
    {
        for (let pattern of patterns)
        {
            if (!match(rule,pattern))
                return false;
        }
        return true;
    }
    
}

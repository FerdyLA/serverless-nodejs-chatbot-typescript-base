import { User } from "models/app/User";

export interface SignUp
{
	email: string;
    password: string;
	data: any;
}
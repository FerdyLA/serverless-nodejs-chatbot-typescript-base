const webpack = require("webpack");
const path = require("path");
const fs = require("fs");

const isProduction = process.env.NODE_ENV === "production";
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');


function excludeExternals(config)
{
    // allows nodejs core modules to be loaded
    let externals = {};
    
    fs.readdirSync("node_modules")
        //.filter(x => [".bin"].indexOf(x) === -1)
        .filter(x => !x.startsWith("."))
        .forEach(mod => externals[mod] = "commonjs " + mod);
        
    return Object.assign(config, { externals });
}


const plugins =
{
    provide: new webpack.ProvidePlugin(
    {
        //$: "jquery",
        //jQuery: "jquery",
        //jquery: "jquery",
        //"window.jQuery": "jquery",
        _: "lodash",
    }),
    
    ignore: new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        
    define: new webpack.DefinePlugin(
    {
        __STAGE__: JSON.stringify(isProduction === "production" ? "production" : "development"),
        __TYPE__: JSON.stringify("server"),
        __TARGET__: JSON.stringify("node"),
        __PRODUCTION__: JSON.stringify(isProduction === "production"),
        __DEVELOPMENT__: JSON.stringify(isProduction !== "production"),
        __BUILD__: JSON.stringify(new Date().toISOString()),
    }),
    
    uglify: new UglifyJsPlugin(
    {
        extractComments: true,
        parallel: true,
        uglifyOptions:
        {
            mangle: false
        }
    }),
};


module.exports = excludeExternals(
{
    target: "async-node",
    
    entry:
    {
        server:
        [
            "moon7/boot/Boot",
            "moon7/boot/Server",
        ],
    },
    
    output:
    {
        path: path.resolve("build"),
        filename: "[name].js",
        library: "[name]_lib",
        libraryTarget: "commonjs2",
    },
    
    devtool: isProduction ? "source-map" : "eval",
    
    resolve:
    {
        alias:
        {
            "react$": "react/umd/react.production.min.js",
            "react-dom$": "react-dom/umd/react-dom.production.min.js",
        },
        
        extensions:
        [
            ".ts",
            ".tsx",
            ".js",
            ".jsx",
        ],
        
        modules:
        [
            "src",
            "libs",
            //"../18r-app-common/src",
            "node_modules",
        ],
    },
    
    resolveLoader:
    {
    },
    
    module:
    {
        rules:
        [
            // javascript
            {
                test: /\.jsx?$/,
                loader: "ts-loader",
                exclude: /node_modules/,
            },
            
            // typescript
            {
                test: /\.tsx?$/,
                loader: "ts-loader",
                exclude: /node_modules/,
            },
        ],
    },
    
    plugins:
    [
        plugins.define,
        //plugins.uglify,
    ],
});

# Chatbot API using Serverless NodeJS Typescript RDS MySQL

## Setup
Currently this script only supports Telegram. More chat apps coming soon.

### Edit these files:
1. /package.json
2. /schema-pull.ts
3. /schema-pull-pk.ts
4. /schema-pull-nopk.ts
5. /env.yml
6. /src/server/ChatBots.ts (line 27 to your ChatBot Class)

### Import MySQL Structure to DB:
Import setup.sql into your RDS instance

### RUN

```sh
# install dependencies
npm install

# pull schema model
npm run pull:schema

# TODO: deploy server to AWS Lambda
npm run deploy

# edit /package.json with the url you get after deploy. register with telegram the endpoint with that url, after adding {url}/{stage}/rpc/{function_name}/{method_name}
npm run register-endpoint
```

### Create your own _Service.ts
Create your own {Something}Service.ts and add a {Something}TelegramBot class that extends TelegramBot class. Start coding into your service, calling {Something}TelegramBot.method when a certain type of message is received.

### Project folder:
- /src/api : Do not touch this.
- src/db: Do not touch this. npm run pull:schema is supposed to connect to MYSQL and get the entire model
- src/server: Only edit /src/server/SelectedServices.ts. Other files should not be modified unless you know what you are doing. All Services linked to SelectedServices.ts will automatically be added to Serverless as EndPoint.
- src/service: This is where all the services and its logic goes. Remember to add each individual {NAME}Service.ts into SelectedServices.ts
- src/models: Models are used to create typescript interface (object models) to allow easier coding and error checking on data types and structures

### Notes:
* Services and models like Auth, AuthService is not applicable to any Chatbot services, but can be used to build your own login authentication on your website that uses this serverless API.
* allow/deny tag above ALL Service Methods are REQUIRED. @allow("all") is used in chatbot api as chatbot do not use the same authentication as our required RPC Headers.
* This is not completed. It is still very rough, and I know it could be better. If you have a better way to code for this, feel free to share.

## Code
Contents within `src/models/db` should map all the database tables as specified
in the `database` repository. `VARCHAR` should map to `string`,
and so on. Nullable fields (hollow/white diamonds in the diagram) should add
`| null`. For instance, nullable string type should be `string | null`.


## Send these commands to @BotFather via /mybots > {@your_bot_name} > Edit Bot > Edit Commands
Commands:
help - Hmmm. What special powers do I have?
status - Am I still alive? How many chats I have?
start - Just to begin talking to me.
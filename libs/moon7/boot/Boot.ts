declare var window: any;
declare var global: any;

import "reflect-metadata";
import * as g from "moon7/core/Global";


void async function main()
{
    let scope: any = typeof window !== "undefined" ? window : global;
    let module: any = g;

    for (let x of Object.keys(module))
    {
        //console.log("GLOBALS:", x);
        scope[x] = module[x];
    }
}();

// UpperCamelCase
// lowerCamelCase
// MixedCamelCase
// UPPER-KEBAB-CASE
// lower-kebab-case
// Mixed-Kebab-Case
// UPPER_SNAKE_CASE
// lower_snake_case
// Mixed-Snake-Case

let camelRx = /([A-Z]?[a-z]+(?=[A-Z]))|([A-Z]+(?=[A-Z][a-z]))|([A-Za-z]+)/g;


export function upper(str: string): string
{
    return str.toUpperCase();
}

export function lower(str: string): string
{
    return str.toLowerCase();
}

export function lowerFirst(str: string): string
{
    return str[0].toLowerCase() + str.substr(1);
}

export function upperFirst(str: string): string
{
    return str[0].toUpperCase() + str.substr(1);
}

export function mixed(str: string): string
{
    return str[0].toUpperCase() + str.substr(1).toLowerCase();
}


export let camel =
{
    split(str: string): string[]
    {
        return Array.from(str.match(camelRx) || []);
    },
    
    upper(arr: string[]): string
    {
        return arr.map(mixed).join("");
    },
    
    lower(arr: string[]): string
    {
        return lowerFirst(arr.map(mixed).join(""));
    },
};

export let kebab =
{
    split(str: string): string[]
    {
        return str.split("-");
    },
    
    upper(arr: string[]): string
    {
        return arr.map(upper).join("-");
    },
    
    lower(arr: string[]): string
    {
        return arr.map(lower).join("-");
    },
    
    mixed(arr: string[]): string
    {
        return arr.map(mixed).join("-");
    },
};

export let snake =
{
    split(str: string): string[]
    {
        return str.split("_");
    },
    
    upper(arr: string[]): string
    {
        return arr.map(upper).join("_");
    },
    
    lower(arr: string[]): string
    {
        return arr.map(lower).join("_");
    },
    
    mixed(arr: string[]): string
    {
        return arr.map(mixed).join("_");
    },
};

export let spaces =
{
    split(str: string): string[]
    {
        return str.split(" ");
    },
    
    upper(arr: string[]): string
    {
        return arr.map(upper).join(" ");
    },
    
    lower(arr: string[]): string
    {
        return arr.map(lower).join(" ");
    },
    
    mixed(arr: string[]): string
    {
        return arr.map(mixed).join(" ");
    },
};

export interface Splitter
{
    split(str: string): string[];
}

export type Joiner = (arr: string[]) => string;


// inflect("FooBARBaz", camel, kebab.lower); // foo-bar-baz
export function inflect(str: string, from: Splitter, to: Joiner): string
{
    return to(from.split(str));
}


/*
console.log("Split Camel");
console.log(camel.split("hello"));
console.log(camel.split("helloFooWorld"));
console.log(camel.split("Hello"));
console.log(camel.split("HelloFooWorld"));
console.log(camel.split("HelloFOOWorld"));
console.log(camel.split("HTMLEntity"));

console.log("Camel to Kebab");
let from = camel;
let to = kebab.lower;
console.log(inflect("hello", from, to));
console.log(inflect("helloFooWorld", from, to));
console.log(inflect("Hello", from, to));
console.log(inflect("HelloFooWorld", from, to));
console.log(inflect("HelloFOOWorld", from, to));
console.log(inflect("HTMLEntity", from, to));

console.log("Kebab to Camel");
from = kebab;
to = camel.lower;
console.log(inflect("hello", from, to));
console.log(inflect("hello-Foo-World", from, to));
console.log(inflect("Hello", from, to));
console.log(inflect("Hello-Foo-World", from, to));
console.log(inflect("Hello-FOO-World", from, to));
console.log(inflect("HTML-Entity", from, to));
*/

let charmap: Struct<string> =
{
    '<3': 'heart', ':)': 'smiley', ':(': 'frown',
    
    '€': 'euro', '₢': 'cruzeiro', '₣': 'french franc', '£': 'pound',
    '₤': 'lira', '₥': 'mill', '₦': 'naira', '₧': 'peseta', '₨': 'rupee',
    '₩': 'won', '₪': 'new shequel', '₫': 'dong', '₭': 'kip', '₮': 'tugrik',
    '₯': 'drachma', '₰': 'penny', '₱': 'peso', '₲': 'guarani', '₳': 'austral',
    '₴': 'hryvnia', '₵': 'cedi', '¢': 'cent', '¥': 'yen', '元': 'yuan',
    '円': 'yen', '﷼': 'rial', '₠': 'ecu', '¤': 'currency', '฿': 'baht',
    "$": 'dollar', '₹': 'indian rupee',
    
    '©':'(c)', 'œ': 'oe', 'Œ': 'OE', '∑': 'sum', '®': '(r)', '†': '+',
    '“': '"', '”': '"', '‘': "'", '’': "'", '∂': 'd', 'ƒ': 'f', '™': 'tm',
    '℠': 'sm', '…': '...', '˚': 'o', 'º': 'o', 'ª': 'a', '•': '*',
    '∆': 'delta', '∞': 'infinity', '♥': 'heart', '&': 'and', '|': 'or',
    '<': 'lesser', '>': 'greater', '+': "plus",
};

function slugReplacer(m: string, m0: string): string
{
    return charmap[m0] || m0;
}

export function slug(title: string): string
{
    return title.toString().trim().toLowerCase()
        .replace(/([^\s\w]{2})/g, slugReplacer)
        .replace(/(\W)/g, slugReplacer)
        .replace(/[\s\W-]+/g, '-')
        .replace(/^-|-$/g, '')
}

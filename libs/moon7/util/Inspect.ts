
const toString = Object.prototype.toString;

/**
 * Inspects a given value to see if it matches
 * a type based on duck-typing (if it looks like a duck,
 * and it quacks like a duck, then its probably a duck).
 * 
 * let thing = { name: "bob", quack: () => console.log("quack") };
 * let Duck = { quack: Function };
 * let Cat = { meow: Function };
 * 
 * Inspect.is(thing, Duck);         // true
 * Inspect.is(thing, Cat);          // false
 */
export function is<T>(value: any, type: any, deep: boolean = true): value is T
{
    throw new Error("not implemented");
}

/**
 * Checks if `value` has the same type as `ref`.
 * Every field in `ref` must exist in `value`
 * 
 * let thing = { name: "bob", quack: () => console.log("quack") };
 * let duckThing = { quack: () => console.log("quack") };
 * let catThing = { meow: () => console.log("quack") };
 * 
 * Inspect.match(thing, Duck);         // true
 * Inspect.match(thing, Cat);          // false
 */
export function match<T>(value: any, ref: any, deep: boolean = true): value is T
{
    throw new Error("not implemented");
}

export function isRegExp(x: any): x is RegExp
{
    return toString.call(x) === "[object RegExp]";
}

export function isFunction<A, R>(x: any): x is (...a: Array<A>) => R
{
    let s: string = toString.call(x);
    if (s === "[object Function]") return true;
    if (s === "[object AsyncFunction]") return true;
    if (typeof x === "function" && s !== "[object RegExp]") return true;
    return false;
}

/** Checks if x is an ES6+ class */
export function isClass<T>(x: any): x is Class<T>
{
    return typeof x === "function" && x.toString().indexOf("class") === 0;
}

/** Checks if x is an instance of a class or plain object */
export function isObject(x: any): x is object
{
    // need to check for null because typeof null is also object
    return x !== null && typeof x === "object";
}

/** Checks if x is a plain object (not an instance of a class) */
export function isPlainObject<T>(x: any): x is Struct<T>
{
    return isObject(x) && (x as any).__proto__ === Object.prototype;
}

export function isArray<T>(x: any): x is Array<T>
{
    return Array.isArray(x) || toString.call(x) === "[object Array]";
}

export function isArrayLike<T>(x: any): x is ArrayLike<T>
{
    if (!x) return false;
    if (x instanceof Array) return true;
    if (isFunction(x)) return false;
    
    let o = Object(x);
    let length = "length" in o && o.length;
    
    if (o.nodeType === 1 && length) return true;
    if (length === 0) return true;
    if (typeof length === "number" && length > 0 && (length - 1) in o) return true;
    
    return false;
}

export function isNumber(x: any): x is number
{
    return typeof x === "number" || toString.call(x) === "[object Number]";
}

export function isInteger(x: any): x is int
{
    return Number.isInteger(x);
}

export function isString(x: any): x is string
{
    return typeof x === "string" || toString.call(x) === "[object String]";
}

export function isBoolean(x: any): x is boolean
{
    return x === true || x === false || toString.call(x) === "[object Boolean]";
}

export function isNull(x: any): x is null
{
    return x === null;
}

export function isUndefined(x: any): x is undefined
{
    return typeof x === "undefined";
}

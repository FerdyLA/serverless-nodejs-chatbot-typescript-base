import { JSON85 } from "moon7/util/Chars";
export { BASE64, BASE64_URL, JSON85 } from "moon7/util/Chars";

/**
 * Split a number into an array of digits of base-n.
 * Each digit is an integer.
 */
export function split(num: int, base: int): int[]
{
    let out: int[] = [];
    let q = Math.floor(Math.abs(num));
    let r;
    
    while (true)
    {
        r = q % base
        out.unshift(r);
        q = (q - r) / base;
        if (q == 0) break;
    }
    
    return out;
}

/**
 * Join an array of digits back into a single integer.
 */
export function join(digits: int[], base: int): int
{
    let out = 0;
    for (let i = 0; i < digits.length; ++i)
        out = (out * base) + digits[i];
    return out;
}

/**
 * Encode a number as a string to base-n with custom digit representation.
 */
export function encode(num: int, base: int, chars: string = JSON85): string
{
    return split(num, base).map(c => chars.charAt(c)).join("");
}

/**
 * Decode a base-n encoded string back into an integer.
 * The string input is case-sensitive.
 * For case-insensitive version, use decodeci.
 */
export function decode(str: string, base: int, chars: string): int
{
    let out = 0;
    for (let i = 0; i < str.length; ++i)
        out = (out * base) + chars.indexOf(str.charAt(i));
    return out;
}

/**
 * Decode a base-n encoded string back into an integer.
 * The string input is case-insensitive up to the base specified in ciBaseLimit.
 * Bases exceeding ciBaseLimit will be case-sensitive.
 */
export function decodeci(str: string, base: int, chars: string, ciBaseLimit: int = 36): int
{
    return decode(base <= ciBaseLimit ? str.toUpperCase() : str, base, chars);
}

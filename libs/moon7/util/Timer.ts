import { sleep } from "moon7/core/Future";

export class Timer
{
    public callback: () => void;
    
    /** Initial delay duration*/
    public delay: int;
    
    /** Subsequent interval duration */
    public interval: int;
    public isRunning: boolean = false;
    
    /** Yes, there's a difference with isRunning */
    public hasStopped: boolean = false;
    
    
    public constructor(delay_ms: int, interval_ms: int, callback: () => void)
    {
        this.delay = delay_ms;
        this.interval = interval_ms;
        this.callback = callback;
    }
    
    public static run(delay_ms: int, interval_ms: int, callback: () => void): Timer
    {
        let timer = new Timer(delay_ms, interval_ms, callback);
        timer.start();
        return timer;
    }
    
    /** Starts the timer. If the timer has already started, this has no effect. */
    public async start(): Promise<void>
    {
        if (this.isRunning)
        {
            console.warn(`Timer has already started.`);
        }
        else
        {
            this.isRunning = true;
            this.hasStopped = false;
            
            await sleep(this.delay);
            
            while (this.isRunning)
            {
                this.callback();
                await sleep(this.interval);
            }
            
            this.hasStopped = true;
        }
    }
    
    public async restart(): Promise<void>
    {
        await this.stop();
        await this.start();
    }
    
    /** Stops the timer and resolves when timer has gracefully stopped */
    public async stop(): Promise<void>
    {
        let halfInterval = Math.ceil(this.interval * 0.5);
        this.isRunning = false;
        
        while (!this.hasStopped)
        {
            await sleep(halfInterval);
        }
    }
}

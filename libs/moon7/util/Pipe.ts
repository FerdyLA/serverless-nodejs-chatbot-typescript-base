
/**
 * let q = pipe(3, day, String.prototype.toUpperCase, String.prototype.toLowerCase);
 * console.log(q);
 */

export function pipe<A,B>(a: A, f1: Fn1<A,B>): B;
export function pipe<A,B,C>(a: A, f1: Fn1<A,B>, f2: Fn1<B,C>): C;
export function pipe<A,B,C,D>(a: A, f1: Fn1<A,B>, f2: Fn1<B,C>, f3: Fn1<C,D>): D;
export function pipe<A,B,C,D,E>(a: A, f1: Fn1<A,B>, f2: Fn1<B,C>, f3: Fn1<C,D>, f4: Fn1<D,E>): E;
export function pipe<A,B,C,D,E,F>(a: A, f1: Fn1<A,B>, f2: Fn1<B,C>, f3: Fn1<C,D>, f4: Fn1<D,E>, f5: Fn1<E,F>): F;
export function pipe(val: any, ...fns: Array<(x: any) => any>): any
{
    for (let fn of fns)
        val = fn.bind(val)(val);
    return val;
}

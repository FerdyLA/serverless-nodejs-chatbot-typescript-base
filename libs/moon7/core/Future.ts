/* tslint:disable:max-line-length */

/**
 * Like a Promise, Future is also an awaitable type.
 * 
 * A Future gives more control on when a promise is resolved/rejected
 * by exposing the resolve/reject functions as methods of this class.
 * 
 * This is useful when used within classes, as sometimes you
 * want one method to return a promise, but some other method
 * to resolve it.
 */
export class Future<T> implements PromiseLike<T>
{
    private readonly promise: Promise<T>;
    private pass!: (value?: T | PromiseLike<T>) => void;
    private fail!: (reason?: any) => void;
    
    public constructor()
    {
        this.promise = new Promise<T>((pass, fail) =>
        {
            this.pass = pass;
            this.fail = fail;
        });
    }
    
    /** Complete the future. If T is not void, value should exist. */
    public resolve(value?: T | PromiseLike<T>): void
    {
        this.pass(value);
    }
    
    /** Fail the future. */
    public reject(reason?: any): void
    {
        this.fail(reason);
    }
    
    /** For debugging. Logs to console. */
    public log(prefix: string = ""): this
    {
        this.then(val => console.info("RESOLVED:", val), err => console.info("REJECTED:", err));
        return this;
    }
    
    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    public then<TResult1, TResult2>(onfulfilled: (value: T) => TResult1 | PromiseLike<TResult1>, onrejected: (reason: any) => TResult2 | PromiseLike<TResult2>): PromiseLike<TResult1 | TResult2>;
    public then<TResult>(onfulfilled: (value: T) => TResult | PromiseLike<TResult>, onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): PromiseLike<TResult>;
    public then<TResult>(onfulfilled: ((value: T) => T | PromiseLike<T>) | undefined | null, onrejected: (reason: any) => TResult | PromiseLike<TResult>): PromiseLike<T | TResult>;
    public then(onfulfilled?: ((value: T) => T | PromiseLike<T>) | undefined | null, onrejected?: ((reason: any) => T | PromiseLike<T>) | undefined | null): PromiseLike<T>
    {
        return this.promise.then(onfulfilled, onrejected);
    }
    
    /*========================================
        Static methods
    ========================================*/
    
    /**
     * Return a promise that will resolve after a delay.
     * await Delay.sleep(1000);
     */
    public static sleep(delay_ms: number): Promise<void>
    {
        return sleep(delay_ms);
    }
    
    /**
     * Future.value("abc", 2000);
     */
    public static value<T>(value: T | PromiseLike<T>, delay_ms: number = 0): Promise<T>
    {
        return new Promise<T>((pass, fail) => setTimeout(() => pass(value), delay_ms));
    }
    
    /**
     * Future.error("abc", 2000);
     */
    public static async error<T>(error: T | PromiseLike<T>, delay_ms: number = 0): Promise<never>
    {
        return new Promise<never>((pass, fail) => setTimeout(() => fail(error), delay_ms));
    }
    
    /**
     * Call a function and return its value after a delay
     * let x = await Future.invoke(() => doSomething(), 1000);
     */
    public static async invoke<T>(fn: () => T, delay_ms: number): Promise<T>
    {
        await sleep(delay_ms);
        return fn();
    }
    
    /**
     * Call a function repeatedly with a delay in between.
     * This is somewhat similar to Unity's InvokeRepeating()
     * let x = await Future.repeat(() => doSomething(), () => isRunning, 1000);
     * @see Signal.repeat();
     */
    public static async repeat(fn: () => any, predicate: () => boolean, delay_ms: number, interval_ms: number= delay_ms): Promise<void>
    {
        // initial delay
        await sleep(delay_ms);
        
        while (predicate())
        {
            fn();
            
            // subsequent interval
            await sleep(interval_ms);
        }
    }
    
    
    /**
     * Add a timeout to a promise.
     * let user1 = await fetchUserData("bob");                          // no timeout
     * let user2 = await Future.timeout(fetchUserData("bob"), 5000);    // with timeout
     */
    public static timeout<T>(promise: PromiseLike<T>, timeout_ms: number): Promise<T>
    {
        return timeout(promise, timeout_ms);
    }
    
    /**
     * Take a function that takes in/returns values, and turn that function into
     * a function that takes in/returns promises.
     */
    public static lift<A1, R>(fn: (a1: A1) => R): (a1: Promise<A1>) => Promise<R>;
    public static lift<A1, A2, R>(fn: (a1: A1, a2: A2) => R): (a1: Promise<A1>, a2: Promise<A2>) => Promise<R>;
    public static lift<A1, A2, A3, R>(fn: (a1: A1, a2: A2, a3: A3) => R): (a1: Promise<A1>, a2: Promise<A2>, a3: Promise<A2>) => Promise<R>;
    public static lift<A1, A2, A3, A4, R>(fn: (a1: A1, a2: A2, a3: A3, a4: A4) => R): (a1: Promise<A1>, a2: Promise<A2>, a3: Promise<A3>, a4: Promise<A4>) => Promise<R>;
    public static lift<A1, A2, A3, A4, A5, R>(fn: (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) => R): (a1: Promise<A1>, a2: Promise<A2>, a3: Promise<A3>, a4: Promise<A4>, a5: Promise<A5>) => Promise<R>;
    public static lift<A1, A2, A3, A4, A5, A6, R>(fn: (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) => R): (a1: Promise<A1>, a2: Promise<A2>, a3: Promise<A3>, a4: Promise<A4>, a5: Promise<A5>, a6: Promise<A6>) => Promise<R>;
    public static lift<A1, A2, A3, A4, A5, A6, A7, R>(fn: (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) => R): (a1: Promise<A1>, a2: Promise<A2>, a3: Promise<A3>, a4: Promise<A4>, a5: Promise<A5>, a6: Promise<A6>, a7: Promise<A7>) => Promise<R>;
    public static lift<A1, A2, A3, A4, A5, A6, A7, A8, R>(fn: (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8) => R): (a1: Promise<A1>, a2: Promise<A2>, a3: Promise<A3>, a4: Promise<A4>, a5: Promise<A5>, a6: Promise<A6>, a7: Promise<A7>, a8: Promise<A8>) => Promise<R>;
    public static lift<A1, A2, A3, A4, A5, A6, A7, A8, A9, R>(fn: (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9) => R): (a1: Promise<A1>, a2: Promise<A2>, a3: Promise<A3>, a4: Promise<A4>, a5: Promise<A5>, a6: Promise<A6>, a7: Promise<A7>, a8: Promise<A8>, a9: Promise<A9>) => Promise<R>;
    public static lift<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, R>(fn: (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9, a10: A10) => R): (a1: Promise<A1>, a2: Promise<A2>, a3: Promise<A3>, a4: Promise<A4>, a5: Promise<A5>, a6: Promise<A6>, a7: Promise<A7>, a8: Promise<A8>, a9: Promise<A9>, a10: Promise<A10>) => Promise<R>;
    public static lift<R>(fn: (...args: Array<any>) => R): (...args: Array<Promise<any>>) => Promise<R>
    {
        return async (...args: Array<Promise<any>>) =>
        {
            return Promise.all(args).then(x => fn(...x));
        };
    }
}

/**
 * Return a promise that will resolve after a delay.
 * await Delay.sleep(1000);
 */
export function sleep(delay_ms: number): Promise<void>
{
    return new Promise<void>(pass => setTimeout(pass, delay_ms));
}

/**
 * Add a timeout to a promise.
 * let user1 = await fetchUserData("bob");                  // no timeout
 * let user2 = await timeout(fetchUserData("bob"), 5000);   // with timeout
 */
export function timeout<T>(promise: PromiseLike<T>, timeout_ms: number): Promise<T>
{
    return new Promise<T>((pass, fail) =>
    {
        promise.then(pass, fail);
        setTimeout(() => fail(new Error("timeout")), timeout_ms);
    });
}

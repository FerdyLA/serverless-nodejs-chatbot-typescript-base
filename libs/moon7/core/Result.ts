// this is used over an api/rpc call

export interface Success<V> { value: V };
export interface Failure<E> { error: E };
export type Result<V,E> = Success<V> | Failure<E>;

export function success<V>(result: Result<V,any>): result is Success<V>
{
    return (result as any).value !== undefined;
}

export function failure<E>(result: Result<any,E>): result is Failure<E>
{
    return (result as any).error !== undefined;
}

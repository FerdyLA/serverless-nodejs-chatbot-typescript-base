// tslint:disable:max-line-length
export const x = 1;

declare global
{
    // #region webpack constants
    const __STAGE__: string;
    const __TYPE__: string;
    const __MAIN__: string;
    const __TARGET__: string;
    
    const __PRODUCTION__: boolean;
    const __DEVELOPMENT__: boolean;
    const __BUILD__: string;
    // #endregion
    
    type byte = number;
    type int = number;
    type float = number;
    
    type int32 = [int];
    type int64 = [int, int];
    type int128 = [int, int, int, int];
    type int256 = [int, int, int, int, int, int, int, int];
    
    type Nil<T> = T | null | undefined;
    type Null<T> = T | null;
    type Void<T> = T | undefined;
    
    type Pair<A, B> = [A, B];
    
    //type Omit<T, K extends keyof T> = T extends any ? Pick<T, Exclude<keyof T, K>> : never;
    type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
    
    
    type Fn<R> = (...a: Array<any>) => R;
    type Fn0<R> = () => R;
    type Fn1<A1, R> = (a1: A1) => R;
    type Fn2<A1, A2, R> = (a1: A1, a2: A2) => R;
    type Fn3<A1, A2, A3, R> = (a1: A1, a2: A2, a3: A3) => R;
    type Fn4<A1, A2, A3, A4, R> = (a1: A1, a2: A2, a3: A3, a4: A4) => R;
    type Fn5<A1, A2, A3, A4, A5, R> = (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) => R;
    type Fn6<A1, A2, A3, A4, A5, A6, R> = (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) => R;
    type Fn7<A1, A2, A3, A4, A5, A6, A7, R> = (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) => R;
    type Fn8<A1, A2, A3, A4, A5, A6, A7, A8, R> = (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8) => R;
    type Fn9<A1, A2, A3, A4, A5, A6, A7, A8, A9, R> = (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9) => R;
    type Fn10<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, R> = (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8, a9: A9, a10: A10) => R;
    
    type Constructor<T> = new(...args: Array<any>) => T;
    
    interface Class<T>
    {
        new(...args: Array<any>): T;
    }
    
    /*interface Generator<Y, S> extends IterableIterator<Y>
    {
        next(value: S): IteratorResult<Y>;
    }*/
    
    /**
     * Table<T> values may be T or undefined.
     * Use Struct<T> for values of T that are never undefined.
     */
    // interface Table<V>
    // {
    //     [name: string]: V | undefined;
    //     [name: number]: V | undefined;
    // }
    
    /**
     * Struct<T> values are T and never undefined.
     * Use Table<T> for values of T that may be undefined.
     */
    interface Struct<T>
    {
        [name: string]: T;
        [name: number]: T;
    }
    
    namespace Reflect
    {
        function getClass<T>(obj: T): Class<T>;
        function getSuperClass(cls: Class<any>): Class<any> | null;
        function getSuperClasses(cls: Class<any>): Array<Class<any>>;
    }
    
    
    /**
     * range(3)     = 0, 1, 2
     * range(3, 7)  = 3, 4, 5, 6
     * 
     * range(stop)
     * range(start, stop)
     * range(start, stop, step)
     * 
     * Usage:
     *      for (let i of range(3, 6)) { ... }
     *      let arr = [...range(5)];
     */
    function range(start: number, stop?: number, step?: number): IterableIterator<number>;
    
    /** Allows you to throw an error as an expression */
    function raise<T>(message?: string): T;
    
    function unreachable(value: never): never;
}

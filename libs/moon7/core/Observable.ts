import { Signal } from "moon7/core/Signal";

export class Observablex<T>
{
    private v: T;
    public readonly changed: Signal<[T,T]>;
    
    public constructor(value: T)
    {
        this.v = value;
        this.changed = new Signal();
    }
    
    public get value(): T
    {
        return this.v;
    }
    
    public set value(newValue: T)
    {
        let oldValue = this.v;
        this.v = newValue;
        this.changed.dispatch([newValue, oldValue]);
    }
}

/*
export function test()
{
    let obj = new Observable<string>("hello");
    obj.changed.add(([n, o]) => console.log(`changed from ${o} to ${n}`));

    obj.value = "yoyo";
    obj.value = "haha";
    obj.value = "lala";
}
*/
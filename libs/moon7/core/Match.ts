
const enum MaybeType
{
    Some, None
}

interface Some<T>
{
    type: MaybeType.Some;
    value: T;
}

interface None
{
    type: MaybeType.None;
}

function Some<T>(value: T): Some<T>
{
    return { type: MaybeType.Some, value };
}

function None(): None
{
    return { type: MaybeType.None };
}

type Maybe<T> = Some<T> | None;


function match<T>(x: Maybe<T>, defaultValue: T): T
{
    switch (x.type)
    {
        case MaybeType.Some:
            return x.value;
            
        case MaybeType.None:
            return defaultValue;
            
        default:
            const exhaustive: never = x;
            throw new Error("Non-exhaustive match");
    }
}


/*

enum Target2
{
    Web,
    Node,
    Electron,
}

type Cases<T, V> =
{
    [P in keyof T]?: V;
}

function select<E, K extends keyof E>(kind: E, value: E[K])
{
    let key = (kind as any)[value];
    return {
        cases<V>(cases: Cases<E,V>): V
        {
            return (cases as any)[key];
        }
    };
}

type Target = "web" | "async-node"

function either(target: any)
{
    
}

let x = select(Target, Target.Node).cases<any>({ Node: true, Web: 3 });

*/

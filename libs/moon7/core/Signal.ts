import { sleep } from "moon7/core/Future";

export type Listener<T> = (value: T) => void;
export type SpawnerListener<T,U> = (signal: Signal<U>) => (value: T) => void;

/**
 * Slots are used by dispatchers to keep track the state of listeners.
 */
export class Slot<T>
{
    public listener: Listener<T>;
    public limit: int;
    public priority: int;
    
    public constructor(listener: Listener<T>, limit: int, priority: int)
    {
        this.listener = listener;
        this.limit = limit;
        this.priority = priority;
    }
    
    public static compare<T>(a: Slot<T>, b: Slot<T>): int
    {
        return a.priority - b.priority;
    }
    
    public canExecute(): boolean
    {
        return this.limit !== 0 && this.priority > 0;
    }
    
    public execute(value: T): void
    {
        if (this.limit > 0) --this.limit;
        this.listener(value);
    }
}


/**
 * Signal is a value dispatcher. You can add listeners to signals to be
 * notified whenever a value is dispatched. It also comes with map, filter
 * and reduce, allowing you to also have publisher/subscriber and
 * eventdispatcher capabilities.
 */
export class Signal<T>
{
    private slots: Array<Slot<T>>;
    private destructor: (() => void) | undefined;
    
    /** A destructor should be specified if the Signal is killable (attached to another source) */
    public constructor(destructor?: () => void)
    {
        this.slots = [];
        this.destructor = destructor;
    }
    
    /** The number of listeners in this signal */
    public get length(): int
    {
        return this.slots.length;
    }
    
    /** Add a callback to this signal. */
    public add(listener: Listener<T>, limit: int = -1, priority: int = 1): void
    {
        let slot = new Slot<T>(listener, limit, priority);
        this.slots.push(slot);
        this.slots.sort(Slot.compare);
    }
    
    /** Add a once-only callback to this signal. */
    public once(listener: Listener<T>, priority: int = 1): void
    {
        this.add(listener, 1, priority);
    }
    
    /** Remove a callback from this signal. */
    public remove(listener: Listener<T>): void
    {
        let i = this.slots.findIndex(s => s.listener === listener);
        this.slots.splice(i, 1);
    }
    
    /** Remove all callbacks from this signal. */
    public clear(): void
    {
        this.slots = [];
    }
    
    /** Returns a promise that resolves on the next dispatch. */
    public next(): Promise<T>
    {
        return new Promise(rs => this.once(rs));
    }
    
    /**
     * Use kill() to "unsubscribe" from signals that are spawned (with spawn,
     * map, filter, or reduce). This allows the spawned signals to be
     * garbage collected.
     */
    public kill(): void
    {
        if (this.destructor) this.destructor();
    }
    
    /** Spawns a new killable signal from the current signal. */
    public spawn<U>(spawner: SpawnerListener<T,U>): Signal<U>
    {
        let signal = new Signal<U>(() => this.remove(listener));
        let listener = spawner(signal);
        this.add(listener);
        return signal;
    }
    
    /**
     * Create another signal that triggers when this signal triggers. The value
     * is transformed through a callback function.
     */
    public map<U>(f: (v: T) => U): Signal<U>
    {
        return this.spawn<U>(s => v => s.dispatch(f(v)));
    }
    
    /**
     * Create a new signal that triggers whenever this signal triggers.
     */
    public filter(): Signal<T>;
    /**
     * Create a new signal that triggers when this signal triggers and
     * satisfies a codition
     */
    public filter(cond: (value: T) => boolean): Signal<T>;
    public filter(cond?: (value: T) => boolean): Signal<T>
    {
        return this.spawn<T>(
            cond ?
                s => x => { if (cond(x)) s.dispatch(x); } :
                s => x => s.dispatch(x)
        );
    }
    
    /**
     * Same concept as array.reduce(), but since a Signal is like an infinite
     * stream, you get a Signal of the reduced value.
     */
    public reduce(fn: (prev: T, curr: T, index: int) => T, initial: T): Signal<T>
    {
        let prev = initial;
        let i = 0;
        
        return this.spawn<T>(s => x =>
        {
            prev = fn(prev, x, i++);
            s.dispatch(prev);
        });
    }
    
    /** Notify all listeners */
    public dispatch(value: T): void
    {
        for (let i = 0; i < this.slots.length; ++i)
        {
            let slot = this.slots[i];
            
            if (slot.canExecute())
                slot.execute(value);
            else
                this.slots.splice(i--, 1);
        }
    }
    
    /**
     * Call a function repeatedly with a delay in between.
     * This is somewhat similar to Unity's InvokeRepeating()
     * @see Future.repeat();
     */
    public static repeat<T>(fn: () => T, predicate: () => boolean, delay_ms: number, interval_ms: number= delay_ms): Signal<T>
    {
        let signal = new Signal<T>();
        
        void async function run(): Promise<void>
        {
            await sleep(delay_ms);
            
            while (predicate())
            {
                let val = fn();
                signal.dispatch(val);
                
                // try again after a delay
                await sleep(interval_ms);
            }
            
            signal.clear();
        }();
        
        return signal;
    }
}

/*
export function test()
{
    
    class ButtonPublisherPattern
    {
        private events: Signal<[string, number]>;
        
        public constructor()
        {
            this.events = new Signal();
        }
        
        public click()
        {
            this.events.dispatch(["click", Math.floor(Math.random() * 10)]);
        }
        
        public drag()
        {
            this.events.dispatch(["drag", Math.floor(Math.random() * 10)]);
        }
        
        public clock()
        {
            this.events.dispatch(["clock", Math.floor(Math.random() * 10)]);
        }
    }
    
    // let startsWithC = b1.events.filter(e => e[0].startsWith("c"));
    // startsWithC.add(x => console.log("starts with c:", x));
    // use btn.clicked.kill() to unsubscribe
    
    class Button
    {
        public readonly clicked: Signal<number>;
        
        public constructor()
        {
            this.clicked = new Signal();
        }
        
        public click()
        {
            this.clicked.dispatch(Math.floor(Math.random() * 10));
        }
    }
    
    console.log("Signals Test");
    
    let b1 = new Button();
    let reduced = b1.clicked.reduce((prev, curr) => prev + curr, 0);
    
    b1.clicked.add(x => console.log("clicked:", x));
    reduced.add(x => console.log("reduced:", x));
    
    //b1.clicked.map(x => x.toString() + "AAA").add(x => console.log("mapped:", x));
    //b1.clicked.next().then(x => console.log("PROMISED ONCE", x));
    //b1.clicked.once(x => console.log("ALSO ONCE", x));
    
    b1.click();
    b1.click();
    b1.click();
    
    reduced.kill();
    console.log("----- reduced killed -----");
    
    b1.click();
    b1.click();
}
*/

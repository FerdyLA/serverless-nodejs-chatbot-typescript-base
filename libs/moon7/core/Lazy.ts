
// first call does compute... then cache the result
// subsequent calls returns cached value

function lazy<T>(fn: Fn0<T>): Fn0<T>
{
    let f: Fn0<T> | null = fn;
    let v: T;
    
    return () =>
    {
        if (f)
        {
            v = f();
            f = null;
        }
        
        return v;
    };
}


export class Lazy<T>
{
    private readonly fn: Fn0<T>;
    
    public constructor(fn: Fn0<T>)
    {
        this.fn = lazy(fn);
    }
    
    public static make<T>(fn: Fn0<T>): Fn0<T>
    {
        return lazy(fn);
    }
    
    public get value(): T
    {
        return this.fn();
    }
}

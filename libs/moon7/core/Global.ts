
export function* range(start: number, stop?: number, step: number= 1): IterableIterator<number>
{
    if (stop == undefined)
        for (let i = 0; i < start; i += step) yield i;
    else
        for (let i = start; i < stop; i += step) yield i;
}

/** Allows you to throw an error as an expression */
export function raise<T>(message?: string): T
{
    throw new Error(message);
}

export function unreachable(value: never): never
{
    throw new Error(`Illegal value: ${value}`);
}

Reflect.getClass = function<T>(obj: T): Class<T>
{
    return obj.constructor as Class<T>;
};

Reflect.getSuperClass = function(cls: Class<any>): Class<any> | null
{
    let proto = Object.getPrototypeOf(cls.prototype);
    return proto && proto.constructor;
};

Reflect.getSuperClasses = function(cls: Class<any>): Array<Class<any>>
{
    let classes = [];
    let curr: Class<any> | null = Reflect.getSuperClass(cls);
    while (curr != null)
    {
        classes.push(curr);
        curr = Reflect.getSuperClass(curr);
    }
    return classes;
};

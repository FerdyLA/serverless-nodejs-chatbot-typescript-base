module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class Client {
    constructor(api, service) {
        this.api = api;
        this.service = service;
    }
}
exports.Client = Client;
class ClientModel extends Client {
    constructor(api, service) {
        super(api, service);
    }
    async load(key) {
        return this.api.call(`${this.service}.load`, key);
    }
    async save(key, value) {
        return this.api.call(`${this.service}.save`, key, value);
    }
    async remove(key) {
        return this.api.call(`${this.service}.remove`, key);
    }
    async exist(key) {
        return this.api.call(`${this.service}.exist`, key);
    }
    async keys() {
        return this.api.call(`${this.service}.keys`);
    }
    async list() {
        return this.api.call(`${this.service}.list`);
    }
}
exports.ClientModel = ClientModel;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __webpack_require__(10);
const Api_1 = __webpack_require__(11);
const AuthClient_1 = __webpack_require__(12);
const PermissionClient_1 = __webpack_require__(13);
const UserClient_1 = __webpack_require__(14);
const InviteClient_1 = __webpack_require__(15);
const SignUpClient_1 = __webpack_require__(16);
const RegistryClient_1 = __webpack_require__(17);
const AccountClient_1 = __webpack_require__(18);
const MemberClient_1 = __webpack_require__(19);
const LocationClient_1 = __webpack_require__(20);
const ComponentClient_1 = __webpack_require__(21);
const ComponentCategoryClient_1 = __webpack_require__(22);
const TaskClient_1 = __webpack_require__(23);
const TaskCategoryClient_1 = __webpack_require__(26);
const DeviceClient_1 = __webpack_require__(27);
// import { StorageClient } from "api/services/PageClient";
const URLs = {
    "dev": "https://ekivt8v6n9.execute-api.ap-southeast-1.amazonaws.com",
    "prod": "https://ekivt8v6n9.execute-api.ap-southeast-1.amazonaws.com",
};
const stage = "dev";
class Api {
    constructor(project, domain, apiKey) {
        this.url = URLs[stage];
        this.project = project;
        this.apiKey = apiKey;
        this.stage = stage;
        this.domain = domain;
        let baseURL = `${this.url}/${stage}`;
        this.headers =
            {
                "x-omni-project": project,
                "x-omni-domain": domain,
                "x-omni-apikey": apiKey,
            };
        this.ajax = axios_1.default.create({
            baseURL: baseURL,
            headers: this.headers,
        });
        this.auth = new AuthClient_1.AuthClient(this, "auth");
        this.permission = new PermissionClient_1.PermissionClient(this, "permission");
        this.user = new UserClient_1.UserClient(this, "user");
        this.invite = new InviteClient_1.InviteClient(this, "invite");
        this.signup = new SignUpClient_1.SignUpClient(this, "signup");
        this.registry = new RegistryClient_1.RegistryClient(this, "registry");
        this.account = new AccountClient_1.AccountClient(this, "account");
        this.member = new MemberClient_1.MemberClient(this, "member");
        this.location = new LocationClient_1.LocationClient(this, "location");
        this.component = new ComponentClient_1.ComponentClient(this, "component");
        this.componentcategory = new ComponentCategoryClient_1.ComponentCategoryClient(this, "componentCategory");
        this.taskcategory = new TaskCategoryClient_1.TaskCategoryClient(this, "taskcategory");
        this.task = new TaskClient_1.TaskClient(this, "task");
        this.device = new DeviceClient_1.DeviceClient(this, "device");
        // this.storage = new PageClient(this, "content");
    }
    setDomain(domain) {
        this.domain = domain;
        this.headers["x-omni-domain"] = domain;
    }
    async call(method, ...args) {
        let [service, fn] = method.split(".");
        try {
            //let result = await this.ajax.post(url, { method: fn, args });
            let result = await this.ajax.request({
                url: `/rpc/${service}/${fn}`,
                method: "post",
                data: args,
                headers: this.headers,
            });
            let data = result.data;
            if (Api_1.isSuccess(data)) {
                return data.value;
            }
            else {
                console.log("RPC LOGIC ERROR");
                throw data.error;
            }
        }
        catch (ex) {
            console.log("RPC CALL ERROR:");
            throw ex;
        }
    }
}
exports.Api = Api;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Api_1 = __webpack_require__(1);
async function testRpc() {
    console.log("Signup Testing via RPC calls");
    try {
        console.log("HEADERS:");
        let api = new Api_1.Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.log(api.headers);
        console.log("=================================");
        // console.log("ACCOUNT FORGOT (EMBRIO):");
        // console.log(await api.account.forgotPassword("ferdinand.lim@embrioent.com"));
        // console.log("=================================");
        console.log("LOGIN:");
        // let { user } = await api.auth.login("limhjf+embrio@gmail.com", "ferdy");
        let { user } = await api.auth.login("technician@embrioent.com", "bb40thcoy");
        // let { user } = await api.auth.login("manager@embrioent.com", "bb40thcoy");
        if (!user)
            throw "Invalid login";
        console.log("- - - - - - - - - - - - - - - - -");
        console.log("LOGIN OK:");
        console.log(user.name);
        console.log("=================================");
        // console.log("CHANGE PASSWORD:");
        // console.log(await api.account.updatePassword({email: "limhjf+embrio@gmail.com", password:"ferdy"}));
        // console.log("=================================");
        console.log("ACCOUNT DETAILS:");
        let account = await api.account.viewDetails();
        console.log(account);
        console.log("=================================");
        console.log("CHANGE ACCOUNT DETAILS:");
        account.add = account.add ? +account.add + 1 : 0;
        // delete account.add;
        account.name = "Ferdy Test";
        console.log(account);
        console.log("=================================");
        console.log("UPDATE ACCOUNT DETAILS:");
        console.log(await api.account.updateDetails(account));
        console.log("=================================");
        console.log("ACCOUNT DETAILS (UDPATED):");
        console.log(await api.account.viewDetails());
        console.log("=================================");
        console.log("ACCOUNT FORGOT (GMAIL):");
        console.log(await api.account.forgotPassword("limhjf+embrio@gmail.com"));
        console.log("=================================");
        console.log(".............");
        console.log("............");
        console.log("...........");
        console.log("..........");
        console.log(".........");
        console.log("........");
        console.log(".......");
        console.log("......");
        console.log(".....");
        console.log("....");
        console.log("...");
        console.log("..");
        console.log(".");
    }
    catch (ex) {
        console.log(ex);
    }
}
exports.testRpc = testRpc;
async function main(...args) {
    console.log("Running tests:", args);
    try {
        testRpc();
    }
    catch (ex) {
        console.log("ERROR", ex);
    }
}
exports.main = main;
async function reset(...args) {
    console.log("Running reset:", args);
    try {
        if (args.length <= 0)
            throw "Not enough arguments. 2 required. New Password & Token.";
        console.log("HEADERS:");
        let api = new Api_1.Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.log(api.headers);
        console.log("=================================");
        console.log("ACCOUNT RESET:");
        console.log(await api.account.resetPassword({ token: args[1], password: args[0] }));
        console.log("=================================");
    }
    catch (ex) {
        console.log("ERROR", ex);
    }
}
exports.reset = reset;
async function forgot(...args) {
    console.log("Running reset:", args);
    try {
        if (args.length <= 0)
            throw "Not enough arguments. 1 required. Email.";
        console.log("HEADERS:");
        let api = new Api_1.Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.log(api.headers);
        console.log("=================================");
        console.log("ACCOUNT FORGOT (", args[0], "):");
        console.log(await api.account.forgotPassword(args[0]));
        console.log("=================================");
    }
    catch (ex) {
        console.log("ERROR", ex);
    }
}
exports.forgot = forgot;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(4);
module.exports = __webpack_require__(7);


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(5);
const g = __webpack_require__(6);
void async function main() {
    let scope = typeof window !== "undefined" ? window : global;
    let module = g;
    for (let x of Object.keys(module)) {
        //console.log("GLOBALS:", x);
        scope[x] = module[x];
    }
}();


/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("reflect-metadata");

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
function* range(start, stop, step = 1) {
    if (stop == undefined)
        for (let i = 0; i < start; i += step)
            yield i;
    else
        for (let i = start; i < stop; i += step)
            yield i;
}
exports.range = range;
/** Allows you to throw an error as an expression */
function raise(message) {
    throw new Error(message);
}
exports.raise = raise;
function unreachable(value) {
    throw new Error(`Illegal value: ${value}`);
}
exports.unreachable = unreachable;
Reflect.getClass = function (obj) {
    return obj.constructor;
};
Reflect.getSuperClass = function (cls) {
    let proto = Object.getPrototypeOf(cls.prototype);
    return proto && proto.constructor;
};
Reflect.getSuperClasses = function (cls) {
    let classes = [];
    let curr = Reflect.getSuperClass(cls);
    while (curr != null) {
        classes.push(curr);
        curr = Reflect.getSuperClass(curr);
    }
    return classes;
};


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Main_1 = __webpack_require__(8);
let [node, script, ...args] = process.argv;
Main_1.main(...args);


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// import { main as rpcMain } from "RpcTest";
// import { main as queryMain } from "QueryTest";
// import { main as hidMain } from "HidTest";
const ApiTest_1 = __webpack_require__(9);
const InviteTest_1 = __webpack_require__(28);
const SignupTest_1 = __webpack_require__(29);
const AccountTest_1 = __webpack_require__(2);
const AccountTest_2 = __webpack_require__(2);
const AccountTest_3 = __webpack_require__(2);
const LocationTest_1 = __webpack_require__(30);
const ComponentTest_1 = __webpack_require__(31);
const ComponentCategoryTest_1 = __webpack_require__(32);
const TaskTest_1 = __webpack_require__(33);
const TaskCategoryTest_1 = __webpack_require__(34);
const DeviceTest_1 = __webpack_require__(35);
const DemoTest_1 = __webpack_require__(36);
async function main(...args) {
    let [cmd, ...rest] = args;
    switch (cmd) {
        case "api":
            {
                console.log("running cmd api");
                ApiTest_1.main(...rest);
                break;
            }
        case "invite":
            {
                console.log("running cmd invite");
                InviteTest_1.main(...rest);
                break;
            }
        case "signup":
            {
                console.log("running cmd signup");
                SignupTest_1.main(...rest);
                break;
            }
        case "account":
            {
                console.log("running cmd account");
                AccountTest_1.main(...rest);
                break;
            }
        case "resetpwd":
            {
                console.log("running cmd resetpwd");
                AccountTest_2.reset(...rest);
                break;
            }
        case "forgotpwd":
            {
                console.log("running cmd forgotpwd");
                AccountTest_3.forgot(...rest);
                break;
            }
        case "demo":
            {
                console.log("running cmd demo");
                DemoTest_1.demo(...rest);
                break;
            }
        case "location":
            {
                console.log("running cmd location");
                LocationTest_1.main(...rest);
                break;
            }
        case "component":
            {
                console.log("running cmd component");
                ComponentTest_1.main(...rest);
                break;
            }
        case "componentcategory":
            {
                console.log("running cmd componetCategory");
                ComponentCategoryTest_1.main(...rest);
                break;
            }
        case "task":
            {
                console.log("running cmd task");
                TaskTest_1.main(...rest);
                break;
            }
        case "taskcategory":
            {
                console.log("running cmd taskcategory");
                TaskCategoryTest_1.main(...rest);
                break;
            }
        case "device":
            {
                console.log("running cmd device");
                DeviceTest_1.main(...rest);
                break;
            }
        default:
            {
                console.log(`invalid cmd: ${cmd}`);
            }
    }
}
exports.main = main;
// npm run cmd ______


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Api_1 = __webpack_require__(1);
async function testRpc() {
    console.log("Login & Domain Change Testing via RPC calls");
    try {
        //let api = new Api("18r", "system/toUNNCyvOUFKiG3gyXL7P4ZM");
        //let api = new Api("18r", "web/srqO01q6ZZi79T9sNlmVwUAn");
        //let api = new Api("18r", "mobile/sMZVm9kvhkNRMMslhiTjyd4d");
        //let api = new Api("18r", "eps/Ch7YEfQKh22OTypwC1hmScbT");
        console.log("HEADERS:");
        let api = new Api_1.Api("caviot", "", "sys/9SqweUQ50G2V29WRAjjCfBTU");
        // let api = new Api("caviot", "", "test/0fSt0yPzAWNvIU0qTOgkv6Jr");
        // let api = new Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.log(api.headers);
        console.log("=================================");
        // console.log("SERVER TIME:");
        // console.log(await api.call("test.timestamp"));
        // // console.log(await api.call("test.elrhckj"));
        // console.log("=================================");
        // console.log("Perm check: ",Permission.match("caviot/role/guest","caviot/role/%%guest%%"));
        // console.log("LIST CONTEXT PERMS:");
        // console.log(await api.auth.checkContextPermissions());
        // console.log("=================================");
        // console.log("RUBBISH PERM TEST:");
        // console.log(await api.call("test.rubbishPerm"));
        // console.log("=================================");
        // let env = await api.call("test.getEnv");
        // console.log("SERVER ENV:", env);
        // let z = await api.call("auth.setPassword", "app_dev@embrioent.com", "Omn1tr1b3$");
        // console.log("SET PWD (Master):", z);
        let t = await api.call("auth.setPassword", "technician@embrioent.com", "00000");
        console.log("SET PWD (Ferdy)", t);
        let m = await api.call("auth.setPassword", "manager@embrioent.com", "00000");
        console.log("SET PWD (Ferdy)", m);
        // let z = await api.call("auth.setPassword", "davidnguyen968@gmail.com", "C@vi0tD3v");
        // console.log("SET PWD (Master):", z);
        // let f = await api.call("auth.setPassword", "dnthien@cmc.com.vn", "C@vi0tD3v");
        // console.log("SET PWD (Ferdy)", f);
        // // console.log("Test Change Domain:", await api.call("test.changeDomain"));
        // console.log("LOGIN:");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("ferdinand.lim@embrioent.com", "bb40thcoy");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("davidnguyen968@gmail.com", "C@vi0tD3v");
        // let { user } = await api.auth.login("dnthien@cmc.com.vn", "123456");
        // if (!user) throw "Invalid login";
        // console.log("- - - - - - - - - - - - - - - - -");
        // console.log("LOGIN OK:");
        // console.log(user);
        // console.log("=================================");
        // console.log(api.headers["x-omni-accesstoken"]);
        // console.log("=================================");
        // // console.log("LIST CONTEXT PERMS:");
        // // console.log(await api.auth.checkContextPermissions());
        // // console.log("=================================");
        // // console.log("CHECK Permissions:");
        // // console.log(await api.call("permission.listFunctionPermissions"));
        // // console.log("=================================");
        // // console.log("CHECK Methods:");
        // // console.log(await api.call("permission.listAllowedMethods"));
        // // console.log("=================================");
        // // // console.log("CHECK 3:");
        // // // console.log(await api.call("test.check3"));
        // // // console.log("=================================");
        // // // console.log("LIST CONTEXT PERMS:");
        // // // console.log(await api.auth.checkContextPermissions());
        // // // console.log("=================================");
        // // // console.log("RUBBISH PERM TEST:");
        // // // console.log(await api.call("test.rubbishPerm"));
        // // // console.log("=================================");
        // // // console.log("CHECK TOKEN:");
        // // // console.log(await api.call("auth.checkToken"));
        // // // console.log("=================================");
        // // console.log("DOMAINS:");
        // // let domains = await api.member.listDomains();
        // // console.log(domains);
        // // console.log("=================================");
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        // console.log("LOGIN:");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("ferdinand.lim@embrioent.com", "bb40thcoy");
        // console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"))
        // // let { user } = await api.auth.login("davidnguyen968@gmail.com", "C@vi0tD3v");
        // // console.log(await api.auth.login("dnthien@cmc.com.vn", "123456"));
        // console.log("=================================");
        // console.log("LOGOUT:");
        // console.log(await api.auth.logout());
        // console.log("=================================");
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        // console.log("LOGIN:");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("ferdinand.lim@embrioent.com", "bb40thcoy");
        // console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"))
        // // let { user } = await api.auth.login("davidnguyen968@gmail.com", "C@vi0tD3v");
        // // console.log(await api.auth.login("dnthien@cmc.com.vn", "123456"));
        // console.log("=================================");
        // console.log("LOGOUT:");
        // console.log(await api.auth.logout());
        // console.log("=================================");
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        // console.log("LOGIN:");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("ferdinand.lim@embrioent.com", "bb40thcoy");
        // console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"))
        // // let { user } = await api.auth.login("davidnguyen968@gmail.com", "C@vi0tD3v");
        // // console.log(await api.auth.login("dnthien@cmc.com.vn", "123456"));
        // console.log("=================================");
        // console.log("LOGOUT:");
        // console.log(await api.auth.logout());
        // console.log("=================================");
        // console.log("REFRESH:");
        // // console.log(await api.auth.changeDomain(domains[Math.floor(Math.random() * domains.length)].key));
        // // console.log(await api.auth.changeDomain("test"));
        // // console.log(await api.auth.changeDomain("caviot-master"));
        // console.log(await api.auth.refresh());
        // console.log("=================================");
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        // console.log("LOGOUT:");
        // console.log(await api.auth.logout());
        // console.log("=================================");
        // console.log("HEADERS:");
        // console.log(api.headers);
        // console.log("=================================");
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        // console.log("LOGIN:");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("ferdinand.lim@embrioent.com", "bb40thcoy");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("davidnguyen968@gmail.com", "C@vi0tD3v");
        // console.log(await api.auth.login("dnthien@cmc.com.vn", "123456"));
        // console.log("=================================");
        // console.log("HEADERS:");
        // console.log(api.headers);
        // console.log("=================================");
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        // console.log("Switch To Master Domain 1:");
        // // console.log(await api.auth.changeDomain(domains[Math.floor(Math.random() * domains.length)].key));
        // // console.log(await api.auth.changeDomain("test"));
        // console.log(await api.auth.changeDomain("caviot-master"));
        // console.log("=================================");
        // console.log("REFRESH:");
        // // console.log(await api.auth.changeDomain(domains[Math.floor(Math.random() * domains.length)].key));
        // // console.log(await api.auth.changeDomain("test"));
        // // console.log(await api.auth.changeDomain("caviot-master"));
        // console.log(await api.auth.refresh());
        // console.log("=================================");
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        // console.log("LOGOUT:");
        // console.log(await api.auth.logout());
        // console.log("=================================");
        // console.log("HEADERS:");
        // console.log(api.headers);
        // console.log("=================================");
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        // console.log("LOGIN:");
        // // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // // let { user } = await api.auth.login("ferdinand.lim@embrioent.com", "bb40thcoy");
        // console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"))
        // // let { user } = await api.auth.login("davidnguyen968@gmail.com", "C@vi0tD3v");
        // // console.log(await api.auth.login("dnthien@cmc.com.vn", "123456"));
        // console.log("=================================");
        // console.log("HEADERS:");
        // console.log(api.headers);
        // console.log("=================================");
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        // console.log("Switch To Master Domain 2:");
        // // console.log(await api.auth.changeDomain(domains[Math.floor(Math.random() * domains.length)].key));
        // // console.log(await api.auth.changeDomain("test"));
        // console.log(await api.auth.changeDomain("caviot-master"));
        // console.log("=================================");
        // console.log("GET USER:");
        // console.log(await api.call("test.getUser"));
        // console.log("=================================");
        // console.log("HEADERS:");
        // console.log(api.headers);
        // console.log("=================================");
        // console.log("LIST CONTEXT PERMS:");
        // console.log(await api.auth.checkContextPermissions());
        // console.log("=================================");
        // console.log("CHECK Permissions:");
        // console.log(await api.call("permission.listFunctionPermissions"));
        // console.log("=================================");
        // console.log("CHECK Methods:");
        // console.log(await api.call("permission.listAllowedMethods"));
        // console.log("=================================");
        // console.log("RUBBISH PERM TEST:");
        // console.log(await api.call("test.rubbishPerm"));
        // console.log("=================================");
        // console.log(api.headers["x-omni-accesstoken"]);
        // console.log("=================================");
        // console.log("CLIENT API:");
        // console.log(api.domain);
        // console.log("=================================");
        // console.log(api.headers["x-omni-accesstoken"]);
        // console.log("=================================");
        // console.log("CONTEXT USER:");
        // console.log(await api.call("test.getUser"));
        // console.log(await api.auth.checkContextUser());
        // console.log("=================================");
        // console.log(api.headers["x-omni-accesstoken"]);
        // console.log("=================================");
        // console.log("CHECK TOKEN:");
        // console.log(await api.call("auth.checkToken"));
        // console.log("=================================");
        // await api.registry.save("18r/config/roles.json",
        // {
        //     "caviot/guest":                   { en: "Guest", zh: "Guest" },
        //     "caviot/member":                  { en: "Member", zh: "Member" },
        //     "caviot/member/building/admin":   { en: "Building Admin", zh: "Building Admin" },
        //     "caviot/member/building/staff":   { en: "Building Staff", zh: "Building Staff" },
        //     "caviot/member/building/visitor": { en: "Building Visitor", zh: "Building Visitor" },
        //     "caviot/member/office/admin":     { en: "Office Admin", zh: "Office Admin" },
        //     "caviot/member/office/staff":     { en: "Office Staff", zh: "Office AdStaffin" },
        //     "caviot/member/office/visitor":   { en: "Office Visitor", zh: "Office Visitor" },
        //     "caviot/member/retail/admin":     { en: "Retail Admin", zh: "Retail Admin" },
        //     "caviot/member/retail/staff":     { en: "Retail Staff", zh: "Retail Staff" },
        //     "caviot/member/retail/visitor":   { en: "Retail Visitor", zh: "Retail Visitor" },
        // });
        // let key = await api.auth.createApiKey("blah",
        // {
        //     tokens:
        //     {
        //         accessDuration: null,           // null means never expire
        //         refreshDuration: null,          // other examples: { minutes: 15 }, { days: 3 }
        //     },
        //     permissions:
        //     {
        //         baseApiRoles: ["caviot/dev/develop", "caviot/guest/view"],  // default roles when using the api key
        //         baseMemberRoles: ["caviot/user/view"],                  // default roles when authenticated (in addition to roles above)
        //     }
        // });
        // "permissions": { "baseApiRoles": ["caviot/system", "caviot/guest"], "baseMemberRoles": ["caviot/member"] }
        console.log(".............");
        console.log("............");
        console.log("...........");
        console.log("..........");
        console.log(".........");
        console.log("........");
        console.log(".......");
        console.log("......");
        console.log(".....");
        console.log("....");
        console.log("...");
        console.log("..");
        console.log(".");
    }
    catch (ex) {
        console.log(ex);
    }
}
exports.testRpc = testRpc;
async function main(...args) {
    console.log("Running tests:", args);
    try {
        testRpc();
        //     console.log("HEADERS:");
        //     let api = new Api("caviot", "", "sys/9SqweUQ50G2V29WRAjjCfBTU");
        //     console.log(api.headers);
        //     console.log("=================================");
        //     console.log("LOGIN:");
        //     let { user } = await api.auth.login("dnthien@cmc.com.vn", "123456");
        //     if (!user) throw "Invalid login";
        //     console.log("- - - - - - - - - - - - - - - - -");
        //     console.log("LOGIN OK:");
        //     console.log(user);
        //     console.log("=================================");
        //     console.log(api.headers["x-omni-accesstoken"]);
        //     console.log("=================================");
        //     console.log("GET USER:");
        //     console.log(await api.call("test.getUser"));
        //     console.log("=================================");
        //     console.log("LOGOUT:");
        //     console.log(await api.auth.logout());
        //     console.log("=================================");
        //     console.log("LOGIN:");
        //     console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"));
        //     console.log("=================================");
        //     console.log("GET USER:");
        //     console.log(await api.call("test.getUser"));
        //     console.log("=================================");
        //     console.log("REFRESH:");
        //     console.log(await api.auth.refresh());
        //     console.log("=================================");
        //     console.log("GET USER:");
        //     console.log(await api.call("test.getUser"));
        //     console.log("=================================");
        //     console.log("LOGOUT:");
        //     console.log(await api.auth.logout());
        //     console.log("=================================");
        //     console.log("GET USER:");
        //     console.log(await api.call("test.getUser"));
        //     console.log("=================================");
        //     console.log("LOGIN:");
        //     console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"));
        //     console.log("=================================");
        //     console.log("GET USER:");
        //     console.log(await api.call("test.getUser"));
        //     console.log("=================================");
        //     console.log("Switch To Master Domain:");
        //     console.log(await api.auth.changeDomain("caviot-master"));
        //     console.log("=================================");
        //     console.log("GET USER:");
        //     console.log(await api.call("test.getUser"));
        //     console.log("=================================");
        //     console.log("REFRESH:");
        //     console.log(await api.auth.refresh());
        //     console.log("=================================");
        //     console.log("GET USER:");
        //     console.log(await api.call("test.getUser"));
        //     console.log("=================================");
        //     console.log("LOGOUT:");
        //     console.log(await api.auth.logout());
        //     console.log("=================================");
        //     console.log("GET USER:");
        //     console.log(await api.call("test.getUser"));
        //     console.log("=================================");
        //     console.log("LOGIN:");
        //     console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"));
        //     console.log("=================================");
        //     console.log("GET USER:");
        //     console.log(await api.call("test.getUser"));
        //     console.log("=================================");
        //     console.log("LOGOUT:");
        //     console.log(await api.auth.logout());
        //     console.log("=================================");
        //     console.log("GET USER:");
        //     console.log(await api.call("test.getUser"));
        //     console.log("=================================");
        //     console.log("LOGIN:");
        //     console.log(await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$"));
        //     console.log("=================================");
        //     console.log("GET USER:");
        //     console.log(await api.call("test.getUser"));
        //     console.log("=================================");
    }
    catch (ex) {
        console.log("ERROR", ex);
    }
}
exports.main = main;


/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
function isSuccess(result) {
    return result.error == null;
}
exports.isSuccess = isSuccess;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __webpack_require__(0);
class AuthClient extends Client_1.Client {
    setDomain(domain) {
        this.api.headers["x-omni-domain"] = domain;
    }
    get accessToken() {
        return this._accessToken || this.api.headers["x-omni-accesstoken"];
    }
    set accessToken(value) {
        console.log(">>> Set AccessToken: ", value, " <<<");
        if (value) {
            this.api.headers["x-omni-accesstoken"] = value;
            this._accessToken = value;
        }
        else {
            delete this.api.headers["x-omni-accesstoken"];
            this._accessToken = value;
        }
    }
    get refreshToken() {
        return this._refreshToken;
    }
    set refreshToken(value) {
        // console.log(">>> Set RefreshToken: ",value," <<<");
        this._refreshToken = value;
    }
    /** key is the email/username/whatever used to identify users */
    async login(key, password) {
        try {
            let result = await this.api.call("auth.login", key, password);
            this.accessToken = result.accessToken;
            this.refreshToken = result.refreshToken;
            this.user = result.user;
            return result;
        }
        catch (ex) {
            console.log("ERROR:", ex);
            throw ex;
        }
    }
    async logout() {
        await this.api.call("auth.logout", this.refreshToken);
        this.accessToken = undefined;
        this.refreshToken = undefined;
        this.user = undefined;
        this.api.setDomain("");
    }
    async refresh() {
        try {
            let result = await this.api.call("auth.refresh", this.refreshToken);
            this.accessToken = result.accessToken;
            this.refreshToken = result.refreshToken;
            this.user = result.user;
            return result.user;
        }
        catch (ex) {
            console.log("ERROR:", ex);
            throw ex;
        }
    }
    async verify() {
        try {
            let result = await this.api.call("auth.verify");
            this.user = result.user;
            return result.user;
        }
        catch (ex) {
            console.log("ERROR:", ex);
            throw ex;
        }
    }
    async changeDomain(domainKey) {
        try {
            let result = await this.api.call("auth.changeDomain", this.refreshToken, domainKey);
            this.user = result.user;
            this.accessToken = result.accessToken;
            this.refreshToken = result.refreshToken;
            this.api.setDomain(domainKey);
            return { user: result.user, domain: domainKey };
        }
        catch (ex) {
            console.log("ERROR:", ex);
            throw ex;
        }
    }
    async createApiKey(app, options) {
        return await this.api.call("auth.createApiKey", app, options);
    }
    async checkContextPermissions() {
        return await this.api.call("test.listContextRoles");
    }
    async checkContextUser() {
        return await this.api.call("test.getUser");
    }
}
exports.AuthClient = AuthClient;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __webpack_require__(0);
class PermissionClient extends Client_1.Client {
    /** grants permissions to user/group/api */
    async grant(type, typeId, ...permissions) {
        return this.api.call("permission.grant", type, typeId, permissions);
    }
    /** revokes permission from user/group/api */
    async revoke(type, typeId, ...permissions) {
        return this.api.call("permission.revoke", type, typeId, permissions);
    }
    /** retrieve permission list of user/group/api */
    async list(type, typeId) {
        return this.api.call("permission.list", type, typeId);
    }
    /** retrieve permission keys of user/group/api */
    async keys() {
        return this.api.call("permission.keys");
    }
    /** retrieve listFunctionPermission of user/group/api */
    async listFunctionPermission() {
        return this.api.call("permission.listFunctionPermission");
    }
    /** retrieve listAllowedMethods of user/group/api */
    async listAllowedMethods() {
        return this.api.call("permission.listAllowedMethods");
    }
}
exports.PermissionClient = PermissionClient;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __webpack_require__(0);
class UserClient extends Client_1.Client {
    /** Return true if deleted. Require only id */
    async view(id) {
        return await this.api.call("component.delete", id);
    }
}
exports.UserClient = UserClient;


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __webpack_require__(0);
class InviteClient extends Client_1.Client {
    /** create invite with email address, permission {role, levels granted, start date, end date} */
    async createInv(invite) {
        return await this.api.call("invite.create", invite);
    }
    /** retrieve ALL permission list of user/group/api for user creation */
    async listRoles() {
        // TODO: List roles in current domain
        return await this.api.call("member.listRoles()");
        // throw "TODO: List roles in current domain";
        //return await this.api.call("permission.findUserPermissionsByTypes", [PermissionRoleType.Building, PermissionRoleType.Office, PermissionRoleType.Retail]);
    }
    /** check if email is invited, grab invite data */
    async list(email) {
        return await this.api.call("invite.list", email);
    }
    /** check email format */
    async emailVerification(email) {
        return await this.api.call("invite.emailVerification", email);
    }
}
exports.InviteClient = InviteClient;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __webpack_require__(0);
class SignUpClient extends Client_1.Client {
    /** create invite with email address, permission {role, levels granted, start date, end date} */
    // public async signup(email: string, info: SignUp): Promise<boolean>
    // {
    //        return await this.api.call("invite.create","ferdy@ferdy.sg",permissions);
    // }
    async createAccount(user) {
        return await this.api.call("signup.createAccount", user);
    }
    async emailVerification(email) {
        return await this.api.call("signup.emailVerification", email);
    }
}
exports.SignUpClient = SignUpClient;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __webpack_require__(0);
class RegistryClient extends Client_1.Client {
    async load(key) {
        let result = await this.api.call(`${this.service}.load`, key);
        return result && result.data;
    }
    async save(key, value) {
        let data = { key, data: value };
        return this.api.call(`${this.service}.save`, key, data);
    }
    async remove(key) {
        return this.api.call(`${this.service}.remove`, key);
    }
    async exist(key) {
        return this.api.call(`${this.service}.exist`, key);
    }
    async keys(options = {}) {
        return this.api.call(`${this.service}.keys`, options);
    }
    async list() {
        let result = await this.api.call(`${this.service}.list`);
        return result;
    }
}
exports.RegistryClient = RegistryClient;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __webpack_require__(0);
class AccountClient extends Client_1.Client {
    /** update Account information. Uses AccountInfo model */
    async updateDetails(info) {
        return await this.api.call("account.updateDetails", info);
    }
    /** update Account password. Uses AccountPassword model */
    async updatePassword(pwd) {
        return await this.api.call("account.updatePassword", pwd);
    }
    /** update Account information. Uses AccountInfo model */
    async viewDetails() {
        return await this.api.call("account.accountDetails");
    }
    /** forgot password. sends an email to user with password reset url */
    async forgotPassword(email) {
        return await this.api.call("account.forgetPassword", email);
    }
    /** reset password. to be used with reset webpage that accepts new password and token */
    async resetPassword(info) {
        return await this.api.call("account.resetPassword", info);
    }
}
exports.AccountClient = AccountClient;


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __webpack_require__(0);
class MemberClient extends Client_1.Client {
    /** joins an existing domain using the invite */
    async joinDomain(inviteId) {
        return await this.api.call("member.joinDomain", inviteId);
    }
    /** creates a domain using just the name of the domain */
    async createDomain(name) {
        return await this.api.call("member.createDomain", name);
    }
    /** list all domains that user is a member of */
    async listDomains() {
        return await this.api.call("member.listDomains");
    }
    /** list all roles that user holds of a domain */
    async listRoles() {
        return await this.api.call("member.listRoles");
    }
    /** list all roles present in the domain the user is currently in */
    async listAllRoles() {
        return await this.api.call("member.listAllRoles");
    }
}
exports.MemberClient = MemberClient;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __webpack_require__(0);
class LocationClient extends Client_1.Client {
    /** Returns true if created. Require only name, data (can be empty object), parentId (can be empty string) */
    async create(location) {
        return await this.api.call("location.create", location);
    }
    /** Returns true if edited.Require only key for identification and updated fields */
    async edit(location) {
        return await this.api.call("location.edit", location);
    }
    /** Return true if deleted. Require only Key */
    async delete(key) {
        return await this.api.call("location.delete", key);
    }
    /** Return Location object. Require only Key */
    async view(key) {
        return await this.api.call("location.view", key);
    }
    /** Return Location object. Require only Key */
    async children(key) {
        return await this.api.call("location.children", key);
    }
    /** Return Location object. Require only Key */
    async tree(key) {
        return await this.api.call("location.tree", key);
    }
    /** Return all Components present in the Location and all its children. Require Location key */
    async components(locationKey) {
        return await this.api.call("location.components", locationKey);
    }
}
exports.LocationClient = LocationClient;


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __webpack_require__(0);
class ComponentClient extends Client_1.Client {
    /** Returns true if created. Require only locationkey ,categorykey, brand,model,
     * parts(can be empty array),custom (can be undefined empty object ),traits(can be undefined empty object) */
    async add(info) {
        return await this.api.call("component.add", info);
    }
    /** Returns true if created. Require only Component id, locationkey ,categorykey, brand,model,
     * parts(can be empty array),custom (can be undefined empty object ),traits(can be undefined empty object) */
    async edit(info) {
        return await this.api.call("component.edit", info);
    }
    /** Return true if deleted. Require only id */
    async delete(id) {
        return await this.api.call("component.delete", id);
    }
    /** Return component object . Require only id */
    async view(id) {
        return await this.api.call("component.view", id);
    }
    /** Return true if relocate. Require only id,locationKey */
    async relocate(id, locationKey) {
        return await this.api.call("component.relocate", id, locationKey);
    }
    /** Return true if changecategory. Require only id,categoryKey */
    async changeCategory(id, categoryKey) {
        return await this.api.call("component.changeCategory", id, categoryKey);
    }
    /** Return true if addPart. Require only id, part (name ,details (optional)) */
    async addPart(id, part) {
        return await this.api.call("component.addPart", id, part);
    }
    /** Return true if editPart. Require only id, part (name ,details (optional)) */
    async editPart(id, part) {
        return await this.api.call("component.editPart", id, part);
    }
    /** Return true if removePart. Require only id, part (name ,details (optional)) */
    async removePart(id, part) {
        return await this.api.call("component.removePart", id, part);
    }
    /** Return true if editTraits. Require only id, traits) */
    async editTraits(id, traits) {
        return await this.api.call("component.removePart", id, traits);
    }
    /** Return true if editCustom. Require only id, custom) */
    async editCustom(id, custom) {
        return await this.api.call("component.removePart", id, custom);
    }
}
exports.ComponentClient = ComponentClient;


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __webpack_require__(0);
class ComponentCategoryClient extends Client_1.Client {
    /** Return true  if added require key,name,desc (can be null)*/
    async add(info) {
        return await this.api.call("componentcategory.add", info);
    }
    /** Return true if edit require key,name,desc (can be null)*/
    async edit(info) {
        return await this.api.call("componentcategory.edit", info);
    }
    /** Return componentcategory object*/
    async list() {
        return await this.api.call("componentcategory.list");
    }
    /** Return true if deleted. Require only key */
    async delete(key) {
        return await this.api.call("componentcategory.delete", key);
    }
}
exports.ComponentCategoryClient = ComponentCategoryClient;


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const AWS = __webpack_require__(24);
const Client_1 = __webpack_require__(0);
const Storage_1 = __webpack_require__(25);
class TaskClient extends Client_1.Client {
    /** Return true if created .Can be null [id,userId,resolved,assigned,created and staus ], require category ,progress, data and images */
    async create(task, images) {
        return await this.api.call("task.create", task);
    }
    /** Return all Task object */
    async list() {
        return await this.api.call("task.list");
    }
    /** View all Task Object with taskId  */
    async view(taskId) {
        return await this.api.call("task.view", taskId);
    }
    /** Return all TaskCategory objects  */
    async listAllCategories() {
        return await this.api.call("task.listAllCategories");
    }
    /** Return all Component's possible TaskCategory objects with componentId   */
    async listComponentTaskCategories(componentId) {
        return await this.api.call("task.listComponentTaskCategories", componentId);
    }
    /** Uploads image and returns a URL.
     * Note: Pass in only 1 input File object
     */
    async uploadImage(imageFile) {
        let awsInfo = await this.api.call("storage.getParams");
        let file = await Storage_1.Storage.pack("images/task/foo.jpg", imageFile, { ensureKeyHasExtension: true });
        let buffer = Buffer.from(file.file, "base64");
        let params = {
            Bucket: awsInfo.bucket,
            Key: file.key,
            Body: buffer,
            ACL: StoragePermission.PublicRead,
            ContentEncoding: 'base64',
            ContentType: file.type,
        };
        AWS.config.update({
            accessKeyId: awsInfo.accessKeyId,
            secretAccessKey: awsInfo.secretAccessKey,
            region: awsInfo.region,
        });
        let s3 = new AWS.S3();
        let result = await s3.upload(params).promise();
        return result.Location + "ihbcwkrds7t4397r";
    }
}
exports.TaskClient = TaskClient;
var StoragePermission;
(function (StoragePermission) {
    StoragePermission["Private"] = "private";
    StoragePermission["PublicRead"] = "public-read";
    StoragePermission["PublicReadWrite"] = "public-read-write";
    StoragePermission["AwsExecRead"] = "aws-exec-read";
    StoragePermission["AuthenticatedRead"] = "authenticated-read";
    StoragePermission["BucketOwnerRead"] = "bucket-owner-read";
    StoragePermission["BucketOwnerFullControl"] = "bucket-owner-full-control";
    StoragePermission["LogDeliveryWrite"] = "log-delivery-write";
})(StoragePermission || (StoragePermission = {}));


/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = require("aws-sdk");

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Storage;
(function (Storage) {
    function extractBase64FromDataURL(dataURL) {
        // data:image/png;base64,iVBORw0KGgoA......
        let pos = dataURL.indexOf(",");
        if (pos == -1)
            throw new Error(`Not a DataURL format`);
        let b64 = dataURL.slice(pos + 1);
        return b64;
    }
    function getBase64(file) {
        return new Promise((pass, fail) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => pass(reader.result);
            reader.onerror = (error) => fail(error);
        });
    }
    // since API can be used from browser or node (they have different types to represent data)
    // we have to provide some method overloading to handle common data formats
    /** data can be File, Blob, Canvas, ArrayBuffer, UInt8Array, Buffer, or a base64 encoded string */
    async function pack(key, data, options = {}) {
        let { type, ensureKeyHasExtension = false } = options;
        let b64 = null;
        if (ensureKeyHasExtension && type != undefined) {
            let currentExt = Mime.getExtensionFromFilename(key);
            let expectedExt = Mime.getExtensionFromMime(type);
            if (currentExt != expectedExt) {
                key = `${key}.${expectedExt}`;
            }
        }
        if (type == undefined) {
            type = Mime.getMimeFromFilename(key);
        }
        if (typeof File != "undefined" && data instanceof File) {
            b64 = extractBase64FromDataURL(await getBase64(data));
        }
        else if (typeof Blob != "undefined" && data instanceof Blob) {
            throw "not implemented";
        }
        else if (typeof Canvas != "undefined" && data instanceof Canvas) {
            b64 = extractBase64FromDataURL(data.toDataURL());
        }
        else if (typeof ArrayBuffer != "undefined" && data instanceof ArrayBuffer) {
            throw "not implemented";
        }
        else if (typeof Uint8Array != "undefined" && data instanceof Uint8Array) {
            throw "not implemented";
        }
        else if (typeof Buffer != "undefined" && data instanceof Buffer) {
            throw "not implemented";
        }
        else if (typeof data == "string" || data instanceof String) {
            b64 = data;
        }
        if (b64 == null)
            throw new Error("Cannot process data for uploading");
        return {
            key,
            file: b64,
            type: type,
        };
        // await this.api.call("storage.upload", key, b64, type);
    }
    Storage.pack = pack;
    // export function multipart(): MultipartUpload
    // {
    //     throw "TODO";
    // }
})(Storage = exports.Storage || (exports.Storage = {}));
// export class MultipartUpload
// {
//     private storage: StorageClient;
//     private constructor(storage: StorageClient)
//     {
//         this.storage = storage;
//     }
//     public async start()
//     {
//     }
//     public async upload()
//     {
//     }
//     public async finish()
//     {
//     }
// }
var Mime;
(function (Mime) {
    let mimeToExt = new Map();
    let extToMime = new Map();
    function add(extension, mime) {
        mimeToExt.set(mime, extension);
        extToMime.set(extension, mime);
    }
    add("png", "image/png");
    add("jpg", "image/jpeg");
    add("jpeg", "image/jpeg");
    add("gif", "image/gif");
    function getExtensionFromFilename(filename) {
        let pos = filename.lastIndexOf(".");
        let ext = filename.slice(pos + 1);
        return ext;
    }
    Mime.getExtensionFromFilename = getExtensionFromFilename;
    function getMimeFromExtension(extension) {
        return extToMime.get(extension) || "application/octet-stream";
    }
    Mime.getMimeFromExtension = getMimeFromExtension;
    function getMimeFromFilename(filename) {
        let extension = getExtensionFromFilename(filename);
        return getMimeFromExtension(extension);
    }
    Mime.getMimeFromFilename = getMimeFromFilename;
    function getExtensionFromMime(mime) {
        return mimeToExt.get(mime);
    }
    Mime.getExtensionFromMime = getExtensionFromMime;
})(Mime = exports.Mime || (exports.Mime = {}));


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __webpack_require__(0);
class TaskCategoryClient extends Client_1.Client {
    /** Return  True require id(can be null),key,name,desc,actions,groupId  */
    async add(info) {
        return await this.api.call("taskcategory.add", info);
    }
}
exports.TaskCategoryClient = TaskCategoryClient;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __webpack_require__(0);
class DeviceClient extends Client_1.Client {
    /** Return true if device is registered or is already registered. */
    async register(device) {
        return await this.api.call("device.register", device);
    }
}
exports.DeviceClient = DeviceClient;


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Api_1 = __webpack_require__(1);
async function testRpc() {
    console.log("Invite Testing via RPC calls");
    try {
        console.log("HEADERS:");
        let api = new Api_1.Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.log(api.headers);
        console.log("=================================");
        // console.log("LIST CONTEXT ROLES:");
        // console.log(await api.call("test.listContextRoles"));
        // console.log("=================================");
        // console.log("SERVER TIME:");
        // console.log(await api.call("test.timestamp"));
        // console.log("=================================");
        console.log("LOGIN:");
        let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        if (!user)
            throw "Invalid login";
        console.log("- - - - - - - - - - - - - - - - -");
        console.log("LOGIN OK:");
        console.log(user.name);
        console.log("=================================");
        // console.log("LIST CONTEXT ROLES:");
        // console.log(await api.call("test.listContextRoles"));
        // console.log("=================================");
        console.log("Switch To Master Domain:");
        console.log(await api.auth.changeDomain("caviot-master"));
        console.log("=================================");
        console.log("LIST CONTEXT ROLES:");
        console.log(await api.call("test.listContextRoles"));
        console.log("=================================");
        console.log("LIST ROLES:");
        console.log(await api.call("member.listRoles"));
        console.log("=================================");
        console.log("CHECK FORMAT:");
        console.log(await api.invite.emailVerification("limhjf+embrio@gmail.com"));
        console.log("=================================");
        console.log("SEND INVITE:");
        console.log(await api.invite.createInv({ email: "limhjf+embrio@gmail.com", data: { name: "Ferdy Test", roleId: "3" } }));
        console.log("=================================");
        console.log(".............");
        console.log("............");
        console.log("...........");
        console.log("..........");
        console.log(".........");
        console.log("........");
        console.log(".......");
        console.log("......");
        console.log(".....");
        console.log("....");
        console.log("...");
        console.log("..");
        console.log(".");
    }
    catch (ex) {
        console.log(ex);
    }
}
exports.testRpc = testRpc;
async function main(...args) {
    console.log("Running tests:", args);
    try {
        testRpc();
    }
    catch (ex) {
        console.log("ERROR", ex);
    }
}
exports.main = main;


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Api_1 = __webpack_require__(1);
async function testRpc() {
    console.log("Signup Testing via RPC calls");
    try {
        console.log("HEADERS:");
        let api = new Api_1.Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.log(api.headers);
        console.log("=================================");
        // console.log("LIST CONTEXT ROLES:");
        // console.log(await api.call("test.listContextRoles"));
        // console.log("=================================");
        // console.log("SERVER TIME:");
        // console.log(await api.call("test.timestamp"));
        // console.log("=================================");
        // console.log("LIST CONTEXT ROLES:");
        // console.log(await api.call("test.listContextRoles"));
        // console.log("=================================");
        console.log("CHECK EMAIL:");
        console.log(await api.invite.list("limhjf+embrio@gmail.com"));
        console.log("=================================");
        console.log("Verify EMAIL:");
        console.log(await api.signup.emailVerification("limhjf+embrio@gmail.com"));
        console.log("=================================");
        console.log("CREATE ACCOUNT (Embrio):");
        console.log(await api.signup.createAccount({ email: "limhjf+embrio@gmail.com", password: "ferdy", data: { name: "Ferdy Test" } }));
        console.log("=================================");
        console.log("CREATE ACCOUNT (18r):");
        console.log(await api.signup.createAccount({ email: "limhjf+18r@gmail.com", password: "ferdy", data: { name: "Ferdy New" } }));
        console.log("=================================");
        console.log(".............");
        console.log("............");
        console.log("...........");
        console.log("..........");
        console.log(".........");
        console.log("........");
        console.log(".......");
        console.log("......");
        console.log(".....");
        console.log("....");
        console.log("...");
        console.log("..");
        console.log(".");
    }
    catch (ex) {
        console.log(ex);
    }
}
exports.testRpc = testRpc;
async function main(...args) {
    console.log("Running tests:", args);
    try {
        testRpc();
    }
    catch (ex) {
        console.log("ERROR", ex);
    }
}
exports.main = main;


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Api_1 = __webpack_require__(1);
async function testRpc() {
    console.log("Invite Testing via RPC calls");
    try {
        console.log("HEADERS:");
        let api = new Api_1.Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.dir(api.headers, { colors: true, depth: null });
        console.log("=================================");
        console.log("LOGIN:");
        let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        if (!user)
            throw "Invalid login";
        console.log("- - - - - - - - - - - - - - - - -");
        console.log("LOGIN OK:");
        console.log(user.name);
        console.log("=================================");
        console.log("Switch To Master Domain:");
        console.dir(await api.auth.changeDomain("caviot-master"), { colors: true, depth: null });
        console.log("=================================");
        console.log("List All Location");
        let locations = await api.location.children(undefined);
        console.dir(locations, { colors: true, depth: null });
        console.log("=================================");
        console.log("Edit Location");
        let changeLocation = locations[locations.length - 1];
        changeLocation.name += "2";
        console.dir(await api.location.edit(changeLocation), { colors: true, depth: null });
        console.log("=================================");
        console.log("Create Location");
        if (changeLocation.key)
            console.dir(await api.location.create({ name: "Zervexx Zervex Zerv", data: {}, parentId: changeLocation.id }), { colors: true, depth: null });
        console.log("=================================");
        console.log("View Location");
        if (changeLocation.key)
            console.dir(await api.location.view(changeLocation.key), { colors: true, depth: null });
        console.log("=================================");
        console.log("Tree Location");
        if (changeLocation.key)
            console.dir(await api.location.tree(changeLocation.key), { colors: true, depth: null });
        console.log("=================================");
        console.log("List Components");
        if (changeLocation.key)
            console.dir(await api.location.components(changeLocation.key), { colors: true, depth: null });
        console.log("=================================");
        // console.log("Delete Location");
        // if (changeLocation.key) console.log(await api.location.delete(changeLocation.key));
        // console.log("=================================");
        console.log(".............");
        console.log("............");
        console.log("...........");
        console.log("..........");
        console.log(".........");
        console.log("........");
        console.log(".......");
        console.log("......");
        console.log(".....");
        console.log("....");
        console.log("...");
        console.log("..");
        console.log(".");
    }
    catch (ex) {
        console.log(ex);
    }
}
exports.testRpc = testRpc;
async function main(...args) {
    console.log("Running tests:", args);
    try {
        // for (let i = 0; i < 5; i++) {
        //     for (let x = 0; x < 3; x++) {
        //         await component();
        //     }
        // await testRpc();
        // }
        console.log("HEADERS:");
        let api = new Api_1.Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.dir(api.headers), { colors: true, depth: null };
        console.log("=================================");
        console.log("LOGIN:");
        // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        let { user } = await api.auth.login("dnthien@cmc.com.vn", "123456");
        if (!user)
            throw "Invalid login";
        console.log("- - - - - - - - - - - - - - - - -");
        console.log("LOGIN OK:");
        console.log(user.name);
        console.log("=================================");
        console.log("Switch To Master Domain:");
        console.dir(await api.auth.changeDomain("caviot-master"), { colors: true, depth: null });
        console.log("=================================");
        console.log("List Components");
        console.dir((await api.location.components("zervexxxxx")), { colors: true, depth: null });
        console.log("=================================");
        console.log("Tree Location");
        console.dir((await api.location.tree("zervexxxxx")), { colors: true, depth: null });
        console.log("=================================");
    }
    catch (ex) {
        console.log("ERROR", ex);
    }
}
exports.main = main;


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Api_1 = __webpack_require__(1);
async function testRpc() {
    console.log("Invite Testing via RPC calls");
    try {
        console.log("HEADERS:");
        let api = new Api_1.Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.log(api.headers);
        console.log("=================================");
        console.log("LOGIN:");
        let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        if (!user)
            throw "Invalid login";
        console.log("- - - - - - - - - - - - - - - - -");
        console.log("LOGIN OK:");
        console.log(user.name);
        console.log("=================================");
        console.log("Switch To Master Domain:");
        console.log(await api.auth.changeDomain("caviot-master"));
        console.log("=================================");
        console.log("List All Location");
        let locations = await api.location.children(undefined);
        console.log(locations);
        console.log("=================================");
        let locationFind = locations[locations.length - 1];
        console.log("Add Component");
        // if (locationFind.key) console.log(await api.component.add({locationkey:locationFind.key,categorykey:"Electronics",brand:"Samsung",model:"MVC",parts:[],traits:{},custom:{}}));
        console.log("=================================");
        // console.log("Edit Component");
        // if (locationFind.key) console.log(await api.component.edit({id:"17",locationkey:locationFind.key,categorykey:"Electronics",brand:"Apple",model:"MVC",parts:[],traits:{},custom:{}}));
        // console.log("=================================");
        // console.log("Delete Component");
        // if (locationFind.key) console.log(await api.component.delete("17"));
        // console.log("=================================");
        console.log("View Component");
        console.log(await api.component.view("8"));
        console.log("=================================");
        // console.log("Relocate Component");
        // if (locationFind.key)  console.log(await api.component.relocate("18","zervexxxxx-zervexx-zervex-zerv"));
        // console.log("=================================");
        // console.log("ChangeCategory Component");
        // console.log(await api.component.changeCategory("18","caviot-master-rishabh-rawat"));
        // console.log("=================================");
        console.log("AddParts Component");
        // console.log(await api.component.addPart("18",{key:"test",name:"rishabh rawat",details:""}));
        console.log("=================================");
        // console.log("EditParts Component");
        // console.log(await api.component.editPart("18",{key:"test",name:"rsdf",details:""}));
        // console.log("=================================");
        // console.log("RemoveParts Component");
        // console.log(await api.component.removePart("18",{key:"test",name:"rsdf",details:""}));
        // console.log("=================================");
        // console.log("EditTraits Component");
        // console.log(await api.component.editTraits("18","sda"));
        // console.log("=================================");
        // console.log("Edit Custom Component");
        // console.log(await api.component.editCustom("83","sda"));
        // console.log("=================================");
        console.log(".............");
        console.log("............");
        console.log("...........");
        console.log("..........");
        console.log(".........");
        console.log("........");
        console.log(".......");
        console.log("......");
        console.log(".....");
        console.log("....");
        console.log("...");
        console.log("..");
        console.log(".");
    }
    catch (ex) {
        console.log(ex);
    }
}
exports.testRpc = testRpc;
async function main(...args) {
    console.log("Running tests:", args);
    try {
        testRpc();
    }
    catch (ex) {
        console.log("ERROR", ex);
    }
}
exports.main = main;


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Api_1 = __webpack_require__(1);
async function testRpc() {
    console.log("Invite Testing via RPC calls");
    try {
        console.log("HEADERS:");
        let api = new Api_1.Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.log(api.headers);
        console.log("=================================");
        console.log("LOGIN:");
        let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        if (!user)
            throw "Invalid login";
        console.log("- - - - - - - - - - - - - - - - -");
        console.log("LOGIN OK:");
        console.log(user.name);
        console.log("=================================");
        console.log("Switch To Master Domain:");
        console.log(await api.auth.changeDomain("caviot-master"));
        console.log("=================================");
        console.log("Add Component Category");
        console.log(await api.componentcategory.add({ key: "dsasddsa", name: "rishabh rawat", desc: { lalala: "hello" } }));
        console.log("=================================");
        console.log("Edit Component Category");
        console.log(await api.componentcategory.edit({ key: "caviot-master-rishabh-rawat", name: "rish rawat", desc: {} }));
        console.log("=================================");
        console.log("List All Component Category");
        let componentCategory = await api.componentcategory.list();
        console.log(componentCategory);
        console.log("=================================");
        console.log("List All Component Category");
        console.log(await api.componentcategory.delete("caviot-master-rishabh-rawat"));
        console.log("=================================");
        console.log(".............");
        console.log("............");
        console.log("...........");
        console.log("..........");
        console.log(".........");
        console.log("........");
        console.log(".......");
        console.log("......");
        console.log(".....");
        console.log("....");
        console.log("...");
        console.log("..");
        console.log(".");
    }
    catch (ex) {
        console.log(ex);
    }
}
exports.testRpc = testRpc;
async function main(...args) {
    console.log("Running tests:", args);
    try {
        testRpc();
    }
    catch (ex) {
        console.log("ERROR", ex);
    }
}
exports.main = main;


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Api_1 = __webpack_require__(1);
async function testRpc() {
    console.log("Invite Testing via RPC calls");
    try {
        console.log("HEADERS:");
        let api = new Api_1.Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.log(api.headers);
        console.log("=================================");
        console.log("LOGIN:");
        let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        if (!user)
            throw "Invalid login";
        console.log("- - - - - - - - - - - - - - - - -");
        console.log("LOGIN OK:");
        console.log(user.name);
        console.log("=================================");
        console.log("Switch To Master Domain:");
        console.log(await api.auth.changeDomain("caviot-master"));
        console.log("=================================");
        let categories = await api.task.listAllCategories();
        console.log(categories);
        console.log("=================================");
        let categoryFind = categories[0];
        console.log(categoryFind.id);
        console.log("Create Task ");
        // console.log(await api.task.create({id:"",userId:"",category:categoryFind, progress:[],data:{desc:"east or west everyone say OHLA!",images:[],componentId:"18"},resolved:null,assigned:null,status:""},[]));
        console.log("=================================");
        console.log("List Task ");
        console.log(await api.task.list());
        console.log("=================================");
        console.log("View Task ");
        console.log(await api.task.view("8"));
        console.log("=================================");
        console.log("listAllCategories ");
        console.log(await api.task.listAllCategories());
        console.log("=================================");
        console.log("Reassign User ");
        // console.log(await api.task.reassignUser("1","1"));
        console.log("=================================");
        // console.log("To DO Action")
        // console.log(await api.task.create({id:"",userId:"",category:categoryFind, progress:[],data:{desc:"east or west everyone say OHLA!",images:[],componentId:"18"},resolved:null,assigned:null,status:""},[]));
        // console.log("=================================");
        // console.log("listComponentTaskCategories ")
        // console.log(await api.task.listComponentTaskCategories("18"));
        // console.log("=================================");
        console.log(".............");
        console.log("............");
        console.log("...........");
        console.log("..........");
        console.log(".........");
        console.log("........");
        console.log(".......");
        console.log("......");
        console.log(".....");
        console.log("....");
        console.log("...");
        console.log("..");
        console.log(".");
    }
    catch (ex) {
        console.log(ex);
    }
}
exports.testRpc = testRpc;
async function main(...args) {
    console.log("Running tests:", args);
    try {
        testRpc();
    }
    catch (ex) {
        console.log("ERROR", ex);
    }
}
exports.main = main;


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Api_1 = __webpack_require__(1);
async function testRpc() {
    console.log("Invite Testing via RPC calls");
    try {
        console.log("HEADERS:");
        let api = new Api_1.Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.log(api.headers);
        console.log("=================================");
        console.log("LOGIN:");
        let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        if (!user)
            throw "Invalid login";
        console.log("- - - - - - - - - - - - - - - - -");
        console.log("LOGIN OK:");
        console.log(user.name);
        console.log("=================================");
        console.log("Switch To Master Domain:");
        console.log(await api.auth.changeDomain("caviot-master"));
        console.log("=================================");
        // let categories:TaskCategory[] =await api.task.listAllCategories();
        // console.log(categories);
        // let categoryFind =categories[0];
        console.log("List All Component Category");
        let componentCategory = await api.componentcategory.list();
        console.log(componentCategory);
        console.log("=================================");
        let category = componentCategory[0];
        console.log(category.name);
        console.log("Create Task Category ");
        console.log(await api.taskcategory.add({ id: "", key: category.key, name: category.name, desc: "This is me ", actions: [{ type: "random", notify: true, groupId: "1" }] }));
        console.log("=================================");
        console.log(".............");
        console.log("............");
        console.log("...........");
        console.log("..........");
        console.log(".........");
        console.log("........");
        console.log(".......");
        console.log("......");
        console.log(".....");
        console.log("....");
        console.log("...");
        console.log("..");
        console.log(".");
    }
    catch (ex) {
        console.log(ex);
    }
}
exports.testRpc = testRpc;
async function main(...args) {
    console.log("Running tests:", args);
    try {
        testRpc();
    }
    catch (ex) {
        console.log("ERROR", ex);
    }
}
exports.main = main;


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Api_1 = __webpack_require__(1);
async function testRpc() {
    console.log("Login & Domain Change Testing via RPC calls");
    try {
        console.log("HEADERS:");
        let api = new Api_1.Api("caviot", "", "sys/9SqweUQ50G2V29WRAjjCfBTU");
        // let api = new Api("caviot", "", "test/0fSt0yPzAWNvIU0qTOgkv6Jr");
        // let api = new Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
        console.log(api.headers);
        console.log("=================================");
        console.log("DEVICE REGISTER:");
        console.log(await api.device.register({ deviceId: "GDBkhde387yGY654HbjF2", deviceType: "Android" }));
        console.log("=================================");
        console.log("LOGIN:");
        // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // let { user } = await api.auth.login("ferdinand.lim@embrioent.com", "bb40thcoy");
        let { user } = await api.auth.login("technician@embrioent.com", "bb40thcoy");
        // let { user } = await api.auth.login("manager@embrioent.com", "bb40thcoy");
        // let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
        // let { user } = await api.auth.login("davidnguyen968@gmail.com", "C@vi0tD3v");
        if (!user)
            throw "Invalid login";
        console.log("- - - - - - - - - - - - - - - - -");
        console.log("LOGIN OK:");
        console.log(user);
        console.log("=================================");
        console.log(api.headers["x-omni-accesstoken"]);
        console.log("=================================");
        console.log("Switch To Master Domain:");
        console.log(await api.auth.changeDomain("caviot-master"));
        console.log("=================================");
        console.log("DEVICE REGISTER:");
        console.log(await api.device.register({ deviceId: "GDBkhde387yGY654HbjF2", deviceType: "iOS" }));
        console.log("=================================");
        console.log("HEADERS:");
        console.log(api.headers);
        console.log("=================================");
        console.log(".............");
        console.log("............");
        console.log("...........");
        console.log("..........");
        console.log(".........");
        console.log("........");
        console.log(".......");
        console.log("......");
        console.log(".....");
        console.log("....");
        console.log("...");
        console.log("..");
        console.log(".");
    }
    catch (ex) {
        console.log(ex);
    }
}
exports.testRpc = testRpc;
async function main(...args) {
    console.log("Running tests:", args);
    try {
        testRpc();
    }
    catch (ex) {
        console.log("ERROR", ex);
    }
}
exports.main = main;


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Api_1 = __webpack_require__(1);
async function demo(...args) {
    console.log("Running tests:", args);
    try {
        testRpc();
    }
    catch (ex) {
        console.log("ERROR", ex);
    }
}
exports.demo = demo;
async function testRpc() {
    console.log("Demo Testing via RPC calls");
    // create api
    console.log("HEADERS:");
    let api = new Api_1.Api("caviot", "", "mobile/HnFuW5RIarlurbao4gv4cdg5");
    console.log(api.headers);
    console.log("=================================");
    // login
    console.log("LOGIN:");
    let { user } = await api.auth.login("app_dev@embrioent.com", "Omn1tr1b3$");
    if (!user)
        throw "Invalid login";
    console.log("- - - - - - - - - - - - - - - - -");
    console.log("LOGIN OK:");
    console.log(user.name);
    console.log("=================================");
    // // create invite
    // console.log("CREATE INVITE:");
    // let roles = await api.member.listRoles();
    // console.log(await api.invite.createInv({email: "limhjf+embrio@gmail.com", data:{name:"Test Demo",roleId:roles[0].id}}));
    // console.log("=================================");
    // // check invite
}
exports.testRpc = testRpc;


/***/ })
/******/ ]);
//# sourceMappingURL=test.js.map
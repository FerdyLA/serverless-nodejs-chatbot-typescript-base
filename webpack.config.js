const webpack = require("webpack");
const path = require('path');
const slsw = require('serverless-webpack');

const entries = {};

Object.keys(slsw.lib.entries).forEach(key => (
    entries[key] = ['./source-map-install.js', "./libs/moon7/boot/Boot.ts", slsw.lib.entries[key]]
));

const plugins =
{
    ignoreMomentLocales: new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
};

module.exports =
{
    entry: entries,
    devtool: 'source-map',
    
    externals:
    {
        'aws-sdk': "commonjs aws-sdk",
    },
    
    resolve:
    {
        extensions:
        [
            '.js',
            '.jsx',
            '.json',
            '.ts',
            '.tsx',
        ],
        
        modules:
        [
            "src",
            "libs",
            "node_modules",
        ],
    },
    
    output:
    {
        libraryTarget: 'commonjs',
        path: path.join(__dirname, '.webpack'),
        filename: '[name].js',
    },
    
    target: 'node',
    
    module:
    {
        loaders:
        [
            { test: /\.ts(x?)$/, loader: 'ts-loader' },
        ],
    },
    
    plugins:
    [
        plugins.ignoreMomentLocales,
    ],
};
